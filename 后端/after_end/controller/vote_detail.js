// 引入数据库
const conn = require('../data/index.js');

// 引入md5
const md5 = require('md5')

const {secret_key} = require("../data/index.js");

// 通过id查找用户
module.exports.getVote_detailID = (req, res) => {
    // console.log(req.params)
    // 当通过id 查询用户的时候
    if (req.params.id !== undefined) {
        conn.query(`select * from vote_detail where id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询投票详情成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找投票
module.exports.getVote_detail = (req, res) => {
    // 第几页 页面数
    let pageNum = req.query.pagenum;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过user_name查询
    if (req.query.users_id === undefined) {
        req.query.users_id = ""
    }
    conn.query(`select count(*) total from vote_detail; select * from vote_detail where users_id like "%${req.query.user_name}%" limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        res.json({
            code: "200",
            msg: "分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
        // console.log(data)
    })

}

// 添加用户
module.exports.addVote_detail = (req, res) => {
    // 对id的判断
    if (req.body.id === undefined || req.body.id === "") {
        req.body.id = null;
    }
    // 对pwd加密
    req.body.pwd = md5(req.body.pwd+ + secret_key)

    // sql语句
    let sql = 'insert into vote_detail values(?,?,?)'
    // 得到的数据
    let params = [req.body.id, req.body.usid, req.body.usid];
    // 对空的数据进行判断
    params.forEach((item, index) => {
        if (item === undefined || item === "") {
            return item = '';
        }
    })
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加投票详情失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加投票详情成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 删除投票详情 deleteUsers
module.exports.deleteVote_detail = (req, res) => {
    let id = req.params.id
    let sql = `delete * from vote_detail where id = ?`;
    let params = [req.params.id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除投票详情失败",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除投票详情成功",
                data: data
            })
        }
    })
}

// 更改投票详情
module.exports.updateVote_detail = (req, res) => {
    let id = req.params.id;
    
    // sql语句
    let sql = `update vote_detail set vote_id=?,users_id=? where id=${req.params.id}`
    // 参数
    let params = [req.body.usid,req.body.usid];
    // 过滤
    params.forEach(item=>{
        if(item===undefined){
            item=''
        }
    })
    // 请求
    conn.query(sql,params,(error,data)=>{
        if(error){
            res.json({
                code:"400",
                msg:"修改投票详情失败",
                error:error
            })
            return console.log(error);
        }
        else{
            res.json({
                code:"200",
                msg:"修改投票详情成功"
            })
        }
    })
}