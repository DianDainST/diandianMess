// 引入数据库
const conn = require("../data/index.js");


// 查找用户评价信息 通过id查找
module.exports.getEvaluateID = (req, res) => {
  // 定义表名
  tableName = "evaluate";

  let sql = `SELECT * FROM evaluate a,list b ,orders c
   where a.list_id = b.id 
   And a.orders_id = c.id
   And id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示用户评价信息失败（通过id查询）",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "显示用户评价信息成功（通过id查询）",
        data: results // 返回数组中的第 1 条记录
      });
    }
  });
};

// 查找用户评价信息
module.exports.getEvaluate = (req, res) => {
  // 定义表名
  tableName = "evaluate";

  // 1. 接收查询参数
  let pagenum = req.query.pagenum || 1;
  let pagesize = req.query.pagesize || 10;
  let sortby = req.query.sortby || "a.id";
  let sortway = req.query.sortway || "asc";
  let content = req.query.content ;

  // 2. 计算出翻页的 limit （翻页原理）
  let _offset = (pagenum - 1) * pagesize;
  let limit = ` LIMIT ${_offset},${pagesize} `;

  // 3. 拼出排序的 order by
  let orderby = ` ORDER BY ${sortby} ${sortway}`;

  // 4. 拼出搜索的 where
  let where = "where a.list_id = b.id And a.orders_id=c.id And d.id=c.users_id";
  let data = [];
  // 如果传了 content 就设置查询的 where 和数据
  if (content) {
    where = "WHERE a.list_id = b.id And a.orders_id=c.id And d.id=c.users_id And list.name LIKE ?";
    data[0] = `%${content}%`;
  }

  // 5. 查询数据
  let sql = `SELECT COUNT(*) total FROM evaluate as a ,list as b ,orders as c ,users d ${where}; SELECT *,a.id FROM evaluate as a ,list as b ,orders as c ,users d ${where} ${orderby} ${limit}`;
  // 执行 SQL
  conn.query(sql, data, (err, results1, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示用户评价信息失败，通过分页查询或者模糊查询",
        error: err
      });
      return console.log(err);
    } else {
      // console.log(results1[1]);
      res.json({
        code: 200,
        msg: "显示用户评价信息成功，通过分页查询或者模糊查询",
        total: results1[0][0].total, // 取出第 1 条（只有一条）记录的 total 字段
        pagenum: req.query.pagenum,
        pagesize: req.query.pagenum,
        data: results1[1]
      });
    }
  });
};

// 添加用户评价信息
module.exports.addEvaluate = (req, res) => {
  // 添加
  // 定义表名
  tableName = "evaluate";

  // 2. 接收数据
  const list_id = req.body.list_id;
  const content = req.body.content;
  const orders_id = req.body.orders_id;
  const add_time = req.body.add_time;

  // 3. 插入到数据库中
  let sql = `INSERT INTO ${tableName} SET ?`;
  // 要插入的数据
  let data = {
    list_id,
    content,
    orders_id,
    add_time
  };
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "添加用户评价信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "添加用户评价信息成功",
        data: results
      });
    }
  });
};

// 修改用户评价信息
module.exports.updateEvaluate = (req, res) => {
  // 定义表名
  tableName = "evaluate";

  // 2. 接收数据
  const content = req.body.content;
  const orders_id = req.body.orders_id;
  const add_time = req.body.add_time;

  // 3. 修改到数据库中
  let sql = `UPDATE ${tableName} SET ? WHERE id=?`;
  // 要插入的数据
  let data = {
    content,
    orders_id,
    add_time
  };
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, [data, id], (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "修改用户评价信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "修改用户评价信息成功",
        data: results
      });
    }
  });
};

// 删除用户评价信息
module.exports.deleteEvaluate = (req, res) => {
  // 定义表名
  tableName = "evaluate";

  let sql = `DELETE FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "删除用户评价信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "删除用户评价信息成功",
        data: results
      });
    }
  });
};
