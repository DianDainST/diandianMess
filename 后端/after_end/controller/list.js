// 引入数据库
const conn = require('../data/index.js');

// 查找餐品基本信息 通过id查找
module.exports.getListID = (req, res) => {
    if (req.params.id !== undefined) {
        conn.query(`select * from list where id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询餐品成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找餐品基本信息
module.exports.getList = (req, res) => {
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 10;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 接受窗口id
    let winid = req.query.win_id || 1;

    // 判断是否有通过user_name查询
    if (req.query.name === undefined) {
        req.query.name = ""
    }
    conn.query(`select count(*) total from list where win_id = ${winid} and name like "%${req.query.name}%"; select * from list where win_id = ${winid} and name like "%${req.query.name}%" limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        res.json({
            code: "200",
            msg: "分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
        // console.log(data)
    })
}

// 添加餐品基本信息
module.exports.addList = (req, res) => {
    // 对id的判断
    if (req.query.id === undefined || req.query.id === "") {
        req.query.id = null;
    }
    // sql语句
    let sql = 'insert into list values(?,?,?,?,?,?,?)'
    // 得到的数据
    let unit = req.body.unit || "份";
    let params = [req.body.id, req.body.name, req.body.price, req.body.img, req.body.intro, unit, req.body.win_id];
    // 对空的数据进行判断
    params.forEach((item, index) => {
        if (item === undefined || item === "") {
            return item = '';
        }
    })
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加餐品失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加餐品成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 修改餐品基本信息
module.exports.updateList = (req, res) => {
    let id = req.params.id;

    let sql = `update list set name=?,price=?,img=?,intro=?,unit=?,
    win_id=? where id=${req.params.id}`

    let params = [req.body.name, req.body.price, req.body.img, req.body.intro, req.body.unit,
    req.body.win_id];

    // 过滤
    params.forEach(item => {
        if (item === undefined) {
            item = ''
        }
    })

    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改餐品失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "修改餐品成功"
            })
        }
    })
}

// 删除餐品基本信息
module.exports.deleteList = (req, res) => {
    let id = req.params.id
    let sql = `delete from list where id = ?`;
    let params = req.params.id;
    console.log(params)
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除菜品失败",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除菜品成功",
                data: data
            })
        }
    })
}




