// 引入数据库
const conn = require('../data/index.js');

// 引入密钥
let { secret_key } = require("../config");

// 使用md5加密
const md5 = require("md5");

let tableName = 'order_detail'

// 查找订单详情 通过id查找
module.exports.getOrder_detailID = (req, res) => {
    // 当通过id 查询订单详情的时候
    if (req.params.id !== undefined) {
        conn.query(`select * from order_detail a , list b ,orders c
        where  a.orders_id = c.id and a.list_id = b.id and a.id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询订单详情出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询订单详情成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找订单详情
module.exports.getOrder_detail = (req, res) => {
    // console.log(req.query)
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 20;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;

    let limit = `${indexStart} ${pageSize}`;

    let sortby = req.query.sortby || "a.id";
    let sortway = req.query.sortway || "asc";
    let list_name = req.query.list_name || " ";
    let box_fee = req.query.box_fee || " "
    // 判断是否有通过餐品基本信息表菜名list_name查询

    // 3. 拼出排序的 order by
    let orderby = ` ORDER BY ${sortby} ${sortway}`;

    // 如果传了 content 就设置查询的 where 和数据
    where = `WHERE a.orders_id = c.id And a.list_id = b.id And list.name LIKE ${list_name} And orders.box_fee LIKE ${box_fee}`;

    // sql语句
    let sql = `select count(*) total from  order_detail a , list b ,orders c ${where};
    select count(*) total from  order_detail a , list b ,orders c ${where} ${orderby} ${limit}`;
    // `select count(*) total from  order_detail a , list b ,orders c where  a.orders_id = c.id And a.list_id = b.id;
    // select * from order_detail a , list b ,orders c
    // where  a.orders_id = c.id And a.list_id = b.id And
    // a.list_name like "%${req.query.list_name}%" limit ${indexStart},${pageSize}`

    conn.query(sql, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "查找订单详情分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        // console.log(data);
        res.json({
            code: "200",
            msg: "查找订单详情分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
    })
}

// 添加订单详情
module.exports.addOrder_detail = (req, res) => {
    // 根据用户登录的token查询到id值 
    let users_id = req.usid;

    // sql语句
    let sql = `insert into ${tableName} set ?`;
    // 得到的数据
    let params = {
        orders_id: users_id || req.body.users_id,
        list_id: req.body.last_use_time,
        list_name: req.body.coupons_state,
        list_price: req.body.coupons_id,
        sum: req.body.sum,
        win_id: req.body.win_id,
        state: req.body.state
    }
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加用户优惠券表失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加用户优惠券表成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 修改订单详情
module.exports.updateOrder_detail = (req, res) => {
    // 更改的是订单详情的状态state
    sql = `update ${tableName} set ? where id =?`
    let data = {
        state: req.body.state
    }
    let params = [data, req.params.id]

    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改用户优惠券失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "修改用户优惠券成功"
            })
        }
    })
}

// 删除订单详情
module.exports.deleteOrder_detail = (req, res) => {
    let id = req.params.id
    let sql = `delete * from ${tableName} where id = ?`;
    let params = [id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除订单详情",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除订单详情",
                data: data
            })
        }
    })
}