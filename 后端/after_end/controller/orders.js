// 引入数据库
const conn = require('../data/index.js');

// 查找订单信息 通过id查找
module.exports.getOrdersID = (req, res) => {
    if (req.params.id !== undefined) {
        conn.query(`select * from orders a
        left join order_detail b on  a.id=b.orders_id
        left join win_id c on b.win_id = c.id
        left join list d on b.list_id = d.id
        where a.id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询用户成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找订单信息
module.exports.getOrders = (req, res) => {
    console.log(req.query);
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 10;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过users_id查询
    if (req.query.id === undefined) {
        req.query.id = ""
    }
    conn.query(`select count(*) total 
    from orders a , order_detail b, win c , list d ,meal_time e
    where a.id=b.orders_id 
    And b.win_id = c.id 
    And b.list_id = d.id 
    And a.meal_time_id = e.id
    group by a.add_time; 

    select a.id,e.meal_name,a.add_time,a.box_fee,a.total_price,GROUP_CONCAT(b.list_name) as 菜名 , GROUP_CONCAT(b.sum) as 分量, GROUP_CONCAT(b.list_price) as 价格
    from orders a , order_detail b, win c , list d ,meal_time e
    where a.id=b.orders_id 
    And b.win_id = c.id 
    And b.list_id = d.id 
    And a.meal_time_id = e.id
    And a.id = "${req.query.id}" 
    group by a.add_time
    limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        res.json({
            code: "200",
            msg: "分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
    })
}

// 添加订单信息
module.exports.addOrders = (req, res) => {
    // 对id的判断
    if (req.body.id === undefined || req.body.id === "") {
        req.body.id = null;
    }
    // sql语句
    let sql = 'insert into orders values(?,?,?,?,?,?,?,?,?,?,?,?,?)'
    // 得到的数据
    let params = [req.body.id, req.body.users_id, req.body.meal_time_id, req.body.people_num, req.body.orders_state, req.body.coupons_id, req.body.coupons_cut_money, req.body.express_fee, req.body.total_price, req.body.add_time, req.body.qr_code, req.body.get_time, req.body.box_fee];
    // 对空的数据进行判断
    params.forEach((item, index) => {
        if (item === undefined || item === "") {
            return item = '';
        }
    })
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加订单失败",
                error: error
            })
            return console.log(error);
        } else {
            res.json({
                code: "200",
                msg: "添加订单成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 修改订单信息
module.exports.updateOrders = (req, res) => {
    let id = req.params.id;
    // sql语句
    let sql = `update orders set users_id=?,meal_time_id=?,people_num=?,orders_state=?,coupons_id=?,
    coupons_cut_money=?,express_fee=?,total_price=?,add_time=?,qr_code=?,get_time=?,box_fee=?,
     where id=${req.params.id}`
    // 参数
    let params = [req.body.users_id, req.body.meal_time_id, req.body.people_num, req.body.orders_state, req.body.coupons_id,
    req.body.coupons_cut_money, req.body.express_fee, req.body.total_price.req.body.add_time, req.body.qr_code, req.body.get_time, req.body.box_fee
    ];
    // 过滤
    params.forEach(item => {
        if (item === undefined) {
            item = ''
        }
    })
    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改订单失败",
                error: error
            })
            return console.log(error);
        } else {
            res.json({
                code: "200",
                msg: "修改订单成功"
            })
        }
    })
}

// 删除订单信息
module.exports.deleteOrders = (req, res) => {
    let id = req.params.id
    let sql = `delete * from orders where id = ?`;
    let params = [req.params.id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除订单失败",
                errer: error
            })
            return console.log(error);
        } else {
            res.json({
                code: "200",
                msg: "删除订单成功",
                data: data
            })
        }
    })
}