// 引入数据库
const conn = require('../data/index.js');

// 渲染菜单表
// 渲染菜单表
module.exports.getMenus = (req, res) => {
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 10;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    let num = req.query.query;
    conn.query(`select count(*) total from meal_time;select dishes.* from list dishes; select a.*,a.id as menu_id,a.count as menu_count ,b.*,c.* from menu a,list b, meal_time c where a.meal_time_id = c.id  and a.list_id = b.id and c.meal_name like '%${num}%'  limit ${indexStart},${pageSize};`, (error, data) => {
        if (error) return console.log(error)
        res.json({
            "code": "200",
            "msg": "显示菜单表成功",
            "data": data[2],
            total: data[0][0].total,
            dishes: data[1],
            pagesize: pageSize,
            pagenum: pageNum,
        })
    })
}

// 添加菜单表
module.exports.addMenus = (req, res) => {
    conn.query('select * from meal_time where meal_name = ?', req.body.meal_name, (error, data) => {
        if (error) return console.log(error)
        if (data.length == 1) {
            res.json({
                "code": '300',
                "msg": "菜单名重复"
            })
        } else {
            // 菜的预定时间表
            let time = {
                meal_name: req.body.meal_name,
                meal_start: req.body.meal_start,
                meal_end: req.body.meal_end,
                pay_end: req.body.pay_end
            }
            let list = req.body.num
            conn.query('insert into meal_time set ?', time, (error, data) => {
                if (error) return console.log(error);
                conn.query(`select id from meal_time where meal_name like "%${req.body.meal_name}%"`, (error, data) => {
                    if (error) return console.log(error)
                    global.meal_time_id = data[0].id;
                    for (let i = 0; i < list.length; i++) {
                        conn.query('insert into menu(meal_time_id,list_id,count,off_price) values(?,?,?,?)', [global.meal_time_id, list[i], req.body.count,req.body.price], (error, data) => {
                            if (error) return console.log(error);
                            if (i == list.length - 1) {
                                res.json({
                                    "code": "200",
                                    "msg": "添加菜单表成功",
                                })
                            }

                        })
                    }

                })
            })
        }
    })

}

// 修改数据,回显,通过id查找
module.exports.getMenusID = (req, res) => {
    let id = [req.params.id]
    conn.query('select * from list ;select a.*,b.*,c.* from  menu a,meal_time b ,list c where a.meal_time_id = ? and a.meal_time_id = b.id and a.list_id = c.id', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            'code': '200',
            "msg": '查找成功',
            data
        })
    })
}

// 修改菜单表
module.exports.updateMenus = (req, res) => {
    // 获取菜单时间的名字
    let meal_name = [req.params.id]
    // 查询菜单时间表id
    conn.query('select id from meal_time where meal_name = ?', meal_name, (error, data) => {
        if (error) return console.log(error)
        let meal_time_id = data[0].id
        conn.query("delete from menu where meal_time_id = ?;delete from meal_time where id = ?", [meal_time_id, meal_time_id], (error, data) => {
            if (error) return console.log(error)
            // 插入数据
            let time = {
                meal_name: req.body.meal_name,
                meal_start: req.body.meal_start,
                meal_end: req.body.meal_end,
                pay_end: req.body.pay_end
            }
            let list = [req.body.list_id]
            conn.query('insert into meal_time set ?', time, (error, data) => {
                if (error) return console.log(error);
                conn.query(`select id from meal_time where meal_name like "%${req.body.meal_name}%"`, (error, data) => {
                    if (error) return console.log(error)
                    global.meal_time_id_two = data[0].id;
                    for (let i = 0; i < list[0].length; i++) {
                        conn.query('insert into menu(meal_time_id,list_id,count,off_price) values(?,?,?,?)', [global.meal_time_id_two, list[0][i], 1, 1], (error, data) => {
                            if (error) return console.log(error);
                            if (i == list.length - 1) {
                                res.json({
                                    "code": "200",
                                    "msg": "修改菜单表成功",
                                })
                            }
                        })
                    }

                })
            })
        })
    })

}


// 删除菜单表
module.exports.deleteMenus = (req, res) => {
    let name = req.params.id;
    conn.query('select id from meal_time where meal_name = ?', name, (error, results) => {
        if (error) return console.log(error)
        let id = results[0].id;
        conn.query('delete from menu where meal_time_id = ?;delete from meal_time where id = ?', [id, id], (error, results) => {
            if (error) return console.log(error);
            res.json({
                "code": "200",
                "msg": "删除成功"
            })
        })
    })
}