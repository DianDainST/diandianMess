// 引入数据库
const conn = require("../data/index.js");

// 查找窗口权限 通过id查找
module.exports.getwinID = (req, res) => {
  // 定义表名
  tableName = "win";

  let sql = `SELECT id,username FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示窗口权限失败（通过id查询）",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "显示窗口权限成功（通过id查询）",
        data: results // 返回数组中的第 1 条记录
      });
    }
  });
};

// 查找窗口权限
module.exports.getwin = (req, res) => {
  // 定义表名
  tableName = "win";

  // 1. 接收查询参数
  let pagenum = req.query.pagenum || 1;
  let pagesize = req.query.pagesize || 10;
  let sortby = req.query.sortby || "id";
  let sortway = req.query.sortway || "asc";
  let win_name = req.query.win_name;

  // 2. 计算出翻页的 limit （翻页原理）
  let _offset = (pagenum - 1) * pagesize;
  let limit = ` LIMIT ${_offset},${pagesize} `;

  // 3. 拼出排序的 order by
  let orderby = ` ORDER BY ${sortby} ${sortway}`;

  // 4. 拼出搜索的 where
  let where = "";
  let data = [];
  // 如果传了 win_name 就设置查询的 where 和数据
  if (win_name) {
    where = " WHERE win_name LIKE ?";
    data[0] = `%${win_name}%`;
  }

  // 5. 查询数据
  let sql = `SELECT id,win_name FROM ${tableName} ${where} ${orderby} ${limit}`;
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // 6. 查询出总的记录数
    sql = `SELECT COUNT(*) total FROM ${tableName} ${where}`;
    conn.query(sql, data, (err1, results1, fields1) => {
      if (err) {
        res.json({
          code: 400,
          msg: "显示窗口权限失败，通过分页查询或者模糊查询",
          error: err
        });
      } else {
        res.json({
          code: 200,
          msg: "显示窗口权限成功，通过分页查询或者模糊查询",
          total: results1[0].total, // 取出第 1 条（只有一条）记录的 total 字段
          pagenum,
          pagesize,
          data: results
        });
      }
    });
  });
};

// 添加窗口权限
module.exports.addwin = (req, res) => {
  // 添加
  // 定义表名
  tableName = "win";

  // 2. 接收数据
  const win_name = req.body.win_name;

  // 3. 插入到数据库中
  let sql = `INSERT INTO ${tableName} SET ?`;
  // 要插入的数据
  let data = {
    win_name
  };
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "添加窗口权限失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "添加窗口权限成功",
        data: results
      });
    }
  });
};

// 修改窗口权限
module.exports.updatewin = (req, res) => {
  // 定义表名
  tableName = "win";

  // 2. 接收数据
  const win_name = req.body.win_name;

  // 3. 修改到数据库中
  let sql = `UPDATE ${tableName} SET ? WHERE id=?`;
  // 要插入的数据
  let data = {
    win_name
  };
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, [data, id], (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "修改窗口权限失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "修改窗口权限成功",
        data: results
      });
    }
  });
};

// 删除窗口权限
module.exports.deletewin = (req, res) => {
  // 定义表名
  tableName = "win";

  let sql = `DELETE FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "删除窗口权限失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "删除窗口权限成功",
        data: results
      });
    }
  });
};
