// 引入数据库
const conn = require("../data/index");
// 引入设置token
const jwt = require("jsonwebtoken");
// 引入密钥
const { secret_key } = require("../config");
// 引入md5
const md5 = require('md5')

//  学生登录
module.exports.loginsStu = (req, res) => {
    if (req.body.user_name === undefined || req.body.pwd === undefined) {
        res.json({
            code: "400",
            "error": "用户名或密码不能为空",
            msg: "用户名或密码不能为空"
        })
    }
    if (req.body.pwd.length < 6 || req.body.pwd.length > 18) {
        res.json({
            code: "400",
            "error": "密码必须 6 到 18 位",
            msg: "密码必须 6 到 18 位"
        })
    }
    if (req.body.user_name.length < 2 || req.body.user_name.length > 12) {
        res.json({
            code: "400",
            "error": "用户名必须 2 到 12 位",
            msg: "用户名必须 2 到 12 位"
        })
    }
    // 判断 学生登录
    let params = [req.body.user_name];
    // console.log(params)
    let sql = `select * from  users where user_name =? `
    conn.query(sql, params, (err, data) => {
        if (err) {
            res.json({
                code: "400",
                msg: "查询出现错误",
                error: err
            })
            return console.log(err);
        }
        else if (data.length == 0) {
            res.json({
                code: "400",
                msg: "用户名不存在",
            })
        }
        else {
            // console.log(md5(req.body.pwd + secret_key),data[0].pwd);
            // 把用户输入的密码加密然后和数据库中的密码对比
            if (data[0].pwd == md5(req.body.pwd + secret_key)) {
                // 生成令牌
                // 参数一、加入到令牌中的数据（用户ID等）
                // 参数二、密钥
                // 参数三、配置信息（过期时间）
                let token = jwt.sign(
                    { id: data[0].id },
                    secret_key,
                    { expiresIn: "2h" }
                )
                // 返回令牌
                res.json({
                    "code": 200,
                    "token": token,
                    "id": data[0].id
                })

            }
            else {
                res.json({
                    "code": 400,
                    "error": '密码不正确！',
                    msg: "密码不正确！"
                })
            }

        }

    })
}


// 管理员登录
module.exports.loginsGly = (req, res) => {
    if (req.body.user_name === undefined || req.body.pwd === undefined) {
        res.json({
            code: "400",
            "error": "管理员名或密码不能为空",
            msg: "管理员名或密码不能为空"
        })
    }
    if (req.body.pwd.length < 6 || req.body.pwd.length > 18) {
        res.json({
            code: "400",
            "error": "密码必须 6 到 18 位",
            msg: "密码必须 6 到 18 位"
        })
    }
    if (req.body.user_name.length < 2 || req.body.user_name.length > 12) {
        res.json({
            code: "400",
            "error": "用户名必须 2 到 12 位",
            msg: "用户名必须 2 到 12 位"
        })
    }
    // 判断 管理员登录
    let params = [req.body.user_name];
    let sql = `select * from grantee where user_name =? `
    conn.query(sql, params, (err, data) => {
        if (err) {
            res.json({
                code: "400",
                msg: "查询出现错误",
                error: err
            })
            return console.log(err);
        }
        else if (data.length == 0) {
            res.json({
                code: "400",
                msg: "管理员名不存在",
            })
        }
        else {
            // 把管理员输入的密码加密然后和数据库中的密码对比
            // console.log(md5(req.body.pwd + secret_key),data[0].pwd )
            if (data[0].pwd == md5(req.body.pwd + secret_key)) {
                // 生成令牌
                // 参数一、加入到令牌中的数据（管理员ID等）
                // 参数二、密钥
                // 参数三、配置信息（过期时间）
                let token = jwt.sign(
                    { id: data[0].id },
                    secret_key,
                    { expiresIn: "2h" }
                )
                // console.log(token)
                // 返回令牌
                res.json({
                    "code": 200,
                    "token": token,
                    "msg": "登录成功"
                })
            }
            else {
                res.json({
                    "code": 400,
                    "error": '密码不正确！',
                    "msg": "密码不正确！"
                })
            }
        }
    })
}