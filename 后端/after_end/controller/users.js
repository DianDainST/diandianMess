// 引入数据库
const conn = require('../data/index.js');

// 引入md5
const md5 = require('md5')

const { secret_key } = require("../config");



const fs = require("fs");

let OSS = require('ali-oss');

let client = new OSS({
    // 仓库地域节点
    region: 'oss-cn-beijing',
    //云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，部署在服务端使用RAM子账号或STS，部署在客户端使用STS。
    // oss 中 密钥id
    accessKeyId: 'LTAI4FuUuL1cAvhVLSXLH672',
    // oss 访问权限代码
    accessKeySecret: 'buhJAjwhtqlVeMHcYLqxvYgVXoXLEN',
    //  存储仓库名字
    bucket: 'wanghailongwang',
});



// 通过id查找用户
module.exports.getUsersID = (req, res) => {
    // console.log(req.params)
    // 当通过id 查询用户的时候
    if (req.params.id !== undefined) {
        conn.query(`select * from users where id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询用户成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找用户
module.exports.getUsers = (req, res) => {
    // console.log(req.query);
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 10;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过user_name查询
    if (req.query.user_name === undefined) {
        req.query.user_name = ""
    }
    conn.query(`select count(*) total from users; select * from users where user_name like "%${req.query.user_name}%" limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        res.json({
            code: "200",
            msg: "分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
    })

}

// 添加用户
module.exports.addUsers = (req, res) => {
    // 对id的判断
    if (req.body.id === undefined || req.body.id === "") {
        req.body.id = null;
    }
    // 对pwd加密
    // console.log(md5(req.body.pwd + secret_key));
    pwd = md5(req.body.pwd + secret_key)
    // sql语句
    let sql = 'insert into users set ?'
    // 得到的数据
    let params = {
        user_name: req.body.user_name,
        pwd: pwd,
        nick_name: req.body.nick_name || "",
        phone_num: req.body.phone_num || "",
        class: req.body.class || "",
        idcard: req.body.idcard || "",
        sushe_num: req.body.sushe_num || 1,
        come_from: req.body.come_from || "",
        wechat_id: req.body.wechat_id || "",
        favorite: req.body.favorite || "",
        gender: req.body.gender || '男',
        birthday: req.body.birthday || '2000-01-01 00:00:00',
        last_point: req.body.last_point || 0,
        total_point: req.body.total_point || 0,
        header_img: req.body.header_img || "",
        money: req.body.money || 0
    }
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加用户失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加用户成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 删除用户 deleteUsers
module.exports.deleteUsers = (req, res) => {
    let id = req.params.id
    let sql = `delete * from users where id = ?`;
    let params = [req.params.id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除用户失败",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除用户成功",
                data: data
            })
        }
    })
}

// 更改用户 
module.exports.updateUsers = (req, res) => {
    let id = req.params.id;
    // 老师方法 若用 要将下方的sql 和 parmas注释掉 并

    sql = `update users set ? where id =?`
    let data = {
        user_name: req.body.user_name,
        pwd: req.body.pwd,
        nick_name: req.body.nick_name || '',
        phone_num: req.body.phone_num || '',
        class: req.body.class || '',
        idcard: req.body.idcard || '',
        sushe_num: req.body.sushe_num || '',
        come_from: req.body.come_from || '',
        wechat_id: req.body.wechat_id || '',
        favorite: req.body.favorite || '',
        gender: req.body.gender,
        birthday: req.body.birthday,
        last_point: req.body.last_point,
        total_point: req.body.total_point,
        header_img: req.body.header_img
    }
    let params = [data, req.params.id]

    // // sql语句
    // let sql = `update users set user_name=?,pwd=?,nick_name=?,phone_num=?,class=?,
    // idcard=?,sushe_num=?,come_from=?,wechat_id=?,favorite=?,gender_enum=?,birthday=?,
    // last_point=?,total_point=?,header_img=? where id=${req.params.id}`
    // // 参数
    // let params = [req.body.user_name, req.body.pwd, req.body.nick_name, req.body.phone_num, req.body.class,
    // req.body.idcard, req.body.sushe_num, req.body.come_from,req.body.wechat_id, req.body.favorite, req.body.gender_enum, req.body.birthday,
    // req.body.last_point, req.body.total_point, req.body.header_img];

    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改用户失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "修改用户成功"
            })
        }
    })
}


// 上传图片到oss
module.exports.Upload = (req, res) => {
    // console.log(req.file);
    fs.writeFile("./public/uploed/" + req.file.originalname, req.file.buffer, async (error) => {
        if (error) {
            res.json({
                data: { pic: null },
                code: "400",
                msg: "上传失败！"

            })
            return console.log(error);
        }
        // 上传 文件 指令
        let result = await client.put('/images/' + req.file.originalname, "./public/uploed/" + req.file.originalname);
        // console.log(result.url)
        res.json({
            // data: { pic: "/uploed/" + req.file.originalname ,},
            data: { pic: result.url },
            code: "200",
            msg: "上传成功！"
        })
    })
}