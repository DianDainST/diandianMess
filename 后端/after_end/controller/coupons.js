// 引入数据库
const conn = require("../data/index.js");

// 定义表名
let tableName = "coupons";

// 查找优惠卷信息 通过id查找
module.exports.getCouponsID = (req, res) => {

  let sql = `SELECT * FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示优惠券信息失败（通过id查询）",
        error: err
      });
      console.log(err);
      
    } else {
      res.json({
        code: 200,
        msg: "显示优惠券信息成功（通过id查询）",
        data: results // 返回数组中的第 1 条记录
  
      });
      console.log(results)
    }
  });
};

// 查找优惠卷信息
module.exports.getCoupons = (req, res) => {
  // 1. 接收查询参数
  let pagenum = req.query.pagenum || 1;
  let pagesize = req.query.pagesize || 10;
  let sortby = req.query.sortby || "id";
  let sortway = req.query.sortway || "asc";
  let coupons_name = req.query.coupons_name;

  // 2. 计算出翻页的 limit （翻页原理）
  let _offset = (pagenum - 1) * pagesize;
  let limit = ` LIMIT ${_offset},${pagesize} `;

  // 3. 拼出排序的 order by
  let orderby = ` ORDER BY ${sortby} ${sortway}`;

  // 4. 拼出搜索的 where
  let where = "";
  let data = [];
  // 如果传了 coupons_name 就设置查询的 where 和数据
  if (coupons_name) {
    where = " WHERE coupons_name LIKE ?";
    data[0] = `%${coupons_name}%`;
  }

  // 5. 查询数据
  let sql = `SELECT * FROM ${tableName} ${where} ${orderby} ${limit}`;
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // 6. 查询出总的记录数
    sql = `SELECT COUNT(*) total FROM ${tableName} ${where}`;
    conn.query(sql, data, (err1, results1, fields1) => {
      if (err) {
        res.json({
          code: 400,
          msg: "显示优惠券信息失败，通过分页查询或者模糊查询",
          error: err
        });
        console.log(err);
      } else {
        res.json({
          code: 200,
          msg: "显示优惠券信成功，通过分页查询或者模糊查询",
          total: results1[0].total, // 取出第 1 条（只有一条）记录的 total 字段
          pagenum,
          pagesize,
          data: results
        });
      }
    });
  });
};

// 添加优惠卷信息
module.exports.addCoupons = (req, res) => {
  // 添加
  // 1. 定义表名
  tableName = "coupons";

  // 2. 接收数据
  const coupons_name = req.body.coupons_name;
  const cut_money = req.body.cut_money;
  const time_limit = req.body.time_limit;
  const type = req.body.type;
  const take_state = req.body.take_state;
  const point_use = req.body.point_use;
  console.log(req.body);
  // 3. 插入到数据库中
  let sql = `INSERT INTO ${tableName} SET ?`;
  // 要插入的数据
  let data = {
    coupons_name,
    cut_money,
    time_limit,
    type,
    take_state,
    point_use
  };

  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "添加优惠卷信息失败",
        error: err
      });
      console.log(err);
    } else {
      res.json({
        code: 200,
        msg: "添加优惠卷信息成功",
        data: results
      });
    }
  });
};

// 修改优惠卷信息
module.exports.updateCoupons = (req, res) => {
  // 1. 定义表名
  tableName = "coupons";

  // 2. 接收数据
  const coupons_name = req.body.coupons_name;
  const cut_money = req.body.cut_money;
  const time_limit = req.body.time_limit;
  const take_state = req.body.take_state;
  const point_use = req.body.point_use;

  // 3. 修改到数据库中
  let sql = `UPDATE ${tableName} SET ? WHERE id=?`;
  // 要插入的数据
  let data = {
    coupons_name,
    cut_money,
    time_limit,
    take_state,
    point_use
  };
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, [data, id], (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "修改优惠卷信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "修改优惠卷信息成功",
        data: results
      });
    }
  });
};

// 删除优惠卷信息
module.exports.deleteCoupons = (req, res) => {
  // 定义表名
  tableName = "coupons";

  let sql = `DELETE FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "删除优惠卷失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "删除优惠卷成功",
        data: results
      });
    }
  });
};
