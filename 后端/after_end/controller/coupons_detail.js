// 引入数据库
const conn = require('../data/index.js');

// 引入密钥
const { secret_key } = require('../config');

// 引入md5加密
const md5 = require("md5");

// 表名
let tableName = 'coupons_detail';

// 查找用户优惠券表 通过id查找
module.exports.getCoupons_detailID = (req, res) => {
    // 当通过id 查询用户优惠券表的时候
    if (req.params.id !== undefined) {
        conn.query(`select * from coupons_detail a , users b , coupons c where  a.users_id=b.id and a.coupons_id=c.id and a.id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询用户优惠券表出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询用户优惠券表成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找用户优惠券表
module.exports.getCoupons_detail = (req, res) => {
    // console.log(req.query)
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 20;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过用户的用户名user_name查询
    if (req.query.user_name === undefined) {
        req.query.user_name = ""
    }
    conn.query(`select count(*) total from ${tableName}; select * from coupons_detail a , users b , coupons c where b.user_name like "%${req.query.user_name}%" and a.users_id=b.id and a.coupons_id=c.id limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "查找用户优惠券表分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        console.log(data);
        res.json({
            code: "200",
            msg: "查找用户优惠券表分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
        // console.log(data)
    })
}

// 添加用户优惠券表
module.exports.addCoupons_detail = (req, res) => {
    // 根据用户登录的token查询到id值 
    let users_id = req.usid;

    // sql语句
    let sql = `insert into ${tableName} set ?`;
    // 得到的数据
    let params = {
        users_id: users_id || req.body.users_id,
        last_use_time: req.body.last_use_time,
        coupons_state: req.body.coupons_state,
        coupons_id: req.body.coupons_id
    }
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加用户优惠券表失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加用户优惠券表成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 修改用户优惠券表
module.exports.updateCoupons_detail = (req, res) => {

    // 更改的是优惠卷 的到期时间 和 优惠卷的状态
    sql = `update ${tableName} set ? where id =?`
    let data = {
        last_use_time: req.body.last_use_time,
        coupons_state: req.body.coupons_state,
    }
    let params = [data, req.params.id]

    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改用户优惠券失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "修改用户优惠券成功"
            })
        }
    })
}

// 删除用户优惠券表
module.exports.deleteCoupons_detail = (req, res) => {
    let id = req.params.id
    let sql = `delete * from ${tableName} where id = ?`;
    let params = [id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除用户优惠券表",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除除用户优惠券表",
                data: data
            })
        }
    })
}