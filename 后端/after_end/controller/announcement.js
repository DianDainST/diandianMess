// 引入数据库
const conn = require('../data/index.js');

// 查找公告
module.exports.getAnnouncement = (req, res) => {
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 10;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 搜索数据
    let num = req.query.query;
    conn.query(`select * from gonggao where add_user like '%${num}%'   limit ${indexStart},${pageSize};`, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "获取公告成功",
            data
        })
    })
}

// 添加公告
module.exports.postAnnouncement = (req, res) => {
    let id = req.stid;
    conn.query("select user_name from grantee where id = ?", id, (error, data) => {
        if (error) return console.log(error)
        let name = data[0].user_name;
        conn.query('insert into gonggao(title,add_user,add_time,last_update_user,last_update_time,content,state) values(?,?,?,?,?,?,?)',[req.body.title,name,req.body.add_time,name,req.body.add_time,req.body.content, req.body.state],(error,data) =>{
            if(error) return console.log(error)
            res.json({
                code:'200',
                msg:"添加成功"
            })
        })
    })
}

// 回显公告
module.exports.getAnnouncementID = (req, res) => {
    let id = req.params.id;
    conn.query("select * from gonggao where id = ?",id,(error,data) => {
        if(error) return console.log(error)
        res.json({
            code:"200",
            msg:"查找成功",
            data
        })
    })
}

// 修改公告
module.exports.putAnnouncement = (req, res) => {
    let id=req.params.id;
    conn.query('select user_name from grantee where id = ?',req.stid,(error,data) => {
        if(error) return console.log(error)
        let name = data[0].user_name;
        conn.query('update gonggao set title=?,last_update_user=?,last_update_time=?,content=?,state=? where id=?',[req.body.title,name,req.body.last_update_time,req.body.content,req.body.state,id],(error,data) => {
            if(error) return console.log(error)
            res.json({
                code:"200",
                msg:"修改成功"
            })
        })
    })
    
}

// 删除公告
module.exports.delAnnouncement = (req, res) => {
    let id = req.params.id;
    conn.query("delete from gonggao where id = ?",id,(error,data) => {
        if(error) return console.log(error)
        res.json({
            code:'200',
            msg:"删除成功"
        })
    })
}
