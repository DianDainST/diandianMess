// 引入数据库
const conn = require('../data/index.js');


// 查找留言
module.exports.getMessage = (req, res) => {
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 5;
    // console.log(pageSize)
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // console.log(indexStart)
    conn.query(`select count(*) total from message;select a.*,b.user_name,c.user_name grantee_name from message a left join users b on a.users_id = b.id left join grantee c on a.grantee_id = c.id limit ${indexStart},${pageSize}`, (error, data) => {
        if (error) return console.log(error)
        res.json({
            "code": "200",
            "msg": "获取评论成功",
            data:data[1],
            total:data[0]
        })
    })
}

// 回显
module.exports.getMessageID = (req,res) => {
    let id = [req.params.id]
    conn.query('select * from message where id = ?',id,(error,data) => {
        if(error) return console.log(error)
        res.json({
            "code":"200",
            "msg":"查找留言成功",
            data
        })
    })
}

// 回复留言
module.exports.putMessage = (req,res) => {
    conn.query('update message set reply = ? ,reply_time = ?,grantee_id=? where id = ?',[req.body.reply,req.body.reply_time,req.stid,req.body.id],(error,data) => {
        if(error) return console.log(error);
        res.json({
            "code":"200",
            "msg":"回复成功"
        })
    })
}