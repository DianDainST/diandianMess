// 引入数据库
const conn = require("../data/index.js");

// 查找窗口业绩 通过id查找
module.exports.getwin_countID = (req, res) => {
  // 定义表名
  tableName = "win_count";

  let sql = `SELECT id,username FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示窗口业绩失败（通过id查询）",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "显示窗口业绩成功（通过id查询）",
        data: results // 返回数组中的第 1 条记录
      });
    }
  });
};

// 查找窗口业绩
module.exports.getwin_count = (req, res) => {
  // 定义表名
  tableName = "win_count";

  // 1. 接收查询参数
  let pagenum = req.query.pagenum || 1;
  let pagesize = req.query.pagesize || 10;
  let sortby = req.query.sortby || "id";
  let sortway = req.query.sortway || "asc";
  let win_sum = req.query.win_sum || " ";

  // 2. 计算出翻页的 limit （翻页原理）
  let _offset = (pagenum - 1) * pagesize;
  let limit = ` LIMIT ${_offset},${pagesize} `;

  // 3. 拼出排序的 order by
  let orderby = ` ORDER BY ${sortby} ${sortway}`;

  // 4. 拼出搜索的 where
  let where = "";
  let data = [];
  // 如果传了 win_sum 就设置查询的 where 和数据
  if (win_sum) {
    where = " WHERE win_sum LIKE ?";
    data[0] = `%${win_sum}%`;
  }

  // 5. 查询数据
  let sql = `SELECT id,win_sum FROM ${tableName} ${where} ${orderby} ${limit}`;
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // 6. 查询出总的记录数
    sql = `SELECT COUNT(*) total FROM ${tableName} ${where}`;
    conn.query(sql, data, (err1, results1, fields1) => {
      if (err) {
        res.json({
          code: 400,
          msg: "显示窗口业绩失败，通过分页查询或者模糊查询",
          error: err
        });
        return console.log(err);
      } else {
        res.json({
          code: 200,
          msg: "显示窗口业绩成功，通过分页查询或者模糊查询",
          total: results1[0].total, // 取出第 1 条（只有一条）记录的 total 字段
          pagenum,
          pagesize,
          data: results
        });
      }
    });
  });
};

// 添加窗口业绩
module.exports.addwin_count = (req, res) => {
  // 添加
  // 定义表名
  tableName = "win_count";

  // 2. 接收数据
  const win_count_name = req.body.win_sum;

  // 3. 插入到数据库中
  let sql = `INSERT INTO ${tableName} SET ?`;
  // 要插入的数据
  let data = {
    win_count_name
  };
  // 执行 SQL
  conn.query(sql, data, (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "添加窗口业绩失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "添加窗口业绩成功",
        data: results
      });
    }
  });
};

// 修改窗口业绩
module.exports.updatewin_count = (req, res) => {
  // 定义表名
  tableName = "win_count";

  // 2. 接收数据
  const win_count_name = req.body.win_count_name;

  // 3. 修改到数据库中
  let sql = `UPDATE ${tableName} SET ? WHERE id=?`;
  // 要插入的数据
  let data = {
    win_count_name
  };
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, [data, id], (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "修改窗口业绩失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "修改窗口业绩成功",
        data: results
      });
    }
  });
};

// 删除窗口业绩
module.exports.deletewin_count = (req, res) => {
  // 定义表名
  tableName = "win_count";

  let sql = `DELETE FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  conn.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "删除窗口业绩失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "删除窗口业绩成功",
        data: resultss
      });
    }
  });
};
