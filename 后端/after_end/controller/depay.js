// 引入数据库
const conn = require("../data/index.js");

  // 定义表名
let  tableName = "depay";

// 查找预付优惠表信息 通过id查找
module.exports.getDepayID = (req, res) => {
  let sql = `SELECT * FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = [req.params.id];
  // 执行 SQL
  db.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "显示预付优惠表信息失败（通过id查询）",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "显示预付优惠表信息成功（通过id查询）",
        data: results // 返回数组中的第 1 条记录
      });
    }
  });
};

// 查找预付优惠表信息
module.exports.getDepay = (req, res) => {
 // console.log(req.query)
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 20;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过用户的用户名user_name查询
    if (req.query.id === undefined) {
        req.query.id = ""
    }
    conn.query(`select count(*) total from ${tableName}; select * from  ${tableName} where id like "%${req.body.id}%" limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "查找用户优惠券表分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        console.log(data);
        res.json({
            code: "200",
            msg: "查找用户优惠券表分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
        // console.log(data)
    })
};

// 添加预付优惠表信息
module.exports.addDepay = (req, res) => {
  // 2. 接收数据
  const pay_money = req.body.pay_money;
  const song_money = req.body.song_money;

  // 3. 插入到数据库中
  let sql = `INSERT INTO ${tableName} SET ?`;
  // 要插入的数据
  let data = {
    pay_money,
    song_money 
  };
  // 执行 SQL
  db.query(sql, data, (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "添加预付优惠表信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "添加预付优惠表信息成功",
        data: results
      });
    }
  });
};

// 修改预付优惠表信息
module.exports.updateDepay = (req, res) => {

  // 2. 接收数据
  const pay_money = req.body.pay_money;
  const song_money = req.body.song_money;


  // 3. 修改到数据库中
  let sql = `UPDATE ${tableName} SET ? WHERE id=?`;
  // 要插入的数据
  let data = {
    pay_money,
    song_money
  };
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  db.query(sql, [data, id], (err, results, fields) => {
    // err：执行 SQL 失败时的错误令牌
    // results：执行 SQL 成功之后的返回值
    // fields：表中字段的令牌
    // 4. 返回结果
    // 是否出错
    if (err) {
      res.json({
        code: 400,
        msg: "修改预付优惠表信息失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "修改预付优惠表信息成功",
        data: results
      });
    }
  });
};

// 删除预付优惠表信息
module.exports.deleteDepay = (req, res) => {

  let sql = `DELETE FROM ${tableName} WHERE id=?`;
  // 获取id
  let id = req.params.id;
  // 执行 SQL
  db.query(sql, id, (err, results, fields) => {
    if (err) {
      res.json({
        code: 400,
        msg: "删除预付优惠表失败",
        error: err
      });
    } else {
      res.json({
        code: 200,
        msg: "删除预付优惠表成功",
        data: results
      });
    }
  });
};
