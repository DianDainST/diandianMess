// 引入数据库
const conn = require('../data/index.js');

// 引入密钥
const { secret_key } = require('../config');

// 引入md5加密
const md5 = require("md5");

// 表名
let tableName='grantee';

// 查找管理员 通过id查找
module.exports.getGranteeID = (req, res) => {
    // console.log(req.params)
    // 当通过id 查询管理员的时候
    if (req.params.id !== undefined) {
        conn.query(`select * from ${tableName} where id = ${req.params.id}`, (error, data) => {
            // 发生错误
            if (error) {
                res.json({
                    code: "400",
                    msg: "id查询出现错误",
                    error: error
                })
                return console.log(error);
            }
            // 成功的结果
            res.json({
                code: "200",
                msg: "通过id查询管理员成功",
                data: data
            })
            // console.log(data);
        })
    }
}

// 查找管理员
module.exports.getGrantee = (req, res) => {
    // console.log(req.query)
    // 第几页 页面数
    let pageNum = req.query.pagenum || 1;
    // 当前页面 的数据量
    let pageSize = req.query.pagesize || 20;
    // 计算limit 的值
    let indexStart = (pageNum - 1) * pageSize;
    // 判断是否有通过user_name查询
    if (req.query.user_name === undefined) {
        req.query.user_name = ""
    }
    conn.query(`select count(*) total from ${tableName}; select * from ${tableName} where user_name like "%${req.query.user_name}%" limit ${indexStart},${pageSize}`, (error, data) => {
        // 如果发生错误
        if (error) {
            res.json({
                code: "400",
                msg: "分页或名字查询出现错误",
                error: error
            })
            return console.log(error);
        }
        // 成功的结果
        // console.log(data);
        res.json({
            code: "200",
            msg: "分页或名字查询成功",
            total: data[0][0].total,
            pagesize: pageSize,
            pagenum: pageNum,
            data: data[1]
        })
        // console.log(data)
    })

}

// 添加管理员
module.exports.addGrantee = (req, res) => {
    // 对id的判断
    if (req.body.id === undefined || req.body.id === "") {
        req.body.id = null;
    }
    // 2. 验证接收到的数据
    if (req.body.user_name == undefined) {
        res.json({
            "code": 400,
            "error": "必须要填写管理员名"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }
    if (req.body.user_name.length < 2 || req.body.user_name.length > 18) {
        res.json({
            "code": 400,
            "error": "管理员名必须 2 ~ 18 位"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }

    if (req.body.pwd == undefined) {
        res.json({
            "code": 400,
            "error": "必须要填写密码"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }
    if (req.body.pwd.length < 6 || req.body.pwd.length > 18) {
        res.json({
            "code": 400,
            "error": "密码必须 6 ~ 18 位"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }


    // 对pwd加密
    req.body.pwd = md5(req.body.pwd + + secret_key)
    // sql语句
    let sql = `insert into ${tableName} set ?`;
    // 得到的数据
    let params = {
        id: req.body.id || null,
        user_name: req.body.user_name,
        pwd: req.body.pwd,
        part: req.body.part,
        win_id: req.body.win_id
    }
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "添加管理员失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "添加管理员成功",
                data: data
            })
            // console.log(data);
        }
    })
}

// 修改管理员
module.exports.updateGrantee = (req, res) => {
    let id = req.params.id;
    // 1. 接收用户名和密码
    const username = req.body.user_name
    const password = req.body.pwd

    // 2. 验证接收到的数据
    if (username == undefined) {
        res.json({
            "code": 400,
            "error": "必须要填写用户名",
            msg:"必须要填写用户名"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }
    if (username.length < 2 || username.length > 18) {
        res.json({
            "code": 400,
            "error": "用户名必须 2 ~ 18 位",
            msg:"用户名必须 2 ~ 18 位"

        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }

    if (password == undefined) {
        res.json({
            "code": 400,
            "error": "必须要填写密码",
            msg:"必须要填写密码"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }
    if (password.length < 6 || password.length > 18) {
        res.json({
            "code": 400,
            "error": "密码必须 6 ~ 18 位",
            msg:"密码必须 6 ~ 18 位"
        })
        // 退出当前这个接口的程序，不再向后执行了
        return
    }
    // 老师方法 若用 要将下方的sql 和 parmas注释掉 并

    sql = `update ${tableName} set ? where id =?`
    let data = {
        user_name: req.body.user_name,
        pwd: md5(req.body.pwd + secret_key),
        part: req.body.part || "窗口",
        win_id: req.body.win_id || '-1',
    }
    let params = [data, req.params.id]

    // 请求
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "修改用户失败",
                error: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "修改用户成功"
            })
        }
    })
}

// 删除管理员
module.exports.deleteGrantee = (req, res) => {
    let id = req.params.id
    let sql = `delete * from ${tableName} where id = ?`;
    let params = [id];
    conn.query(sql, params, (error, data) => {
        if (error) {
            res.json({
                code: "400",
                msg: "删除用户失败",
                errer: error
            })
            return console.log(error);
        }
        else {
            res.json({
                code: "200",
                msg: "删除用户成功",
                data: data
            })
        }
    })
}