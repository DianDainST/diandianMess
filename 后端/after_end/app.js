const express = require("express");

const app = express();

// body-parser 针对post 的提交
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

// 引入 cors 解决跨域 问题
var cors = require("cors");
app.use(cors());

var corsOptions = {
  origin: "http://127.0.0.1:8080",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.get("/products/:id", cors(corsOptions), function(req, res, next) {
  res.json({ msg: "This is CORS-enabled for only example.com." });
});

// 引入express-session
const session = require("express-session");

// 配置session
app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: false,
    // 登录时间600秒
    cookie: {
      maxAge: 6000000
    }
  })
);



// 引入登录的路由 logins.js
const logins = require("./router/logins.js");
app.use(logins);
// 引入用户管理路由 users.js
const users = require("./router/users.js");
app.use(users);
// 引入管理员管理信息路由 grantee
const grantee = require("./router/grantee.js");
app.use(grantee);
// 引入查找用户优惠券表信息 coupons_detail
const coupons_detail = require("./router/coupons_detail.js");
app.use(coupons_detail);
// 引入订单详情信息
const order_detail = require("./router/order_detail.js");
app.use(order_detail);
// 引入预付优惠信息
const depay = require("./router/depay.js");
app.use(depay);

// 引入餐品管理信息路由 list.js
const list = require("./router/list.js");
app.use(list);

// 引入订单基本信息路由 orders
const orders = require("./router/orders");
app.use(orders);

// 引入投票表路由 vote.js
const vote = require("./router/vote.js");
app.use(vote);

// 引入投票详情表路由
const vote_detail = require("./router/vote_detail.js");
app.use(vote_detail);

// 托管菜单表页面
const menu = require("./router/menus.js");
app.use(menu);

// 托管留言页面
const message = require("./router/message.js");
app.use(message);

// 托管留言页面
const coupons = require("./router/coupons.js");
app.use(coupons);

// 托管留言页面
const evaluate = require("./router/evaluate.js");
app.use(evaluate);

// 托管留言页面
const win_count = require("./router/win_count.js");
app.use(win_count);

// 托管留言页面
const win = require("./router/win.js");
app.use(win);

// 静态托管
app.use(express.static("./public"));

// 监听窗口
app.listen(8080, () => {
  console.log("http://127.0.0.1:8080");
});
