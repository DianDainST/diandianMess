const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/win");

// 查找优惠卷 通过id查找
router.get("/api/v1/win/:id", ctr.getwinID);
// 查找优惠卷
router.get("/api/v1/win", ctr.getwin);
// 添加优惠卷
router.post("/api/v1/win", ctr.addwin);
// 修改优惠卷
router.put("/api/v1/win/:id", ctr.updatewin);
// 删除优惠卷
router.delete("/api/v1/win/:id", ctr.deletewin);

// 暴露路由
module.exports = router;
