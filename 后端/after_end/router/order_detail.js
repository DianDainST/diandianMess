const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/order_detail.js")

// 查找订单详情信息 通过id查找
router.get("/api/v1/order_detail/:id",ctr.getOrder_detailID);
// 查找订单详情信息
router.get("/api/v1/order_detail",ctr.getOrder_detail);
// 添加订单详情信息
router.post("/api/v1/order_detail",ctr.addOrder_detail);
// 修改订单详情信息
router.put("/api/v1/order_detail/:id",ctr.updateOrder_detail);
// 删除订单详情信息
router.delete("/api/v1/order_detail/:id",ctr.deleteOrder_detail);

// 暴露路由
module.exports = router;