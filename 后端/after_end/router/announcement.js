const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/announcement")

// 查找公告
router.get("/api/v1/announcement", ctr.getAnnouncement);

// 添加公告
router.post("/api/v1/announcement", ctr.postAnnouncement);

// 回显公告
router.get('/api/v1/announcement/:id', ctr.getAnnouncementID)

// 修改公告
router.put('/api/v1/announcement/:id', ctr.putAnnouncement)

// 删除公告
router.delete('/api/v1/announcement/:id', ctr.delAnnouncement)


// 暴露路由
module.exports = router;