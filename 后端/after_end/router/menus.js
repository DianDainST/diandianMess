const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/menus")


// 查找菜单表
router.get("/api/v1/menus",ctr.getMenus);
// 添加菜单表
router.post("/api/v1/menus",ctr.addMenus);
// 修改回显
router.get("/api/v1/menus/:id",ctr.getMenusID);
// 修改菜单表
router.put("/api/v1/menus/:id",ctr.updateMenus);
// 删除菜单表
router.delete("/api/v1/menus:id",ctr.deleteMenus);

// 暴露路由
module.exports = router;