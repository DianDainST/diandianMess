const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/evaluate.js");

// 查找用户评价信息 通过id查找
router.get("/api/v1/evaluate/:id", ctr.getEvaluateID);
// 查找用户评价信息
router.get("/api/v1/evaluate", ctr.getEvaluate);
// 添加用户评价信息
router.post("/api/v1/evaluate", ctr.addEvaluate);
// 修改用户评价信息
router.put("/api/v1/evaluate/:id", ctr.updateEvaluate);
// 删除用户评价信息
router.delete("/api/v1/evaluate/:id", ctr.deleteEvaluate);

// 暴露路由
module.exports = router;
