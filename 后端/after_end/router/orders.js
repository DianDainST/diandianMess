const express = require("express");

const router = express.Router();

// 引入密钥 
let {secret_key} = require("../config");

// 引入包
const jwt = require("jsonwebtoken")


// 引入controller
const ctr = require("../controller/orders.js")



// 查找订单信息 通过id查找
router.get("/api/v1/orders/:id",ctr.getOrdersID);
// 查找订单信息
router.get("/api/v1/orders",ctr.getOrders);
// 添加订单信息
router.post("/api/v1/orders",ctr.addOrders);
// 修改订单信息
router.put("/api/v1/orders/:id",ctr.updateOrders);
// 删除订单信息
router.delete("/api/v1/orders/:id",ctr.deleteOrders);

// 暴露路由
module.exports = router;