const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/grantee.js")

// 查找管理员 通过id查找
router.get("/api/v1/grantee/:id(\\d+)",ctr.getGranteeID);
// 查找管理员
router.get("/api/v1/grantee",ctr.getGrantee);
// 添加管理员
router.post("/api/v1/grantee",ctr.addGrantee);
// 修改管理员
router.put("/api/v1/grantee/:id(\\d+)",ctr.updateGrantee);
// 删除管理员
router.delete("/api/v1/grantee/:id(\\d+)",ctr.deleteGrantee);

// 暴露路由
module.exports = router;