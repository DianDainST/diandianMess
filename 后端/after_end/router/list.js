const express = require("express");

const router = express.Router();

// 引入密钥 
let {secret_key} = require("../config");

// 引入包
const jwt = require("jsonwebtoken")


// 引入controller
const ctr = require("../controller/list")


// 查找餐品基本信息 通过id查找
router.get("/api/v1/list/:id",ctr.getListID);
// 查找餐品基本信息
router.get("/api/v1/list",ctr.getList);
// 添加餐品基本信息
router.post("/api/v1/list",ctr.addList);
// 修改餐品基本信息
router.put("/api/v1/list/:id",ctr.updateList);
// 删除餐品基本信息
router.delete("/api/v1/list/:id",ctr.deleteList);

// 暴露路由
module.exports = router;