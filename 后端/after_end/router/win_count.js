const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/win_count");

// 查找优惠卷 通过id查找
router.get("/api/v1/win_count/:id", ctr.getwin_countID);
// 查找优惠卷
router.get("/api/v1/win_count", ctr.getwin_count);
// 添加优惠卷
router.post("/api/v1/win_count", ctr.addwin_count);
// 修改优惠卷
router.put("/api/v1/win_count/:id", ctr.updatewin_count);
// 删除优惠卷
router.delete("/api/v1/win_count/:id", ctr.deletewin_count);

// 暴露路由
module.exports = router;
