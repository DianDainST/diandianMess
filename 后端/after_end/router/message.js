const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/message")


// 查找留言
router.get("/api/v1/message", ctr.getMessage);

// 回显留言
router.get('/api/v1/message/:id',ctr.getMessageID)

// 回复留言
router.put('/api/v1/message',ctr.putMessage)


// 暴露路由
module.exports = router;

