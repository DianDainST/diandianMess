const express = require("express");

const router = express.Router();

// 引入密钥 
let {secret_key} = require("../config");

// 引入包
const jwt = require("jsonwebtoken")

// 引入controller
const ctr = require("../controller/vote.js")




// 查找投票 通过id查找
router.get("/api/v1/vote/:id(\\d+)",ctr.getVoteID);
// 查找投票
router.get("/api/v1/vote",ctr.getVote);

// 添加投票
router.post('/api/v1/vote',ctr.addVote);

// 删除投票
router.delete("/api/v1/vote/:id(\\d+)",ctr.deleteVote);

// 修改投票
router.put('/api/v1/vote/:id(\\d+)',ctr.updateVote);



// 将路由暴露出去
module.exports=router;