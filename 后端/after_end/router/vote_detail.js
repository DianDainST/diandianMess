const express = require("express");

const router = express.Router();

// 引入密钥 
let {secret_key} = require("../config");

// 引入包
const jwt = require("jsonwebtoken")

// 引入controller
const ctr = require("../controller/vote_detail.js")




// 查找投票详情 通过id查找
router.get("/api/v1/vote_detail/:id(\\d+)",ctr.getVote_detailID);

// 查找投票详情
router.get("/api/v1/vote_detail",ctr.getVote_detail);

// 添加投票详情
router.post('/api/v1/vote_detail',ctr.addVote_detail);

// 删除投票详情
router.delete("/api/v1/vote_detail/:id(\\d+)",ctr.deleteVote_detail);

// 修改投票详情
router.put('/api/v1/vote_detail/:id(\\d+)',ctr.updateVote_detail);



// 将路由暴露出去
module.exports=router;