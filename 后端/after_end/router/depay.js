const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/depay")

// 查找预付优惠表  通过id查找
router.get("/api/v1/depay/:id",ctr.getDepayID);
// 查找预付优惠表 
router.get("/api/v1/depay",ctr.getDepay);
// 添加预付优惠表 
router.post("/api/v1/depay",ctr.addDepay);
// 修改预付优惠表 
router.put("/api/v1/depay/:id",ctr.updateDepay);
// 删除预付优惠表 
router.delete("/api/v1/depay/:id",ctr.deleteDepay);

// 暴露路由
module.exports = router;