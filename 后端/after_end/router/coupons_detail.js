const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/coupons_detail.js")

// 查找用户优惠券表信息 通过id查找
router.get("/api/v1/coupons_detail/:id",ctr.getCoupons_detailID);
// 查找用户优惠券表信息
router.get("/api/v1/coupons_detail",ctr.getCoupons_detail);
// 添加用户优惠券表信息
router.post("/api/v1/coupons_detail",ctr.addCoupons_detail);
// 修改用户优惠券表信息
router.put("/api/v1/coupons_detail/:id",ctr.updateCoupons_detail);
// 删除用户优惠券表信息
router.delete("/api/v1/coupons_detail/:id",ctr.deleteCoupons_detail);

// 暴露路由
module.exports = router;