const express = require("express");

const router = express.Router();

// 引入上传文件插件
const multer = require("multer");
const uplode = multer()

// 引入密钥 
let { secret_key } = require("../config");

// 引入包
const jwt = require("jsonwebtoken")

// 引入controller
const ctr = require("../controller/users.js")

// 添加用户
router.post('/api/v1/users', ctr.addUsers);

// 为路由添加中间键
router.use((req, res, next) => {
    // 获取令牌
    // console.log(req.headers)
    let token = req.headers.authorization.substring(7);
    // console.log(token);

    // 解析令牌
    try {
        let decoded = jwt.verify(token, secret_key);
        // console.log(decoded);
        req.stid = decoded.id
        next();
        // decoded中有id 可以存储在req 中
    } catch (err) {
        res.json({
            "code": "401",
            "msg": "解析token失败"
        })
    }
});


// 查找用户 通过id查找
router.get("/api/v1/users/:id(\\d+)", ctr.getUsersID);
// 查找用户
router.get("/api/v1/users", ctr.getUsers);



// 删除用户
router.delete("/api/v1/users/:id(\\d+)", ctr.deleteUsers);

// 修改用户
router.put('/api/v1/users/:id(\\d+)', ctr.updateUsers);

// 上传图片
// 6.上传图片
router.post("/api/v1/users/Upload", uplode.single("file"), ctr.Upload);


// 将路由暴露出去 
module.exports = router;