const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/logins")



// 用户登录
router.post("/api/v1/logins",ctr.loginsStu);
// 管理员登录
router.post("/api/v1/loginsGly",ctr.loginsGly);


// 暴露路由
module.exports = router;