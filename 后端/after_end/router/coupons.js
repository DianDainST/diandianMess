const express = require("express");

const router = express.Router();

// 引入controller
const ctr = require("../controller/coupons")

// 查找优惠卷 通过id查找
router.get("/api/v1/coupons/:id",ctr.getCouponsID);
// 查找优惠卷
router.get("/api/v1/coupons",ctr.getCoupons);
// 添加优惠卷
router.post("/api/v1/coupons",ctr.addCoupons);
// 修改优惠卷
router.put("/api/v1/coupons/:id",ctr.updateCoupons);
// 删除优惠卷
router.delete("/api/v1/coupons/:id",ctr.deleteCoupons);

// 暴露路由
module.exports = router;