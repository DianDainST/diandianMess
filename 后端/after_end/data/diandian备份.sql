/*
 Navicat Premium Data Transfer

 Source Server         : whlFWQ
 Source Server Type    : MySQL
 Source Server Version : 50644
 Source Host           : 39.106.158.169:3306
 Source Schema         : diandian

 Target Server Type    : MySQL
 Target Server Version : 50644
 File Encoding         : 65001

 Date: 21/10/2019 15:53:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for coupons
-- ----------------------------
DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '优惠卷表的id',
  `coupons_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '优惠卷名称',
  `cut_money` tinyint(4) NOT NULL COMMENT '面额',
  `time_limit` tinyint(4) NOT NULL COMMENT '期限',
  `type` tinyint(4) NOT NULL COMMENT '优惠券类型 0:可重复领取 1:不可重复领取',
  `take_state` tinyint(4) NOT NULL COMMENT '0：可领取 1：不可领取',
  `point_use` int(11) NULL DEFAULT NULL COMMENT '兑换积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of coupons
-- ----------------------------
INSERT INTO `coupons` VALUES (1, '芒种优惠券', 21, 1, 0, 0, 23);
INSERT INTO `coupons` VALUES (3, '元宵节优惠券', 5, 4, 0, 1, 2);
INSERT INTO `coupons` VALUES (4, '端午节优惠卷', 10, 3, 0, 0, 4);
INSERT INTO `coupons` VALUES (5, '程序员节优惠卷', 5, 5, 0, 1, 2);
INSERT INTO `coupons` VALUES (6, '元旦优惠券', 5, 1, 1, 0, 2);
INSERT INTO `coupons` VALUES (7, '情人节优惠卷', 15, 2, 0, 0, 6);
INSERT INTO `coupons` VALUES (8, '圣诞节优惠卷', 5, 2, 1, 1, 2);
INSERT INTO `coupons` VALUES (9, '双十一优惠卷', 10, 12, 0, 0, 4);
INSERT INTO `coupons` VALUES (10, '中秋节优惠卷', 15, 5, 0, 0, 6);
INSERT INTO `coupons` VALUES (11, '王海龙优惠券', 10, 6, 0, 0, 100);
INSERT INTO `coupons` VALUES (12, '腊八节优惠券', 5, 6, 5, 1, 26);
INSERT INTO `coupons` VALUES (13, '春节优惠券', 25, 10, 1, 0, 30);
INSERT INTO `coupons` VALUES (14, '寒食节优惠券', 45, 1, 1, 0, 50);
INSERT INTO `coupons` VALUES (15, '劳动节优惠券', 34, 3, 1, 1, 23);
INSERT INTO `coupons` VALUES (16, '教师节优惠券', 29, 2, 0, 1, 26);
INSERT INTO `coupons` VALUES (17, '立冬优惠券', 37, 1, 1, 0, 21);
INSERT INTO `coupons` VALUES (18, '圣诞节优惠券', 25, 3, 1, 0, 46);
INSERT INTO `coupons` VALUES (19, '建军节优惠券', 81, 8, 0, 1, 81);
INSERT INTO `coupons` VALUES (20, '中元节优惠券', 78, 1, 0, 1, 20);
INSERT INTO `coupons` VALUES (21, '100元优惠卷', 100, 7, 0, 0, 1);
INSERT INTO `coupons` VALUES (22, '50元优惠卷', 50, 4, 0, 0, 1);
INSERT INTO `coupons` VALUES (23, '10元优惠卷', 10, 4, 0, 0, 1);
INSERT INTO `coupons` VALUES (24, '5元优惠卷', 5, 3, 1, 0, 3);
INSERT INTO `coupons` VALUES (25, '2元优惠卷', 2, 4, 0, 0, 5);
INSERT INTO `coupons` VALUES (26, '1元优惠卷', 1, 6, 0, 0, 3);

-- ----------------------------
-- Table structure for coupons_detail
-- ----------------------------
DROP TABLE IF EXISTS `coupons_detail`;
CREATE TABLE `coupons_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户优惠卷标id',
  `users_id` int(11) NOT NULL COMMENT '用户id 外键-用户基本信息表',
  `last_use_time` datetime(0) NOT NULL COMMENT '到期时间',
  `coupons_state` tinyint(4) NOT NULL COMMENT '优惠券状态 0:可用 1：已使用 2：已过期  外键-优惠卷信息表',
  `coupons_id` int(11) NOT NULL COMMENT '优惠卷id 外键-优惠卷信息表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of coupons_detail
-- ----------------------------
INSERT INTO `coupons_detail` VALUES (1, 1, '2019-11-07 09:45:16', 1, 1);
INSERT INTO `coupons_detail` VALUES (2, 2, '2019-10-23 11:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (3, 1, '2019-10-24 10:45:16', 0, 6);
INSERT INTO `coupons_detail` VALUES (4, 1, '2019-10-25 10:45:16', 1, 1);
INSERT INTO `coupons_detail` VALUES (5, 2, '2019-10-15 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (6, 2, '2019-10-26 10:45:16', 0, 2);
INSERT INTO `coupons_detail` VALUES (7, 2, '2019-10-27 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (8, 1, '2019-10-20 10:45:16', 1, 7);
INSERT INTO `coupons_detail` VALUES (9, 3, '2019-10-20 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (10, 3, '2019-10-24 10:45:16', 1, 3);
INSERT INTO `coupons_detail` VALUES (11, 3, '2019-10-31 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (12, 1, '2019-10-31 10:45:16', 1, 4);
INSERT INTO `coupons_detail` VALUES (13, 4, '2019-10-29 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (14, 4, '2019-10-18 10:45:16', 0, 6);
INSERT INTO `coupons_detail` VALUES (15, 4, '2019-10-23 10:45:16', 1, 1);
INSERT INTO `coupons_detail` VALUES (16, 4, '2019-10-29 10:45:16', 0, 5);
INSERT INTO `coupons_detail` VALUES (17, 5, '2019-10-17 10:45:16', 1, 1);
INSERT INTO `coupons_detail` VALUES (18, 5, '2019-10-26 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (19, 1, '2019-10-15 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (20, 1, '2019-10-15 10:45:16', 0, 1);
INSERT INTO `coupons_detail` VALUES (21, 1, '2019-10-23 13:33:50', 0, 21);
INSERT INTO `coupons_detail` VALUES (22, 1, '2019-10-23 13:34:06', 0, 26);
INSERT INTO `coupons_detail` VALUES (23, 1, '2019-10-23 13:52:41', 0, 22);
INSERT INTO `coupons_detail` VALUES (24, 1, '2019-10-23 13:52:46', 0, 23);
INSERT INTO `coupons_detail` VALUES (25, 1, '2019-10-23 13:52:56', 0, 22);
INSERT INTO `coupons_detail` VALUES (26, 1, '2019-10-23 13:53:03', 0, 22);

-- ----------------------------
-- Table structure for depay
-- ----------------------------
DROP TABLE IF EXISTS `depay`;
CREATE TABLE `depay`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '预付优惠表',
  `pay_money` int(11) NOT NULL COMMENT '充值金额',
  `song_money` int(11) NOT NULL COMMENT '赠送金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of depay
-- ----------------------------
INSERT INTO `depay` VALUES (1, 100, 50);
INSERT INTO `depay` VALUES (2, 100, 50);
INSERT INTO `depay` VALUES (3, 200, 100);
INSERT INTO `depay` VALUES (4, 200, 100);
INSERT INTO `depay` VALUES (5, 200, 100);
INSERT INTO `depay` VALUES (6, 200, 100);
INSERT INTO `depay` VALUES (7, 300, 150);
INSERT INTO `depay` VALUES (8, 300, 150);
INSERT INTO `depay` VALUES (9, 300, 150);
INSERT INTO `depay` VALUES (10, 300, 150);

-- ----------------------------
-- Table structure for evaluate
-- ----------------------------
DROP TABLE IF EXISTS `evaluate`;
CREATE TABLE `evaluate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户评价信息表的id',
  `list_id` int(11) NOT NULL COMMENT '菜品的id 外键-餐品基本信息表',
  `content` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '评价内容',
  `orders_id` int(11) NOT NULL COMMENT '订单id 外键-订单表',
  `add_time` datetime(0) NOT NULL COMMENT '评价时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of evaluate
-- ----------------------------
INSERT INTO `evaluate` VALUES (2, 1, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (3, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (4, 3, '这到红烧肉真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (5, 5, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (6, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (7, 13, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (8, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (9, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (10, 11, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (11, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (12, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (13, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (14, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (15, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (16, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (17, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (18, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (19, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (20, 2, '这到红烧狮子头真好吃', 1, '2019-10-16 08:28:00');
INSERT INTO `evaluate` VALUES (21, 40, '213123', 59, '2019-10-21 00:51:32');
INSERT INTO `evaluate` VALUES (22, 44, '真好吃', 61, '2019-10-21 09:55:22');

-- ----------------------------
-- Table structure for gonggao
-- ----------------------------
DROP TABLE IF EXISTS `gonggao`;
CREATE TABLE `gonggao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '餐馆广告表的id',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '标题',
  `add_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '发布人',
  `add_time` datetime(0) NOT NULL COMMENT '发布时间',
  `last_update_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '修改人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告详情',
  `state` tinyint(4) NOT NULL COMMENT '0：隐藏，1：显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gonggao
-- ----------------------------
INSERT INTO `gonggao` VALUES (4, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-18 16:52:19', '今天的饭真的真的太难吃了，还死贵', 0);
INSERT INTO `gonggao` VALUES (5, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (6, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (7, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (8, '今天的饭真难吃', '高浩', '2019-10-17 20:55:58', '夏克斌', '2019-10-18 16:50:55', '阿三打撒', 1);
INSERT INTO `gonggao` VALUES (9, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (10, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (11, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (12, '今天的饭真难吃', '王海龙', '2019-10-17 20:55:58', '王海龙', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (13, '今天的饭真难吃', '师文博', '2019-10-17 20:55:58', '师文博', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (14, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (15, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (17, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (18, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (19, '今天的饭真难吃', '夏克斌', '2019-10-17 20:55:58', '夏克斌', '2019-10-17 20:56:15', '今天的饭真的真的太难吃了，还死贵', 1);
INSERT INTO `gonggao` VALUES (20, '今天的饭真好吃', '王海龙', '2019-10-17 20:55:58', '王海龙', '2019-10-17 20:56:15', '今天的饭真的真的太好吃了，还便宜', 1);
INSERT INTO `gonggao` VALUES (21, '萨大', '夏克斌', '2019-10-18 15:37:33', '夏克斌', '2019-10-18 15:37:33', '啊是大', 1);
INSERT INTO `gonggao` VALUES (22, '阿三', '夏克斌', '2019-10-18 15:41:49', '夏克斌', '2019-10-18 15:41:49', '阿三', 1);
INSERT INTO `gonggao` VALUES (23, '阿三', '夏克斌', '2019-10-18 15:41:56', '夏克斌', '2019-10-18 15:41:56', '阿三', 1);
INSERT INTO `gonggao` VALUES (24, '阿三', '夏克斌', '2019-10-18 15:42:44', '夏克斌', '2019-10-18 15:42:44', '啊是大', 1);
INSERT INTO `gonggao` VALUES (25, '啊是大', '夏克斌', '2019-10-18 15:44:39', '夏克斌', '2019-10-18 15:44:39', '阿三的', 1);

-- ----------------------------
-- Table structure for grantee
-- ----------------------------
DROP TABLE IF EXISTS `grantee`;
CREATE TABLE `grantee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员基本信息表的id',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `pwd` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `part` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '模块',
  `win_id` tinyint(4) NOT NULL COMMENT '权限：0：全部权限，-1为没有权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of grantee
-- ----------------------------
INSERT INTO `grantee` VALUES (1, '夏克斌', '6105b4157fb5de0cf528cfb7f28d41d9', '食堂经理', 0);
INSERT INTO `grantee` VALUES (2, '夏克斌', '6105b4157fb5de0cf528cfb7f28d41d9', '食堂经理', 0);
INSERT INTO `grantee` VALUES (3, '王海龙', '6105b4157fb5de0cf528cfb7f28d41d9', '食堂经理', 0);
INSERT INTO `grantee` VALUES (4, '师文博', '5b905a4a236e6a39d05c2fc3971336eb', '项目经理', 0);
INSERT INTO `grantee` VALUES (5, '高浩', '5b905a4a236e6a39d05c2fc3971336eb', '前端经理', 0);
INSERT INTO `grantee` VALUES (6, '王海龙', '6105b4157fb5de0cf528cfb7f28d41d9', '食堂经理', 0);
INSERT INTO `grantee` VALUES (7, '王海龙', '6105b4157fb5de0cf528cfb7f28d41d9', '食堂经理', 0);
INSERT INTO `grantee` VALUES (8, '师文博', '5b905a4a236e6a39d05c2fc3971336eb', '项目经理', 0);
INSERT INTO `grantee` VALUES (9, '师文博', '5b905a4a236e6a39d05c2fc3971336eb', '项目经理', 0);
INSERT INTO `grantee` VALUES (10, '高浩', '5b905a4a236e6a39d05c2fc3971336eb', '前端经理', 0);

-- ----------------------------
-- Table structure for list
-- ----------------------------
DROP TABLE IF EXISTS `list`;
CREATE TABLE `list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '餐品',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜名',
  `price` int(11) NOT NULL COMMENT '价格',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '图片',
  `intro` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '简介',
  `unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '计量单位',
  `win_id` int(11) NOT NULL COMMENT '所属窗口 窗口id 外键-餐品基本信息表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of list
-- ----------------------------
INSERT INTO `list` VALUES (1, '红烧狮子头', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '这是一道非常美味的菜肴', '2个', 1);
INSERT INTO `list` VALUES (2, '豆腐鲈鱼煲', 111, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '吃', '5', 1);
INSERT INTO `list` VALUES (3, '鸡肉', 222, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '啃', '6', 9);
INSERT INTO `list` VALUES (5, '青菜炒青菜', 12, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '好吃', '份', 1);
INSERT INTO `list` VALUES (8, '红烧王丁耀', 1, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '一般人吃不下去', '份', 1);
INSERT INTO `list` VALUES (10, '刀削面', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '好吃', '份', 2);
INSERT INTO `list` VALUES (11, '拉面', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '好吃', '份', 2);
INSERT INTO `list` VALUES (12, '果木烤鸭', 24, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '贼好吃', '份', 3);
INSERT INTO `list` VALUES (13, '黄焖鸡', 21, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg\r\nhttp://p0.meituan.net/dealwatera/d8f969590f2c191757994a248129db8973004.jpg@467h_702w_2e_90Q%7Cwatermark=1&&p=4', '超好吃', '份', 1);
INSERT INTO `list` VALUES (14, '饺子', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '还可以', '份', 2);
INSERT INTO `list` VALUES (15, '翅根咖喱饭', 15, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg\r\n', '还行', '份', 4);
INSERT INTO `list` VALUES (16, '炸酱面', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '不错不错', '份', 2);
INSERT INTO `list` VALUES (17, '冷面', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '有点冷', '份', 2);
INSERT INTO `list` VALUES (18, '番茄牛腩意面', 12, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '美味', '份', 2);
INSERT INTO `list` VALUES (19, '小鸡炖蘑菇', 20, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '好吃', '份', 3);
INSERT INTO `list` VALUES (20, '过桥米线', 10, '\r\nhttps://wanghailongwang.oss-cn-beijing.aliyuncs.com/images/1F79FBA2D6435184D4E14E80AB557C60.jpg', '好吃', '份', 2);
INSERT INTO `list` VALUES (21, '炒田螺', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13781_s.jpg', '广州名菜', '份', 1);
INSERT INTO `list` VALUES (22, '叉烧包', 14, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13786_s.jpg', '叉烧包是广东地区具有代表性的特色传统西关名点之一，是粤式早茶的“四大天王（虾饺、干蒸烧卖、叉烧包、蛋挞）”之一。', '份', 1);
INSERT INTO `list` VALUES (23, '沙河粉', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13790_s.jpg', '沙河粉，简称河粉，外文名Shahe noodles，是广东省广州地区一种大众化的传统名吃之一，主要食材为大米。', '份', 10);
INSERT INTO `list` VALUES (24, '烤小猪', 467, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13806_s.jpg', '烤小猪是元谋一带具有地方风味的传统佳肴，以小猪为主要原料，', '份', 1);
INSERT INTO `list` VALUES (25, '金丝烩鱼翅', 234, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13809_s.jpg', '全丝烩鱼翅是福建省传统的地方名菜，属于闽菜系。', '份', 1);
INSERT INTO `list` VALUES (26, '武昌鱼', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13819_s.jpg', '武昌鱼（Megalobrama amblycephala），学名团头鲂，俗称鳊鱼、草鳊等，属鲤形目（Cypriniformes），鲤科（Cyprinidae），鲌亚科（Cultrinae），鲂属（Megalobrama）。', '份', 2);
INSERT INTO `list` VALUES (27, '老通城豆皮', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13826_s.jpg', '豆皮是一种湖北武汉著名的传统小吃，制馅讲究，煎制精细，煎好后油光闪亮，色黄味香。', '份', 7);
INSERT INTO `list` VALUES (28, '棉花糖', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13829_s.jpg', '棉花糖指市场上一种软性糖果,疏松多孔,具有一定的弹性和韧性,因口感和质地与棉花相似而得名。', '份', 1);
INSERT INTO `list` VALUES (29, '老大兴鲴鱼', 78, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13855_s.jpg', '老大兴”是坐落在汉口利济北路靠近解放大道航空路口的老大兴园酒楼的简称。', '份', 7);
INSERT INTO `list` VALUES (30, '四季美汤包', 56, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13952_s.jpg', '四季美汤包是湖北武汉著名的小吃，属于苏式汤包。', '份', 1);
INSERT INTO `list` VALUES (31, '杭州煨鸡', 89, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13963_s.jpg', '杭州煨鸡是江浙一道滋补的菜肴，其做法很简单，营养很丰富。', '份', 3);
INSERT INTO `list` VALUES (32, '酱汁肉', 79, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1584_s.jpg', '酱汁肉，是江苏省苏州市的传统名菜，卤酱菜谱之一。其特色是上口酥润，香味浓郁，皮糯肉烂, 入口即化，肥而不腻，而色泽鲜艳。', '份', 8);
INSERT INTO `list` VALUES (33, '樱桃肉', 79, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1590_s.jpg', '樱桃肉是一道江苏省苏州市的传统名菜之一，属于苏菜系。', '份', 1);
INSERT INTO `list` VALUES (34, '糕团', 79, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_77s.jpg', '糕团小点是南京的传统小吃，南京人吃甜食讲究甜而不腻，糯而不粘，方称为甜品之上乘。', '份', 6);
INSERT INTO `list` VALUES (35, '酱鸡', 85, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_78s.jpg', '酱鸡是一道乌镇的特色名菜，外观酱红油亮，入口脆嫩鲜美，后味无尽。选用本地农民当年放养的土种雌鸡作原料。', '份', 1);
INSERT INTO `list` VALUES (36, '春卷', 64, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_86s.jpg', '春卷，又称春饼、春盘、薄饼。是中国民间节日的一种传统食品，流行于中国各地，在江南等地尤盛。在中国南方，过春节不吃饺子，吃春卷和芝麻汤圆。当年放养的土种雌鸡作原料。', '份', 5);
INSERT INTO `list` VALUES (37, '西湖醋鱼', 53, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_92s.jpg', '西湖醋鱼（West Lake Fish in Vinegar Gravy），别名为叔嫂传珍，宋嫂鱼，是浙江杭州饭店的一道传统地方风味名菜。', '份', 7);
INSERT INTO `list` VALUES (38, '杭州煨鸡', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_94s.jpg', '杭州煨鸡是江浙一道滋补的菜肴，其做法很简单，营养很丰富。', '份', 1);
INSERT INTO `list` VALUES (39, '开煲狗肉', 97, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_95s.jpg', '狗肉煲属于砂锅/火锅，主要原料是狗肉，口味是香，工艺是炖，难度属于中级。', '份', 8);
INSERT INTO `list` VALUES (40, '金鱼饺', 35, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/pic450_s.jpg', '金鱼饺是湖北省传统的地方小吃，属于鄂菜系。形似金鱼，晶莹剔透，馅成鲜香。', '份', 1);
INSERT INTO `list` VALUES (41, '玉兔饼', 35, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/pic781_s.jpg', '玉兔茄饼是一道美食，制作材料有茄子500克等。', '份', 3);
INSERT INTO `list` VALUES (42, '玲珑鱼脆羹', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/pic1311_s.jpg', '土魠鱼羹是一道以土魠鱼为主要材料制作而成的一道菜品。', '份', 1);
INSERT INTO `list` VALUES (43, '玲珑鱼脆羹', 80, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13921_s.jpg', '三色凉糕是一道美食，制作材料有糯米配菜(中级)等。', '份', 10);
INSERT INTO `list` VALUES (44, '香麻麻品酥', 80, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13772_s.jpg', '我也不早是啥好吃的', '份', 9);
INSERT INTO `list` VALUES (45, '板鸭', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13800_s.jpg', '板鸭，中国南方地区名菜，亦是江苏、福建、江西、湖南、安徽等省的特产。', '份', 1);
INSERT INTO `list` VALUES (46, '锅巴菜', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13817_s.jpg', '“嘎巴菜”本名应为“锅巴菜”，天津话称“嘎巴菜”。锅巴菜以绿豆、小米水磨成浆，摊成薄厚均匀的煎饼（天津人俗称锅巴）、晾干后切成柳叶形小条，浸在素卤之中，盛碗，点上芝麻酱、腐乳汁、辣油、辣糊、撒上卤香干片和香菜沫等六种小料制成。成品五彩缤纷，多味混合，素香扑鼻，锅巴香嫩有咬劲，味美...', '份', 1);
INSERT INTO `list` VALUES (47, '小站大米', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13820_s.jpg', '小站米，产自天津小站地区的一种特产稻米。小站开发，仅有120年。但小站稻却是自北宋以来，千余年漫长的历史进程中，在历代先哲垦拓北海，法式南方的实践递鍃推进下，得以问世的，是整个天津屯垦史上的一个突出环节。', '份', 3);
INSERT INTO `list` VALUES (48, '天津紫蟹', 56, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13821_s.jpg', '天津紫蟹是中华绒蟹的一种，它体小，仅有一颗大衣纽扣大小。揭开蟹盖，蟹黄呈猪肝紫色，煮熟后变成橘红色，味极鲜美。紫蟹都产在寒风凛冽的冬季，因此，常常用于什锦火锅。', '份', 1);
INSERT INTO `list` VALUES (49, '耳朵眼炸糕', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13827_s.jpg', '耳朵眼炸糕（英文名：Erduoyan fried rice cake）为天津三绝食品之一，始创于清朝光绪年间（1900年），旧时因店铺紧靠耳朵眼胡同而得名。', '份', 4);
INSERT INTO `list` VALUES (50, '狗不理包子', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13835_s.jpg', '狗不理包子（Go Believe/Goubuli steamed bun）是一道由面粉、猪肉等材料制作而成的小吃，始创于公元1858年（清朝咸丰年间）.至今有100多年历史的它为“天津三绝”之首，', '份', 1);
INSERT INTO `list` VALUES (51, '大闸蟹', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13846_s.jpg', '中华绒螯蟹为日本绒螯蟹中华亚种，又名河蟹、大闸蟹，是我国传统的水产珍品。中华绒螯蟹常穴居江、河、湖荡泥岸，昼匿夜出，以动物尸体或谷物为食。到每年秋季，长得比较丰满，常回游到近海繁殖，母体所带的卵在翌年3-5月间孵化，幼体经过多次变态，发育成为幼蟹，再溯江、河而上，在淡水中继续生长', '份', 10);
INSERT INTO `list` VALUES (52, '油炸臭干子', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13866_s.jpg', '油炸臭干子是一道美食，制作原料主要有臭豆腐乳、豆腐。', '份', 1);
INSERT INTO `list` VALUES (53, '鸡鸭血汤', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13867_s.jpg', ' 鸡鸭血汤是一道以鸡鸭血为主要原料，精心煮制而成的传统名吃，具有补血、解毒的功效。', '份', 6);
INSERT INTO `list` VALUES (54, '三黄鸡', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13868_s.jpg', '“三黄鸡”的名字由朱元璋钦赐。在国家农业部权威典籍《中国家禽志》一书中排名首位，该鸡属农户大自然放养。其肉质细嫩，味道鲜美，营养丰富，在国内外享有较高的声誉。', '份', 6);
INSERT INTO `list` VALUES (55, '五香豆', 67, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13874_s.jpg', '五香豆又称奶油五香豆，是上海市知名的传统小吃，该食品由上海老城隍庙郭记兴隆五香豆店首创，故又称城隍庙奶油五香豆。', '份', 1);
INSERT INTO `list` VALUES (56, '五香豆', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13875_s.jpg', '蜜饯也称果脯，古称蜜煎。中国民间糖蜜制水果食品。流传于各地，历史悠久。', '份', 1);
INSERT INTO `list` VALUES (57, '盐水火腿', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13881_s.jpg', '盐水火腿又叫西式火腿。因形状而分为圆腿和方腿，又有熏烟和不熏烟的区别。它是以净瘦肉为原料，经盐腌提取可溶性蛋白、滚揉破坏肌肉组织结构、装模成型后煮制而.', '份', 4);
INSERT INTO `list` VALUES (58, '王致和臭豆腐', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13884_s.jpg', '王致和臭豆腐是老北京的传统小吃，属于豆腐乳的一种。颜色呈青色，闻起来臭，吃起来香。发明人是安徽人王致和，流传至今已有300多年。他原是个文人，多次进京赶考不中，改为以制豆腐为生，并创制出臭豆腐。臭豆腐曾作为御膳小菜送往宫廷，', '份', 1);
INSERT INTO `list` VALUES (59, '桂花陈酒', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13890_s.jpg', '桂花陈酒，原名“桂花东酒”，在中国已有三千多年的酿造历史。但这种酒一直“栖身”于深宫禁苑，后来，酿制此酒的技艺失传。', '份', 10);
INSERT INTO `list` VALUES (60, '打卤面', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13891_s.jpg', '打卤面是山西、山东一代的传统面食。打卤面做法多样，风味不一，用料也多种多样，随用料、做法不同，亦有不同风味。', '份', 1);
INSERT INTO `list` VALUES (61, '萨奇马', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13894_s.jpg', '萨奇，日本漫画《海贼王》及其衍生作品中的角色。白胡子海贼团的第四队队长，但却因为得到黑胡子梦寐以求的黑暗果实而被黑胡子杀害。此外，他其实也是一名厨师。', '份', 4);
INSERT INTO `list` VALUES (62, '小窝头', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13904_s.jpg', '小窝头，是北京地区特色传统名点之一。制作时将小米面、敉子面、玉米面、栗子面混合，做成圆锥形，每个底部都有一个圆洞，小巧玲珑，蒸熟后呈金黄色。', '份', 1);
INSERT INTO `list` VALUES (63, '仿膳宫廷菜', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13911_s.jpg', '小窝头，是北京地区特色传统名点之一。制作时将小米面、敉子面、玉米面、栗子面混合，做成圆锥形，每个底部都有一个圆洞，小巧玲珑，蒸熟后呈金黄色。', '份', 5);
INSERT INTO `list` VALUES (64, '涮羊肉', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13913_s.jpg', '涮羊肉，又称“羊肉火锅”，始于元代，兴起于清代，早在18世纪，康熙、乾隆二帝所举办的几次规模宏大的“千叟宴”，内中就有羊肉火锅，后流传至市肆。《旧都百话》云：“羊肉锅...', '份', 1);
INSERT INTO `list` VALUES (65, '天津小枣', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1547_s.jpg', '天津小枣是中国小枣中优良品种之一。果实呈长圆形，表面深红色，皮薄肉厚核小，含糖分高，枣肉可拉出金色糖丝，品质与金丝小枣相同。天津小枣以静海、北大港、蓟县等地栽培最多。', '份', 3);
INSERT INTO `list` VALUES (66, '天津对虾', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1550_s.jpg', '天津对虾, 又称大虾、明虾、东方大虾。因其在很久之前出售或统计捕获时以\"对\"为单位计算而得名。天津对虾身长体壮，壳薄肉肥，光滑明亮，味道鲜美，风味独特，蛋白质含量高，营...', '份', 3);
INSERT INTO `list` VALUES (67, '天津红果', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1586_s.jpg', '天津红果', '份', 3);
INSERT INTO `list` VALUES (68, '天津板栗', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1587_s.jpg', '津板栗，别名红油皮栗子、油栗、天津甘栗。产地主要集中在蓟县小港、小营等乡。同时，北京、唐山、承德等地的山区，也生长大量栗树，所产栗子，品质特优，因为过去这些地区生..', '份', 3);
INSERT INTO `list` VALUES (69, '天津核桃', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1589_s.jpg', '天津板栗，别名红油皮栗子、油栗、天津甘栗。产地主要集中在蓟县小港、小营等乡。同时，北京、唐山、承德等地的山区，也生长大量栗树，所产栗子，品质特优，因为过去这些地区生...', '份', 3);
INSERT INTO `list` VALUES (70, '天津鸭梨', 34, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_3s.jpg', '中国梨类优良品种之一。果形美观匀称，似鸭蛋，梗处有突起，果柄歪斜如鸭嘴，故名鸭梨。', '份', 3);
INSERT INTO `list` VALUES (71, '天津鸭梨', 32, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/mogu_88s.jpg', '赤小豆，又名红小豆、赤豆、朱豆，为豆科一年生半缠绕草本植物赤小豆的成熟种子，全国各地均有栽培，夏秋荚果成熟时采收，晒干，收集种子备用。', '份', 3);
INSERT INTO `list` VALUES (72, '蚝油炒饭', 21, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/zzpic20488_s.jpg', '原料：剩米饭、胡萝卜、火腿肠、洋葱、黄瓜、木耳、鸡蛋、蚝油、盐。', '份', 4);
INSERT INTO `list` VALUES (73, '炒米饭', 21, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102161627101.jpg', '原料：剩米饭、西红柿、鸡蛋、椒、红泡椒、盐。', '份', 4);
INSERT INTO `list` VALUES (74, '蛋炒饭', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201012220910275.jpg', '原料：米饭、葱、鸡蛋、花生油、咸鸡蛋、鸡精、盐。', '份', 9);
INSERT INTO `list` VALUES (75, '什锦炒饭', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/2018050915258605026838197577.jpg', '原料：隔夜剩米饭、鸡蛋、火腿肠、胡萝卜、黄瓜、葱碎', '份', 4);
INSERT INTO `list` VALUES (76, '皮蛋瘦肉粥', 10, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/20180424152455915892813.jpg', '原料：大米、猪瘦肉、皮蛋、姜丝、葱花、食用油、盐。', '份', 4);
INSERT INTO `list` VALUES (77, '生菜姜片粥', 10, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201103231107303.jpg', '原料：大米、生菜、肉片、枸杞、葱棵。', '份', 4);
INSERT INTO `list` VALUES (78, '豆腐米饭三明治', 10, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102171604084.jpg', '原料：米饭、葱、豆腐、鸡蛋、芝麻、纯净水、盐、韩式辣酱、醋精、糖、韩式香油。', '份', 9);
INSERT INTO `list` VALUES (79, '简单饭包', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201101101911461.jpg', '原料：米饭、生菜叶、葱、香菜、鸡蛋酱、肉酱、土豆泥。', '份', 4);
INSERT INTO `list` VALUES (81, '宫保鸡丁盖饭', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/20180424152456121734913.jpg', '原料：米饭、生菜叶、葱、香菜、鸡蛋酱、肉酱、土豆泥。', '份', 4);
INSERT INTO `list` VALUES (82, '泡菜炒饭', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201103240933141.jpg', '原料：米饭、泡菜、青菜、烤肠、胡萝卜、香油、盐、鸡精。', '份', 8);
INSERT INTO `list` VALUES (83, '炒饭', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102151624418.jpg', '原料：米饭、肉、洋葱、蘑菇、胡萝卜丝、菠菜。', '份', 4);
INSERT INTO `list` VALUES (84, '海鲜饭', 13, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201012061348330.jpg', '原料：各种海鲜、米饭、菜花、北豆腐、葱、高汤。', '份', 4);
INSERT INTO `list` VALUES (85, '腊八面', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102151642556.jpg', '原料：胡萝卜、豆角、黄豆、花生、玉米粒、面粉。', '份', 2);
INSERT INTO `list` VALUES (86, '蛋包饭', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181451586.jpg', '原料：虾仁、胡萝卜、黄瓜、隔夜饭、生菜、鸡蛋、盐、甜辣酱。', '份', 4);
INSERT INTO `list` VALUES (87, '米饭蛋卷', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181122085.jpg', '原料：米饭、黄瓜、火腿肠、洋葱、薄荷叶、鸡蛋、盐。', '份', 4);
INSERT INTO `list` VALUES (88, '辣白菜土豆泥拌饭', 17, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102111308582.jpg', '原料：辣白菜、土豆、青椒、洋葱、盐、辣白菜泡菜水。', '份', 10);
INSERT INTO `list` VALUES (89, '番茄泡饭', 17, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181236200%20(1).jpg', '原料：剩饭、番茄、鸡蛋、姜蒜葱。', '份', 4);
INSERT INTO `list` VALUES (90, '米饭饼', 7, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181451586.jpg', '原料：米饭、鸡蛋、青豆、蟹棒、火腿肠。', '份', 4);
INSERT INTO `list` VALUES (91, '酱香煎米团', 7, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/20171010150760369420213.jpg', '原料：米饭、大喜大烤肉酱、食油。', '份', 10);
INSERT INTO `list` VALUES (92, '菠萝虾仁炒饭', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/2018042615247349676759702111.jpg', '原料：米饭、菠萝、海虾、胡萝卜、青豌豆、鸡蛋、盐、鸡粉。', '份', 4);
INSERT INTO `list` VALUES (93, '美味蛋炒饭', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181201288.jpg', '原料：米饭、胡萝卜、豌豆、火腿肠、青瓜、鸡蛋、十三香粉、盐、鸡精。', '份', 4);
INSERT INTO `list` VALUES (94, '粽香八宝饭', 30, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181036330.jpg', '原料：糯米、粽叶、冰糖、蜜枣、枸杞、葡萄干、莲子、豌豆、花生、红豆沙、山楂糕、黄油。', '份', 4);
INSERT INTO `list` VALUES (95, '云南过桥米线', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/201102181028064.jpg', '味美而不腻', '份', 5);
INSERT INTO `list` VALUES (96, '酸菜米线', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guoqiaomixian-017.jpg', '酸菜米线是云南风味的特色小吃，制作原料主要有米线、云南酸菜等。其味道酸爽，制作简单，深受云南人喜爱 ，特别是夏天，乃解暑消饿首选。', '份', 5);
INSERT INTO `list` VALUES (97, '麻辣米线', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guoqiaomixian-009.jpg', '麻辣米线是一道色香味俱全的传统小吃，属于川菜系。青花椒放入锅中，小火干炒，炒至变色变酥。放入案板晾凉，用擀面杖压成碎末。', '份', 5);
INSERT INTO `list` VALUES (99, '清汤米线', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guoqiaomixian-012.jpg', '用量米线一小把(袋装)清汤一碗(鸡汤)鸡胸肉一小块青菜一小把蘑菇（鲜蘑）豆芽干豆腐丝木耳四川泡菜黑胡', '份', 5);
INSERT INTO `list` VALUES (100, '李记米线', 17, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guoqiaomixian-005.jpg', '独特的味道只有吃了才知道', '份', 5);
INSERT INTO `list` VALUES (101, '酸辣米线', 18, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guoqiaomixian-001.jpg', '酸辣酸辣的，跟酸菜米线有点点的类似，吃了保准让你还想再吃第二次', '份', 5);
INSERT INTO `list` VALUES (103, '张亮麻辣烫', 10, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D46387474%2C1348010094%26fm%3D26%26gp%3D0.jpg', '娃娃菜 牛百叶 黑木耳 油麦菜 淡水卷 花菜 平菇 鱼豆腐 红萝卜 日本豆腐 素鸭 冬瓜 木耳菜 香菇贡丸 小青菜 金针菇 ...', '份', 6);
INSERT INTO `list` VALUES (104, '咖喱鸡', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/bpic13919_s.jpg', '咖喱鸡是以鸡肉、土豆、咖喱粉、胡萝卜、洋葱为主要食材制作而成的一道菜肴。具备以咸鲜口味为主，略带甜辣，咖喱味浓厚的口感。', '份', 7);
INSERT INTO `list` VALUES (105, '日式肥牛', 14, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1-1FP414410E06.jpg', '独特的肥牛饭，吃起来还有淡淡的牛肉味', '份', 7);
INSERT INTO `list` VALUES (106, '鱼香肉丝 ', 14, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guojisp_153s.jpg', '鱼香肉丝是一道常见川菜。鱼香，是四川菜肴主要传统味型之一。成菜具有鱼香味，其味是调味品调制而成。此法源出于四川民间独具特色的烹鱼调味方法，而今已广泛用于川味的熟菜中', '份', 7);
INSERT INTO `list` VALUES (107, '巴黎龙虾', 14, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/hpic1585_s.jpg', '武汉市巴厘龙虾餐饮管理有限公司于-在武汉市江汉区工商行政管理局（质量技术监督局）登记成立。法定代表人周芬，公司经营范围包括；中餐类制售，含凉菜，不含裱...', '份', 8);
INSERT INTO `list` VALUES (108, '鸡肝牛排', 67, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D1218799460%2C3412385506%26fm%3D85%26app%3D63%26f%3DJPEG.jpg', '棕色牛排汁 (Sauce Espanole),烤鳀鱼纽约客牛排,黑椒牛排,煎面包渣小牛排,沙朗牛排佐山药胡椒汁,和式汉堡牛排,鸡肝豆苗汤,慈禧式煎小块牛排,菠菜鸡肝粉丝汤,黑', '份', 8);
INSERT INTO `list` VALUES (109, '鸡丁沙拉', 50, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D79731405%2C3255222078%26fm%3D26%26gp%3D0.jpg', '鸡丁沙拉是以熟鸡丁、去皮熟马铃薯、熟豌豆、番茄、沙拉油为主要原料制成的沙拉。', '份', 8);
INSERT INTO `list` VALUES (110, '烤大虾苏夫力', 100, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D3267702794%2C2194007619%26fm%3D115%26gp%3D0.jpg', '北国奶汁烤大虾,粗盐烤大虾,奶油烤大虾,烧烤大虾配辣味花生柠檬酱,香草烤大虾,干烤大虾,咸肉烤大虾,烤大虾,烹大虾,大虾三食,靠大虾,焅大虾,大虾青丝面,炸大虾..', '份', 8);
INSERT INTO `list` VALUES (111, '烤羊马鞍', 98, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D855200776%2C3977164199%26fm%3D26%26gp%3D0.jpg', '常用食材——羊身上的一部分，由于羊马鞍的形态和马鞍子相似，故称羊马鞍又称羊上腰，此部位无肋骨，肉质鲜嫩，用途广泛，可加工成多种羊排肉。', '份', 8);
INSERT INTO `list` VALUES (112, '明治排', 78, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D1684951525%2C3600007839%26fm%3D179%26app%3D42%26f%3DJPEG.jpg', '明治牛排，特点是黄白相间，香鲜可口。', '份', 8);
INSERT INTO `list` VALUES (114, '焗馄饨', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D2018270710%2C2029524445%26fm%3D111%26gp%3D0.jpg', '用量做好冷冻的馄饨若干马苏里拉芝士适量1. 烤盘垫一张油纸，摆入馄饨（我的是前些天做好冷冻在冰箱的），180度7分钟2. 拿出烤盘将馄饨们集合，撒入芝士。喜欢的人可以多撒一些。200度三分钟。', '份', 8);
INSERT INTO `list` VALUES (115, '比萨饼', 67, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D4265423458%2C2022588657%26fm%3D202%26mola%3Dnew%26crop%3Dv1.jpg', '比萨（Pizza），又称为比萨饼、匹萨、批萨、披萨，是一种发源于意大利的食品，在全球颇受欢迎。比萨饼的通常做法是用发酵的圆面饼上面覆盖番茄酱、奶酪以及其他配料，并由烤炉烤制而成。奶酪通常用莫萨里拉干酪，也有混用几种奶酪的形式，包括帕马森干酪、罗马乳酪（romano）、意大利乡村软酪（ricotta...', '份', 8);
INSERT INTO `list` VALUES (116, '美式牛扒', 67, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D734688384%2C593224761%26fm%3D26%26gp%3D0.jpg', '美式牛扒，菜肴名称，取名独特，色感口感俱佳，深受当地民众喜爱。', '份', 8);
INSERT INTO `list` VALUES (117, '什锦冷盘', 43, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D3432158855%2C1259413202%26fm%3D15%26gp%3D0.jpg', '什锦冷菜,东北什锦凉菜,凉面冷盘,热门冷盘【卤牛肉】,烩什锦,烧什锦,素什锦,什锦玉米,什锦土菜香,什锦烤麸,什锦核果饼干,什锦天妇罗,什锦沙拉,什锦豆腐,什锦锅巴.', '份', 8);
INSERT INTO `list` VALUES (118, '鱼肉包子', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D2603633896%2C2714667093%26fm%3D26%26gp%3D0.jpg', '萝卜缨羊肉包子,白菜酱肉包子,酸菜肉包子,芽菜瘦肉包子,回锅肉包子,茴香茄子鱼肉酱,萝卜鱼肉卷,扁鱼肉羹,鱼肉大葱饺,薯泥鱼肉糕,鱼肉芝麻饼,清蒸鱼肉狮子头,咸鱼...', '份', 8);
INSERT INTO `list` VALUES (119, '红烧猪蹄', 12, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1306536264861.jpg%40196w_126h_99q_1e_1c.jpg', '味浓适口，肥而不腻。', '份', 9);
INSERT INTO `list` VALUES (120, '酸菜鱼', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1272009488187.jpg%40288w_216h_99q_1e_1c.jpg', '制作酸菜鱼的鱼最好选用淡水鱼，肉质的口感会更好。  2、片鱼的时候刀一定要快，事先最好先磨磨刀，事半功倍。', '份', 9);
INSERT INTO `list` VALUES (121, '拔丝山药', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1281412026187.jpg%40288w_216h_99q_1e_1c.jpg', '外甜内滑，营养丰富。', '份', 9);
INSERT INTO `list` VALUES (122, '香菇土豆炖鸡肉', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1269477828208.jpg%40288w_216h_99q_1e_1c.jpg', '洗净香菇的小窍门： 香菇的菌伞下面有很多褶皱，里面也有脏东西，浸泡在淀粉水或是面粉水中一段时间就很容易洗净褶皱里面的脏东西。', '份', 9);
INSERT INTO `list` VALUES (123, '蜜制叉烧鸡翅', 24, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1363056805093.jpg%40288w_216h_50q_1e_1c.jpg', ' 鸡翅与少许盐，料酒，生姜，葱，少许蜂蜜，2大勺李锦记叉烧酱，一点生抽', '份', 9);
INSERT INTO `list` VALUES (124, '番茄土豆炖牛肉', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1301784181451.jpg%40288w_216h_99q_1e_1c.jpg', '红汤酸甜，肉质嫩滑。', '份', 9);
INSERT INTO `list` VALUES (125, ' 酱鸡爪', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1269464708254.jpg%40288w_216h_99q_1e_1c.jpg', '鸡爪，糖，甜辣酱，老抽，酱油，姜片，花椒，大料，盐，料酒', '份', 9);
INSERT INTO `list` VALUES (126, '鸡蛋羹', 5, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1304532797781.jpg%40288w_216h_99q_1e_1c.jpg', ' 主料：鸡蛋，盐，水，油，葱花', '份', 9);
INSERT INTO `list` VALUES (127, '红烧猪尾', 23, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/1281498994125.jpg%40288w_216h_99q_1e_1c.jpg', ' 猪尾500克(食部)，胡萝卜50克，土豆50克。调料葱段，姜片，蒜片各适量，精盐5克，酱油15克，料酒25克，白糖70克，八角2粒，花椒少许，淀粉20克，植物油20克。', '份', 9);
INSERT INTO `list` VALUES (128, ' 熬乳茶', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D3418154792%2C1570717632%26fm%3D26%26gp%3D0.jpg', '熬茶拜唐阿，清代光禄寺负责煎熬乳茶的执事人。寺置数人，原设“熬茶蒙古”亦数人。拜唐阿(执事人)，以八旗子弟充。拨入该寺者，专以熬煎乳茶，故称。光禄寺茶汤.', '份', 10);
INSERT INTO `list` VALUES (129, ' 金糕', 15, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D552819604%2C3207372668%26fm%3D115%26gp%3D0.jpg', '金糕又名京糕、山楂糕，特色糕点之一，它以山楂为主料，佐以白砂糖、桂花精制而成。它除了切块吃之外，还可撒上白糖，或与梨丝拌着吃，酸甜绵软，味美可口。酥炸金糕经过油炸处理，又多了一分香甜酥脆的口感。是满汉全席的菜品之一。', '份', 10);
INSERT INTO `list` VALUES (130, '爆炒鱿鱼', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D1671062100%2C3895356830%26fm%3D74%26app%3D80%26f%3DJPEG%26size%3Df121%2C90.jpg', '爆炒鱿鱼，是用鱿鱼制作的一道家常菜。鱿鱼有少量的脂肪和碳水化合物，含有大量的胆固醇，鱿鱼中的微量元素以及钙、磷、硒、钾、钠含量都是较高的', '份', 10);
INSERT INTO `list` VALUES (131, ' 五彩炒驼峰', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D4067574170%2C3995537185%26fm%3D85%26app%3D63%26f%3DJPEG.jpg', '是中国菜系西北菜菜系中很有特色的菜式之一,以驼峰为主要材料,烹饪以为熟炒主,口味独特。驼峰肥美，佐以各种配料，软脆兼备，油润醇厚，色彩浓艳美观，营养丰富，独具风味。', '份', 10);
INSERT INTO `list` VALUES (132, '水晶梅花包', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D272070876%2C995655578%26fm%3D26%26gp%3D0.jpg', '水晶開口包,水晶南瓜包,水晶煎包,桂花水晶糕巧克力,红花水晶映虾蟹,西安小吃-梅花柿子饼,素鲍扒梅花掌,水晶花肘,梅花土豆,生蚝水晶包,梅花冬瓜脯,荷花水晶虾仁,...', '份', 10);
INSERT INTO `list` VALUES (133, '黄金角', 20, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D35332166%2C3724334413%26fm%3D26%26gp%3D0.jpg', '黄金角古迹保护区位于新西兰距北帕默斯顿50公里左右的内陆,靠近马可雷斯平原(Macraes Flat)村，毗邻巨大的马可雷斯金矿区。离这个风景点最近的地方是达尼丁', '份', 10);
INSERT INTO `list` VALUES (134, '金蟾玉鲍', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D4064735017%2C3310570319%26fm%3D74%26app%3D80%26f%3DJPEG%26size%3Df121%2C90.jpg', '金蟾玉鲍是以海鲜材料为主要食材的一道名菜，主要材料为鲍鱼和鱼茸，此菜尤其受到南方滨海地区人士的喜爱，被列入“满汉全席”。', '份', 10);
INSERT INTO `list` VALUES (137, '牛肉麻辣烫', 45, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/u%3D3185041888%2C1926081806%26fm%3D85%26app%3D52%26f%3DPNG.jpg', '麻辣烫(Spicy Hot Pot)是起源于四川乐山牛华镇的传统特色小吃，麻辣火锅也是吸收了麻辣烫的优点改良而来。是川渝地区最有特色也最能代表“川味”的一种饮食。2017年6月20日，国...', '份', 6);

-- ----------------------------
-- Table structure for meal_time
-- ----------------------------
DROP TABLE IF EXISTS `meal_time`;
CREATE TABLE `meal_time`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用餐时间表的id',
  `meal_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用餐时间名称',
  `meal_start` datetime(0) NOT NULL COMMENT '用餐开始时间',
  `meal_end` datetime(0) NOT NULL COMMENT '用餐结束时间',
  `pay_end` datetime(0) NULL DEFAULT NULL COMMENT '最晚付款时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meal_time
-- ----------------------------
INSERT INTO `meal_time` VALUES (1, '2019-10-17早饭', '2019-10-17 15:14:53', '2019-10-17 15:14:53', '2019-10-17 15:14:53');
INSERT INTO `meal_time` VALUES (2, '2019-10-17午饭', '2019-10-17 15:14:53', '2019-10-17 15:14:53', '2019-10-17 15:14:53');
INSERT INTO `meal_time` VALUES (23, '2019-10-19早饭', '2019-10-19 07:30:00', '2019-10-19 08:00:00', '2019-10-19 07:30:36');
INSERT INTO `meal_time` VALUES (45, '2019-10-19午饭', '2019-10-19 12:00:00', '2019-10-19 12:30:30', '2019-10-19 12:00:40');
INSERT INTO `meal_time` VALUES (67, '2019-10-19晚饭', '2019-10-19 18:44:08', '2019-10-19 19:00:22', '2019-10-19 17:50:38');
INSERT INTO `meal_time` VALUES (212, '2019-10-17晚饭', '2019-10-17 15:14:53', '2019-10-17 15:14:53', '2019-10-17 15:14:53');
INSERT INTO `meal_time` VALUES (213, '2019-10-20午饭', '2019-10-20 09:03:52', '2019-10-20 13:03:57', '2019-10-20 14:04:09');
INSERT INTO `meal_time` VALUES (214, '2019-10-20晚饭', '2019-10-20 14:11:57', '2019-10-20 23:12:03', '2019-10-20 23:12:12');
INSERT INTO `meal_time` VALUES (215, '2018-10-20晚辅', '2019-10-20 23:01:22', '2019-10-20 23:01:22', '2019-10-20 23:01:22');
INSERT INTO `meal_time` VALUES (216, '2019-10-21午饭', '2019-10-21 09:00:00', '2019-10-22 09:00:00', '2019-10-22 09:00:00');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单表的id',
  `meal_time_id` int(11) NOT NULL COMMENT '用餐时间id',
  `list_id` int(11) NOT NULL COMMENT '菜品的id 外键-餐品基本信息表',
  `count` int(11) NOT NULL COMMENT '预售数量：大于等于0：有预售，等于0为售罄；-1：不限预售数量',
  `off_price` int(11) NOT NULL COMMENT '促销价 0为没有促销价，以原价上架',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 288 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (266, 1, 1, 0, 1);
INSERT INTO `menu` VALUES (267, 1, 8, 0, 1);
INSERT INTO `menu` VALUES (268, 2, 18, 0, 1);
INSERT INTO `menu` VALUES (269, 2, 14, 0, 1);
INSERT INTO `menu` VALUES (270, 212, 18, 0, 1);
INSERT INTO `menu` VALUES (271, 212, 14, 0, 1);
INSERT INTO `menu` VALUES (272, 215, 1, 0, 1);
INSERT INTO `menu` VALUES (273, 215, 2, 0, 1);
INSERT INTO `menu` VALUES (274, 215, 5, 0, 1);
INSERT INTO `menu` VALUES (275, 215, 8, 0, 1);
INSERT INTO `menu` VALUES (276, 215, 11, 0, 1);
INSERT INTO `menu` VALUES (277, 215, 13, 0, 1);
INSERT INTO `menu` VALUES (278, 215, 12, 0, 1);
INSERT INTO `menu` VALUES (279, 215, 14, 0, 1);
INSERT INTO `menu` VALUES (280, 215, 16, 0, 1);
INSERT INTO `menu` VALUES (281, 215, 18, 0, 1);
INSERT INTO `menu` VALUES (282, 216, 1, 0, 1);
INSERT INTO `menu` VALUES (283, 216, 12, 0, 1);
INSERT INTO `menu` VALUES (284, 216, 16, 0, 1);
INSERT INTO `menu` VALUES (285, 216, 20, 0, 1);
INSERT INTO `menu` VALUES (286, 216, 24, 0, 1);
INSERT INTO `menu` VALUES (287, 216, 28, 0, 1);

-- ----------------------------
-- Table structure for menu_again
-- ----------------------------
DROP TABLE IF EXISTS `menu_again`;
CREATE TABLE `menu_again`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单表的id',
  `list_id` int(11) NOT NULL COMMENT '菜品的id 外键-餐品基本信息表',
  `count` int(11) NOT NULL COMMENT '预售数量：大于等于0：有预售，等于0为售罄；-1：不限预售数量',
  `off_price` int(11) NOT NULL COMMENT '促销价 0为没有促销价，以原价上架',
  `meal_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `meal_start` datetime(0) NOT NULL COMMENT '用餐开始时间',
  `meal_end` datetime(0) NOT NULL COMMENT '用餐结束时间',
  `pay_end` datetime(0) NULL DEFAULT NULL COMMENT '最晚付款时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_again
-- ----------------------------
INSERT INTO `menu_again` VALUES (1, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (2, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (3, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (4, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (5, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (6, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (7, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (8, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (9, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (10, 1, 0, 0, '午饭', '2019-10-14 22:07:18', '2019-10-15 22:07:24', '2019-10-15 22:07:21');
INSERT INTO `menu_again` VALUES (11, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (12, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (13, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (14, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (15, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (16, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (17, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (18, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (19, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');
INSERT INTO `menu_again` VALUES (20, 2, 0, 0, '早饭', '2019-10-08 22:07:55', '2019-10-10 22:08:01', '2019-10-09 22:07:58');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '留言表的id',
  `users_id` int(11) NOT NULL COMMENT '留言用户的id 外键-用户表',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '留言内容',
  `add_time` datetime(0) NOT NULL COMMENT '留言时间',
  `reply` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '回复内容',
  `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `grantee_id` int(11) NULL DEFAULT NULL COMMENT '回复人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (1, 1, ' 毫无疑问，小笼馒头是上海第一小吃。', '2019-10-15 15:54:45', '的确是的', '2019-10-18 10:50:02', 5);
INSERT INTO `message` VALUES (2, 1, '上海人无论有馅无馅的包子均称为馒头。', '2019-10-09 15:55:39', '近年来外地食品小吃日益流行', '2019-10-17 15:55:55', 2);
INSERT INTO `message` VALUES (3, 1, '小笼馒头是很讲功夫的点心。', '2019-10-15 15:54:45', '首先蒸的功夫讲究，蒸汽上来8分钟为最佳。', '2019-10-20 15:55:55', 2);
INSERT INTO `message` VALUES (4, 1, '如果小笼包上桌的时候是瘪的，就说明蒸的时间差五秒钟。', '2019-10-15 15:54:45', '而如果点的笼数多，店家一般会几笼一起上桌', '2019-10-20 15:55:55', 2);
INSERT INTO `message` VALUES (5, 1, '小笼的皮的功夫很讲究。', '2019-10-15 15:54:45', '正宗的南翔馒头用不发酵的面粉做皮开粉要用滚水。', '2019-10-18 10:51:06', 1);
INSERT INTO `message` VALUES (7, 1, '海小笼馒头的另一流派是用半发面做皮的，作法历史更悠久，是从扬州一路而来的。半发面的小笼一般一两6个，也有8个一两，但个子总要大一些。其实半发面小笼馒头是更普遍一点，缺点是蒸过了后，皮容易酥也容易破。当年绿杨邨和新镇江酒家都是以半发面小笼出名的。', '2019-10-15 15:54:45', '现在皮子似乎以死面为主流。很少半发面做皮。这点我么有考证，仅仅是直觉，需要和老师傅一一论证下。', '2019-10-20 15:55:55', 2);
INSERT INTO `message` VALUES (8, 1, '小笼的肉馅最讲究。正宗的南翔鲜肉小笼馒头的肉馅不应该掺入葱末,姜末则少许。', '2019-10-15 15:54:45', '所谓鲜肉便是当天的新鲜夹心猪后腿肉斩成，瘦四肥一，肉馅不可绞成肉泥，鼎泰丰便输在这里。馅料为白汤，如果在馅料中加入酱油，这馒头的风味便失之粗略而近于江北风格了。', '2019-10-18 10:39:42', 2);
INSERT INTO `message` VALUES (9, 1, '肉皮冻的在馅料中的比例则要非常讲究，少则汤汁不足，多则汤汁偏油而覆盖了猪肉和蟹粉的鲜味，更破坏了馒头的口味，再者小笼馒头不是灌汤包，现在由于外地汤包的进入，大家一味讲究汤要大，掺多了肉皮冻，造成的结果是馅料和汤头互不相干，口感层次脱节。', '2019-10-15 15:54:45', '再者蟹粉和肉的比例搭配原则是让蟹粉和猪肉两鲜并存。猪肉为底味，蟹粉为主味，两者不可偏废。一般节约的店出于成本的考虑，会将蟹粉的量减少，加大肉皮冻的比重，这样的包子，吃两个就被猪油闷住了，那还有美味可言？', '2019-10-18 08:45:44', NULL);
INSERT INTO `message` VALUES (10, 1, '小笼馒头的馅料日益丰富，其基本的为鲜肉，上海小笼为白汤，但凡吃出红汤的均为外地汤包。但凡吃出略微加酱油的，大多为保证肉馅的存放时间。', '2019-10-15 15:54:45', '虾肉小笼则是肉馅里加入虾仁，旧法用河虾仁，早上现剥，新鲜且鲜美。现在则用冰鲜虾仁，多为养殖虾或海虾，腥且不鲜。', '2019-10-20 15:55:55', 2);

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单详情表id',
  `orders_id` int(11) NOT NULL COMMENT '订单id',
  `list_id` int(11) NOT NULL COMMENT '菜品的id 外键-餐品基本信息表',
  `list_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜品名称 外键-餐品基本信息表',
  `list_price` int(11) NOT NULL COMMENT '金额 外键-餐品基本信息表',
  `sum` int(11) NOT NULL COMMENT '菜品份数',
  `win_id` int(11) NOT NULL COMMENT '窗口id 外键-餐品基本信息表',
  `state` tinyint(4) NOT NULL COMMENT '状态：0：未领取；1：已领取',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order_detail
-- ----------------------------
INSERT INTO `order_detail` VALUES (1, 1, 1, '北京烤鸭', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (2, 1, 2, '涮羊肉', 111, 1, 1, 1);
INSERT INTO `order_detail` VALUES (3, 1, 10, '仿膳宫廷菜', 10, 1, 2, 0);
INSERT INTO `order_detail` VALUES (4, 1, 1, '烧麦', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (5, 2, 1, '浦东鸡', 10, 1, 1, 1);
INSERT INTO `order_detail` VALUES (6, 2, 1, '熏火腿', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (7, 2, 1, '五香豆', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (8, 3, 1, '三黄鸡', 10, 1, 2, 1);
INSERT INTO `order_detail` VALUES (9, 3, 1, '桂发祥麻花', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (10, 4, 1, '天津银鱼', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (11, 4, 4, '赖汤圆', 15, 2, 3, 1);
INSERT INTO `order_detail` VALUES (12, 4, 4, '担担面', 15, 2, 3, 1);
INSERT INTO `order_detail` VALUES (13, 5, 4, '龙抄手', 10, 2, 4, 0);
INSERT INTO `order_detail` VALUES (14, 5, 5, '珍珠圆子', 16, 3, 4, 1);
INSERT INTO `order_detail` VALUES (15, 5, 5, '荷叶软饼', 16, 3, 5, 1);
INSERT INTO `order_detail` VALUES (16, 3, 5, '凤尾酥', 19, 2, 3, 1);
INSERT INTO `order_detail` VALUES (17, 48, 54, '三黄鸡', 45, 1, 1, 0);
INSERT INTO `order_detail` VALUES (18, 49, 10, '刀削面', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (19, 50, 10, '刀削面', 20, 1, 1, 0);
INSERT INTO `order_detail` VALUES (20, 51, 11, '拉面', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (21, 51, 12, '果木烤鸭', 34, 1, 1, 0);
INSERT INTO `order_detail` VALUES (22, 51, 11, '拉面', 54, 1, 1, 0);
INSERT INTO `order_detail` VALUES (23, 51, 11, '拉面', 54, 1, 1, 0);
INSERT INTO `order_detail` VALUES (24, 51, 14, '饺子', 64, 1, 1, 0);
INSERT INTO `order_detail` VALUES (25, 52, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (26, 53, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (27, 53, 2, '豆腐鲈鱼煲', 121, 1, 1, 0);
INSERT INTO `order_detail` VALUES (28, 53, 3, '鸡肉', 343, 1, 1, 0);
INSERT INTO `order_detail` VALUES (29, 54, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (30, 54, 22, '叉烧包', 24, 1, 1, 0);
INSERT INTO `order_detail` VALUES (31, 54, 19, '小鸡炖蘑菇', 44, 1, 1, 0);
INSERT INTO `order_detail` VALUES (32, 55, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (33, 56, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (34, 56, 2, '豆腐鲈鱼煲', 121, 1, 1, 0);
INSERT INTO `order_detail` VALUES (35, 56, 10, '刀削面', 131, 1, 1, 0);
INSERT INTO `order_detail` VALUES (36, 57, 1, '红烧狮子头', 10, 1, 1, 0);
INSERT INTO `order_detail` VALUES (37, 58, 3, '鸡肉', 222, 1, 1, 0);
INSERT INTO `order_detail` VALUES (38, 58, 5, '青菜炒青菜', 234, 1, 1, 0);
INSERT INTO `order_detail` VALUES (39, 58, 23, '沙河粉', 246, 1, 1, 0);
INSERT INTO `order_detail` VALUES (40, 59, 2, '豆腐鲈鱼煲', 111, 1, 1, 0);
INSERT INTO `order_detail` VALUES (41, 59, 10, '刀削面', 121, 1, 1, 0);
INSERT INTO `order_detail` VALUES (42, 59, 22, '叉烧包', 135, 1, 1, 0);
INSERT INTO `order_detail` VALUES (43, 60, 12, '果木烤鸭', 24, 1, 1, 0);
INSERT INTO `order_detail` VALUES (44, 61, 22, '叉烧包', 28, 1, 1, 0);
INSERT INTO `order_detail` VALUES (45, 61, 36, '春卷', 92, 1, 1, 0);
INSERT INTO `order_detail` VALUES (46, 61, 42, '玲珑鱼脆羹', 137, 1, 1, 0);
INSERT INTO `order_detail` VALUES (47, 61, 46, '锅巴菜', 171, 1, 1, 0);
INSERT INTO `order_detail` VALUES (48, 62, 1, '红烧狮子头', 10, 1, 1, 0);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单表的id',
  `users_id` int(11) NOT NULL COMMENT '用户的id 外键-用户表',
  `meal_time_id` int(11) NOT NULL COMMENT '用餐时间',
  `people_num` int(11) NOT NULL COMMENT '用餐人数',
  `orders_state` tinyint(4) NOT NULL COMMENT '0:未支付，1：支付未领取，2：领取未评价 3：已评价',
  `coupons_id` int(11) NULL DEFAULT NULL COMMENT '优惠卷id 外键-优惠卷表',
  `coupons_cut_money` int(11) NULL DEFAULT NULL COMMENT '优惠金额 外键-优惠卷表',
  `express_fee` int(11) NULL DEFAULT NULL COMMENT '快递费',
  `total_price` int(11) NOT NULL COMMENT '总价格',
  `add_time` datetime(0) NOT NULL COMMENT '下单时间',
  `qr_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '二维码信息',
  `get_time` datetime(0) NOT NULL COMMENT '取餐时间',
  `box_fee` tinyint(4) NOT NULL COMMENT '0:食堂  大于0:打包',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 1, 1, 4, 3, 1, 20, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (2, 2, 1, 5, 1, 1, 5, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (3, 3, 1, 6, 1, 1, 30, 2, 35, '2019-10-16 21:16:46', '二维码', '2019-10-18 21:16:46', 1);
INSERT INTO `orders` VALUES (4, 4, 1, 2, 1, 1, 10, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (5, 5, 1, 1, 2, 1, 5, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-09 21:16:46', 1);
INSERT INTO `orders` VALUES (6, 6, 1, 8, 2, 1, 20, 2, 68, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (7, 7, 1, 4, 1, 1, 20, 2, 56, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (8, 8, 1, 4, 2, 1, 20, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-18 21:16:46', 1);
INSERT INTO `orders` VALUES (9, 9, 1, 5, 2, 1, 25, 2, 28, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (10, 10, 1, 4, 2, 1, 5, 2, 50, '2019-10-16 21:16:46', '二维码', '2019-10-19 21:16:46', 1);
INSERT INTO `orders` VALUES (11, 11, 1, 4, 2, 1, 5, 2, 55, '2019-10-16 21:16:46', '二维码', '2019-10-16 21:16:46', 1);
INSERT INTO `orders` VALUES (21, 1, 213, 1, 0, 0, 0, 0, 45, '2019-10-20 09:52:12', '无', '2019-10-20 09:52:12', 0);
INSERT INTO `orders` VALUES (22, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 09:52:29', '无', '2019-10-20 09:52:29', 0);
INSERT INTO `orders` VALUES (24, 1, 213, 1, 0, 0, 0, 0, 11, '2019-10-20 09:55:23', '无', '2019-10-20 09:55:23', 0);
INSERT INTO `orders` VALUES (25, 1, 213, 1, 0, 0, 0, 0, 45, '2019-10-20 09:56:21', '无', '2019-10-20 09:56:21', 0);
INSERT INTO `orders` VALUES (26, 1, 213, 1, 0, 0, 0, 0, 45, '2019-10-20 09:59:00', '无', '2019-10-20 09:59:00', 0);
INSERT INTO `orders` VALUES (27, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 09:59:36', '无', '2019-10-20 09:59:36', 0);
INSERT INTO `orders` VALUES (28, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 09:59:58', '无', '2019-10-20 09:59:58', 0);
INSERT INTO `orders` VALUES (29, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:05:10', '无', '2019-10-20 10:05:10', 0);
INSERT INTO `orders` VALUES (30, 1, 213, 1, 0, 0, 0, 0, 20, '2019-10-20 10:05:47', '无', '2019-10-20 10:05:47', 0);
INSERT INTO `orders` VALUES (31, 1, 213, 1, 0, 0, 0, 0, 30, '2019-10-20 10:06:01', '无', '2019-10-20 10:06:01', 0);
INSERT INTO `orders` VALUES (32, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:06:42', '无', '2019-10-20 10:06:42', 0);
INSERT INTO `orders` VALUES (33, 1, 213, 1, 1, 0, 0, 0, 10, '2019-10-20 10:07:12', '无', '2019-10-20 10:07:12', 0);
INSERT INTO `orders` VALUES (34, 1, 213, 1, 1, 0, 0, 0, 20, '2019-10-20 10:07:18', '无', '2019-10-20 10:07:18', 0);
INSERT INTO `orders` VALUES (35, 1, 213, 1, 1, 0, 0, 0, 10, '2019-10-20 10:08:56', '无', '2019-10-20 10:08:56', 0);
INSERT INTO `orders` VALUES (36, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:10:20', '无', '2019-10-20 10:10:20', 0);
INSERT INTO `orders` VALUES (37, 1, 213, 1, 3, 0, 0, 0, 20, '2019-10-20 10:10:38', '无', '2019-10-20 10:10:38', 0);
INSERT INTO `orders` VALUES (38, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:11:09', '无', '2019-10-20 10:11:09', 0);
INSERT INTO `orders` VALUES (39, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:11:55', '无', '2019-10-20 10:11:55', 0);
INSERT INTO `orders` VALUES (40, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:12:12', '无', '2019-10-20 10:12:12', 0);
INSERT INTO `orders` VALUES (41, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:13:12', '无', '2019-10-20 10:13:12', 0);
INSERT INTO `orders` VALUES (42, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:13:26', '无', '2019-10-20 10:13:26', 0);
INSERT INTO `orders` VALUES (43, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:15:11', '无', '2019-10-20 10:15:11', 0);
INSERT INTO `orders` VALUES (44, 1, 213, 1, 0, 0, 0, 0, 20, '2019-10-20 10:15:26', '无', '2019-10-20 10:15:26', 0);
INSERT INTO `orders` VALUES (45, 1, 213, 1, 0, 0, 0, 0, 30, '2019-10-20 10:15:31', '无', '2019-10-20 10:15:31', 0);
INSERT INTO `orders` VALUES (46, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:15:44', '无', '2019-10-20 10:15:44', 0);
INSERT INTO `orders` VALUES (47, 1, 213, 1, 0, 0, 0, 0, 111, '2019-10-20 10:47:09', '无', '2019-10-20 10:47:09', 0);
INSERT INTO `orders` VALUES (48, 1, 213, 1, 0, 0, 0, 0, 45, '2019-10-20 10:47:40', '无', '2019-10-20 10:47:40', 0);
INSERT INTO `orders` VALUES (49, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:50:38', '无', '2019-10-20 10:50:38', 0);
INSERT INTO `orders` VALUES (50, 1, 213, 1, 0, 0, 0, 0, 20, '2019-10-20 10:50:41', '无', '2019-10-20 10:50:41', 0);
INSERT INTO `orders` VALUES (51, 1, 213, 1, 0, 0, 0, 0, 10, '2019-10-20 10:51:40', '无', '2019-10-20 10:51:40', 0);
INSERT INTO `orders` VALUES (52, 1, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 14:12:26', '无', '2019-10-20 14:12:26', 0);
INSERT INTO `orders` VALUES (53, 1, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 14:16:31', '无', '2019-10-20 14:16:31', 0);
INSERT INTO `orders` VALUES (54, 1, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 22:38:33', '无', '2019-10-20 22:38:33', 0);
INSERT INTO `orders` VALUES (55, 1, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 22:42:58', '无', '2019-10-20 22:42:58', 0);
INSERT INTO `orders` VALUES (56, 14, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 22:45:07', '无', '2019-10-20 22:45:07', 0);
INSERT INTO `orders` VALUES (57, 14, 214, 1, 0, 0, 0, 0, 10, '2019-10-20 23:01:22', '无', '2019-10-20 23:01:22', 0);
INSERT INTO `orders` VALUES (58, 22, 215, 1, 1, 0, 0, 0, 222, '2019-10-20 23:29:53', '无', '2019-10-20 23:29:53', 0);
INSERT INTO `orders` VALUES (59, 23, 215, 1, 3, 0, 0, 0, 111, '2019-10-21 00:30:00', '无', '2019-10-21 00:30:00', 0);
INSERT INTO `orders` VALUES (60, 18, 215, 1, 0, 0, 0, 0, 24, '2019-10-21 09:14:56', '无', '2019-10-21 09:14:56', 0);
INSERT INTO `orders` VALUES (61, 24, 216, 1, 3, 0, 0, 0, 28, '2019-10-21 09:51:38', '无', '2019-10-21 09:51:38', 0);
INSERT INTO `orders` VALUES (62, 1, 216, 1, 0, 0, 0, 0, 10, '2019-10-21 10:18:49', '无', '2019-10-21 10:18:49', 0);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户基本信息表的id',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT ' 用户名',
  `pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `nick_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '昵称',
  `phone_num` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '手机号',
  `class` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '班级',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '身份证号',
  `sushe_num` int(11) NULL DEFAULT NULL COMMENT '宿舍号',
  `come_from` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '籍贯',
  `wechat_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '微信号',
  `favorite` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '爱好',
  `gender` enum('男','女') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '性别',
  `birthday` date NOT NULL COMMENT '生日',
  `last_point` int(11) NOT NULL COMMENT '剩余积分',
  `total_point` int(11) NOT NULL COMMENT '总积分',
  `header_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '头像',
  `money` decimal(11, 2) NOT NULL COMMENT '钱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '王海龙', '6105b4157fb5de0cf528cfb7f28d41d9', '海龙', '13778424516', '全栈12', '511123200008267015', 321, '四川', '13778424516', '游泳', '男', '2000-08-25', 1000, 1000, 'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/1559565909404.png', 177.10);
INSERT INTO `users` VALUES (2, '夏克斌', '6105b4157fb5de0cf528cfb7f28d41d9', '老夏', '13884174517', '全栈12', '510682200005257125', 322, '河北', '13884174517', '打游戏', '男', '1998-12-03', 1000, 1200, 'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/1567058595029.jpg', 998.00);
INSERT INTO `users` VALUES (3, '夏克斌', '6105b4157fb5de0cf528cfb7f28d41d9', '老夏', '13884174517', '全栈12', '510682200005257125', 322, '河北', '13884174517', '打游戏', '男', '1998-12-03', 1000, 1200, 'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/1567058595029.jpg', 998.00);
INSERT INTO `users` VALUES (4, '夏克斌', '6105b4157fb5de0cf528cfb7f28d41d9', '老夏', '13884174517', '全栈12', '510682200005257125', 322, '河北', '13884174517', '打游戏', '男', '1998-12-03', 1000, 1200, 'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/1567058595029.jpg', 998.00);
INSERT INTO `users` VALUES (5, '师文博', '6105b4157fb5de0cf528cfb7f28d41d9', '阿师', '17834560987', '全栈12', '510680200007257909', 331, '陕西', '17834560987', '写代码', '男', '2000-07-25', 0, 0, '无', 10.00);
INSERT INTO `users` VALUES (6, '师文博', '6105b4157fb5de0cf528cfb7f28d41d9', '阿师', '17834560987', '全栈12', '510680200007257909', 331, '陕西', '17834560987', '写代码', '男', '2000-07-25', 0, 0, '无', 10.00);
INSERT INTO `users` VALUES (8, '高浩', '6105b4157fb5de0cf528cfb7f28d41d9', '小高', '17605255740', '全栈12', '845012369741358924', 444, '江苏', '17605255740', '敲代码', '男', '2000-09-12', 0, 0, '无', 20.00);
INSERT INTO `users` VALUES (9, '高浩', '6105b4157fb5de0cf528cfb7f28d41d9', '小高', '17605255740', '全栈12', '845012369741358924', 444, '江苏', '17605255740', '敲代码', '男', '2000-09-12', 0, 0, '无', 20.00);
INSERT INTO `users` VALUES (10, '高浩', '6105b4157fb5de0cf528cfb7f28d41d9', '小高', '17605255740', '全栈12', '845012369741358924', 444, '江苏', '17605255740', '敲代码', '男', '2000-09-12', 0, 0, '无', 20.00);
INSERT INTO `users` VALUES (11, '刘孝天', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (12, '刘会珍', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (13, '高浩', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (14, '李晓雨', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 83247.00);
INSERT INTO `users` VALUES (15, '樊保华', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (16, '龙华', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (17, '梨花', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (18, '龙慧婷', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 100000.00);
INSERT INTO `users` VALUES (19, '1231231', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (20, '樊保华', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (21, '李晓雨', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 0.00);
INSERT INTO `users` VALUES (22, '龙华秋月', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 9052.00);
INSERT INTO `users` VALUES (23, '聂代睿', '6105b4157fb5de0cf528cfb7f28d41d9', '', '', '', '', 1, '', '', '', '男', '2000-01-01', 0, 0, '', 999999632.00);
INSERT INTO `users` VALUES (24, '发生的', '6105b4157fb5de0cf528cfb7f28d41d9', '123', '12345672345', '123', '511123200008237015', 1, '吃撒', '123123123123', '123', '男', '1999-12-31', 0, 0, '123', 9999571.00);
INSERT INTO `users` VALUES (25, '123', '6105b4157fb5de0cf528cfb7f28d41d9', '123', '123456782345', '去哪找', '511123200008237015', 321, '四川', '12341234123', '游戏', '男', '2000-00-00', 134, 212, 'http://wanghailongwang.oss-cn-beijing.aliyuncs.com//images/guojisp_152s.jpg', 0.00);

-- ----------------------------
-- Table structure for vote
-- ----------------------------
DROP TABLE IF EXISTS `vote`;
CREATE TABLE `vote`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票表的id',
  `users_id` int(11) NOT NULL COMMENT '发起用户id 外键-用户表',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '发起投票标题',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜品名称 外键-餐品基本信息表',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜品图片 外键-餐品基本信息表',
  `sum` int(11) NOT NULL COMMENT '菜品图片',
  `add_time` datetime(0) NOT NULL COMMENT '发起时间',
  `end_time` datetime(0) NOT NULL COMMENT '结束时间',
  `state` tinyint(4) NOT NULL COMMENT '0:未采纳;1:已采纳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of vote
-- ----------------------------
INSERT INTO `vote` VALUES (1, 1, '想吃青椒炒肉丝', '青椒肉丝', 'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/%E4%B8%8B%E8%BD%BD.jpg', 10, '2019-10-17 16:48:00', '2019-10-18 16:48:05', 0);
INSERT INTO `vote` VALUES (4, 4, '地方v百度', '的成本', '二个人', 13, '2019-10-17 18:14:26', '2019-10-17 18:14:31', 0);
INSERT INTO `vote` VALUES (5, 5, '的v热问题', '啊v出卖我居然', '速度v额特别', 14, '2019-10-17 18:14:58', '2019-10-17 18:15:03', 0);
INSERT INTO `vote` VALUES (6, 6, '谁知道CR v', '未拆封v个', '潍坊个地方v', 15, '2019-10-17 18:15:37', '2019-10-17 18:15:42', 0);
INSERT INTO `vote` VALUES (7, 7, '是卡册积极', '额，每次方可我发奖励金额为考虑', '的生产任务我v', 16, '2019-10-17 18:16:06', '2019-10-17 18:16:11', 0);
INSERT INTO `vote` VALUES (8, 8, '受到惩罚我', '速度v特别', 'ui看i了郁闷', 17, '2019-10-17 18:16:36', '2019-10-17 18:16:40', 0);
INSERT INTO `vote` VALUES (9, 9, '考虑从马克思', '，删除急啊看你', '描述擦军事才能', 18, '2019-10-17 18:18:10', '2019-10-17 18:18:16', 0);
INSERT INTO `vote` VALUES (10, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);
INSERT INTO `vote` VALUES (11, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);
INSERT INTO `vote` VALUES (12, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);
INSERT INTO `vote` VALUES (13, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);
INSERT INTO `vote` VALUES (14, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);
INSERT INTO `vote` VALUES (15, 10, '但是每次v看完就', '什么的承诺非玩家发', '没事的v ', 19, '2019-10-17 18:18:41', '2019-10-17 18:18:45', 0);

-- ----------------------------
-- Table structure for vote_detail
-- ----------------------------
DROP TABLE IF EXISTS `vote_detail`;
CREATE TABLE `vote_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '投票详情表的id',
  `vote_id` int(11) NOT NULL COMMENT '投票id 外键-用户表',
  `users_id` int(11) NOT NULL COMMENT '踢票id 外键-用户表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of vote_detail
-- ----------------------------
INSERT INTO `vote_detail` VALUES (1, 1, 0);
INSERT INTO `vote_detail` VALUES (2, 2, 0);
INSERT INTO `vote_detail` VALUES (3, 3, 0);
INSERT INTO `vote_detail` VALUES (4, 4, 0);
INSERT INTO `vote_detail` VALUES (5, 5, 0);
INSERT INTO `vote_detail` VALUES (6, 6, 0);
INSERT INTO `vote_detail` VALUES (7, 7, 20);
INSERT INTO `vote_detail` VALUES (8, 8, 2);
INSERT INTO `vote_detail` VALUES (9, 9, 2);
INSERT INTO `vote_detail` VALUES (10, 10, 3);
INSERT INTO `vote_detail` VALUES (11, 11, 2);
INSERT INTO `vote_detail` VALUES (12, 12, 4);
INSERT INTO `vote_detail` VALUES (13, 13, 2);
INSERT INTO `vote_detail` VALUES (14, 14, 1);
INSERT INTO `vote_detail` VALUES (15, 15, 2);
INSERT INTO `vote_detail` VALUES (16, 16, 6);
INSERT INTO `vote_detail` VALUES (17, 17, 2);
INSERT INTO `vote_detail` VALUES (18, 18, 5);
INSERT INTO `vote_detail` VALUES (19, 19, 11);
INSERT INTO `vote_detail` VALUES (20, 20, 2);

-- ----------------------------
-- Table structure for win
-- ----------------------------
DROP TABLE IF EXISTS `win`;
CREATE TABLE `win`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '窗口权限表的id',
  `win_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '窗口名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of win
-- ----------------------------
INSERT INTO `win` VALUES (1, '炒菜窗口');
INSERT INTO `win` VALUES (2, '面食窗口');
INSERT INTO `win` VALUES (3, '果木烤鸭');
INSERT INTO `win` VALUES (4, '米饭窗口');
INSERT INTO `win` VALUES (5, '米线窗口');
INSERT INTO `win` VALUES (6, '麻辣烫窗口');
INSERT INTO `win` VALUES (7, '快餐窗口');
INSERT INTO `win` VALUES (8, '西餐窗口');
INSERT INTO `win` VALUES (9, '特色菜窗口');
INSERT INTO `win` VALUES (10, '地方菜窗口');
INSERT INTO `win` VALUES (11, '热菜窗口');
INSERT INTO `win` VALUES (12, '凉菜窗口');
INSERT INTO `win` VALUES (13, '火锅窗口');
INSERT INTO `win` VALUES (14, '海鲜窗口');
INSERT INTO `win` VALUES (15, '烘焙窗口');
INSERT INTO `win` VALUES (16, '泡酱腌菜窗口');
INSERT INTO `win` VALUES (17, '小吃窗口');
INSERT INTO `win` VALUES (18, '零食窗口');
INSERT INTO `win` VALUES (19, '饮品窗口');
INSERT INTO `win` VALUES (20, '面食窗口');
INSERT INTO `win` VALUES (21, '甜品窗口');
INSERT INTO `win` VALUES (22, '炒饭窗口');
INSERT INTO `win` VALUES (23, '小粥窗口');
INSERT INTO `win` VALUES (24, '甜品窗口');

-- ----------------------------
-- Table structure for win_count
-- ----------------------------
DROP TABLE IF EXISTS `win_count`;
CREATE TABLE `win_count`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '窗口业绩表的id',
  `win_id` int(11) NOT NULL COMMENT '窗口id 外键-餐品基本信息表',
  `win_date` date NOT NULL COMMENT '业绩时间',
  `win_sum` int(11) NOT NULL COMMENT '窗口销售额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of win_count
-- ----------------------------
INSERT INTO `win_count` VALUES (1, 1, '2019-10-17', 10);
INSERT INTO `win_count` VALUES (2, 2, '2019-10-17', 0);
INSERT INTO `win_count` VALUES (3, 2, '2019-10-17', 0);
INSERT INTO `win_count` VALUES (4, 3, '2019-10-17', 50);
INSERT INTO `win_count` VALUES (5, 3, '2019-10-17', 50);
INSERT INTO `win_count` VALUES (6, 3, '2019-10-17', 50);
INSERT INTO `win_count` VALUES (7, 3, '2019-10-17', 50);
INSERT INTO `win_count` VALUES (8, 2, '2019-10-17', 0);
INSERT INTO `win_count` VALUES (9, 2, '2019-10-17', 0);
INSERT INTO `win_count` VALUES (10, 2, '2019-10-17', 0);

SET FOREIGN_KEY_CHECKS = 1;
