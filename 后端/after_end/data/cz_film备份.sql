/*
 Navicat Premium Data Transfer

 Source Server         : whlFWQ
 Source Server Type    : MySQL
 Source Server Version : 50644
 Source Host           : 39.106.158.169:3306
 Source Schema         : cz_film

 Target Server Type    : MySQL
 Target Server Version : 50644
 File Encoding         : 65001

 Date: 30/10/2019 08:40:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL COMMENT '管理员姓名',
  `admin_password` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '管理员密码',
  `admin_phone` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '管理员手机号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (5, 'adminsup3', '123456', '13952289845');
INSERT INTO `admin` VALUES (6, 'admin111', '123123', '325345');
INSERT INTO `admin` VALUES (7, 'adminsup', '123456', '13952289845');
INSERT INTO `admin` VALUES (8, 'admin1', '123456', '13952289845');
INSERT INTO `admin` VALUES (9, 'admin', '123456', '13952289845');
INSERT INTO `admin` VALUES (10, 'admin3', '123456', '13952289845');
INSERT INTO `admin` VALUES (11, '朱大帅', '89465123', '1489741');
INSERT INTO `admin` VALUES (12, '朱朱男孩', '89465123', '1489741');
INSERT INTO `admin` VALUES (13, '李晓雨你个傻屌', '123456', '13952289845');

-- ----------------------------
-- Table structure for ca_filminfo
-- ----------------------------
DROP TABLE IF EXISTS `ca_filminfo`;
CREATE TABLE `ca_filminfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '影片id',
  `film_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '影片名称',
  `image_url` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '视频链接',
  `url_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接hash值',
  `score` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评分',
  `star` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '演员',
  `director` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '导演',
  `channel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '频道',
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '影片类型名称',
  `subtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '影片子类型',
  `year` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上映时间',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地区',
  `describe` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '影片简介',
  `add_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '添加时间',
  `is_download` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否已经采集',
  `recommend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '推荐  默认1  推荐1 不推荐0',
  `hot` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '热门  默认1  热门1 不热门0',
  `num` int(11) UNSIGNED NULL DEFAULT 1 COMMENT '集书',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 344 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ca_filminfo
-- ----------------------------
INSERT INTO `ca_filminfo` VALUES (4, '路西法第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-02/156465917765766390.jpg', '88ys.cc', 'oumeiju/201802/49324.html', '9.4', '汤姆·艾利斯/D.B.伍德塞德/劳伦·日尔曼', '伦·怀斯曼/艾瑞琪·拉·塞拉', NULL, '电视剧', '科幻片', '2016', '美国', '在《哥谭》作为蝙蝠侠的衍生剧播出后，DC继续决定将旗下的Lucifer也改编成电视剧。这位无聊的地狱之王在王座上过得可是一点也不开心。他下定决心抛弃他的地狱王国，来到洛杉矶成了位酒吧老板，追寻娱乐之都的刺激生活并协助洛杉矶的警察一起惩治罪犯。《路西法第一季》电视剧于2018年02月19号由88影视网收集自网络发布，同时提供在线观看服务。', NULL, '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (5, '中国机长', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/156993180187486821.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '1', '未知', '爱奇艺', '电影', '恐怖片', '2019', '中国', '未知', '2019-10-11 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (6, '空降利刃', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156855240195418503.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '2', '未知', '爱奇艺', '电影', '战争片', '2019', '中国', '未知', '2019-10-11 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (7, '攀登者', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156941700425569619.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '3', '未知', '爱奇艺', '电影', '科幻片', '2019', '中国', '未知', '2019-10-11 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (8, '世界欠我一个初恋', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156941580152161677.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '1', '未知', '爱奇艺', '电影', '爱情片', '2019', '中国', '没有', '2019-00-00 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (9, '在远方', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156915780362788267.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '2', '未知', '爱奇艺', '电影', '爱情片', '2019', '中国', '没有', NULL, '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (13, '陈情令', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-06/15616401227.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '3', '未知', '爱奇艺', '电影', '偶像片', '2019', '中国', '没有', '1571122598169', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (14, '画江湖之不良人第三季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-10/156460245293339449.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '4', '未知', '爱奇艺', '动漫', '魔幻片', '2019', '中国', '未知', '1571123489893', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (15, '速度与激情：特别行动', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156516850540021914.jpeg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '5', '未知', '爱奇艺', '电影', '动作片', '2019', '美国', '未知', '2019-10-11 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (16, '我的莫格利男孩', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156708420609452309.jpg', 'https://www.88ys.cc/vod-play-id-67341-src-1-num-1.html', '/zhongguojizhang', '5.0', '6', '未知', '爱奇艺', '综艺', '爱情片', '2019', '中国', '未知', '2019-10-11 00:00:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (36, '保佑哈特', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157138680223848709.jpg', 'https://www.88ys.cc/vod-play-id-67848-src-1-num-1.html', '/baoyouhate', '5.0', '克里斯汀·韦格,玛娅·鲁道夫,吉莉恩·贝尔,伊克·巴里霍尔兹', '未知', '爱奇艺', '动漫', '欧美动漫', '未知', '美国', '未知', '2019-10-18 04:27', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (37, '逆天邪神第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-04/15563647241.jpg', 'https://www.88ys.cc/vod-play-id-59665-src-1-num-1.html', '/nitianxieshenyi', '5.0', '未知', '未知', '爱奇艺', '动漫', '国产动漫', '2019', '大陆', '未知', '2019-10-18 06:03', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (38, '武战道', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157138860163801988.jpg', 'https://www.88ys.cc/vod-play-id-67852-src-1-num-1.html', '/wuzhandao', '5.0', '未知', '王巍', '爱奇艺', '动漫', '国产动漫', NULL, '2013', '未知', '2019-10-18 04:46', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (39, '星球大战：抵抗组织第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157046880215938639.jpg', 'https://www.88ys.cc/vod-play-id-67075-src-1-num-1.html', '/qingqiudazhan', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '未知', '未知', '2019-10-18 04:16', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (41, '野兽巨星', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157059481555004934.jpg', 'https://www.88ys.cc/vod-play-id-67128-src-1-num-1.html', '/qingqiudazhan', '5.0', '小林亲弘,千本木彩花,小野友树,种崎敦美,', '松見真一', NULL, '动漫', ' 其他动漫', '2019', '未知', '未知', '2019-10-18 04:16', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (64, '\r\n留住我的灵魂', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131480218975961.jpg', 'https://www.88ys.cc/vod-play-id-67800-src-1-num-1.html', '/liuzhuwodelinhun', '8.6', 'Parker Smerek,Remington Gielniak,Arielle Olkhovsky', 'Ajmal Zaheer Ahmad', '爱奇艺', '电影', ' 恐怖片', '2019', '美国', '未知', '2019-10-17 10:50', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (65, '高分少女', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-07/156460311984385621.jpg', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-07/156460311984385621.jpg', '/liuzhuwodelinhun', '5.0', '天崎滉平,铃代纱弓,广濑有纪,兴津和幸', '山川吉树', '爱奇艺', '动漫', '日本动漫', '2019', '日本', '未知', '2019-10-19 02:26', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (66, '歌舞伎町夏洛克', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084740228817274.jpg', 'https://www.88ys.cc/vod-play-id-67275-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '小西克幸,中村悠一,山下诚一郎,齐藤壮马', '吉村爱', '爱奇艺', '动漫', '其他动漫', '2019', '日本', '未知', '2019-10-19 02:32', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (67, '食戟之灵第四季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084920216801049.jpg', 'https://www.88ys.cc/vod-play-id-67269-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '松冈祯丞,金元寿子,高桥未奈美,石田彰,伊藤静', '米谷良知', '爱奇艺', '动漫', '日本动漫', '2019', '日本', '未知', '2019-10-19 02:31', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (68, '厨神小当家日语版', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157142340355988517.jpg', 'https://www.88ys.cc/vod-play-id-67886-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '藤原夏海,茅野爱衣,藤井雪代,中村悠一', '川崎逸朗', '爱奇艺', '动漫', '国产动漫', '2019', '大陆', '未知', '2019-10-19 02:36', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (69, '\r\n无限之住人 重制版', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157081020172782296.jpg', 'https://www.88ys.cc/vod-play-id-67248-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '津田健次郎,佐仓绫音,佐佐木望,铃木达央', '浜崎博嗣', '爱奇艺', '动漫', '其他动漫', '2019', '日本', '未知', '2019-10-18 03:59', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (70, '精灵梦叶罗丽第七季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156787140288033269.jpg', 'https://www.88ys.cc/vod-play-id-65575-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '乔菲菲,山新,杨凝,程愿', '金今', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-18 12:38', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (71, '天行九歌', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564649572448017048.jpg', 'https://www.88ys.cc/vod-play-id-7993-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '赵路,吴磊,翟巍,黄莺,孟祥龙,洪海天,夏磊,海帆', '沈乐平', '爱奇艺', '动漫', '国产动漫', '2019', '大陆', '未知', '2019-10-18 12:28', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (72, '\r\n野蛮纪源', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157097281066146198.jpg', 'https://www.88ys.cc/vod-play-id-67427-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '詹姆斯·斯尼德', '詹姆斯·斯尼德', '爱奇艺', '动漫', '欧美动漫', '2019', '美国', '未知', '2019-10-17 01:43', '1', '1', '1', 5);
INSERT INTO `ca_filminfo` VALUES (73, '\r\n企鹅乐园2019', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157129140222804965.jpg', 'https://www.88ys.cc/vod-play-id-67771-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', 'Aaron LaPlante', '托马斯Freeley，玛丽亚Petrano，雅各Whiteshed', '爱奇艺', '动漫', '其他动漫', '2019', '美国', '未知', '2019-10-17 01:43', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (74, '放课后桌游俱乐部', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157007580166003460.jpg', 'https://www.88ys.cc/vod-play-id-66716-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '宫下早纪,高野麻里佳,富田美优,黑田崇矢', '今泉贤一', '爱奇艺', '动漫', '日本动漫', '2019', '日本', '未知', '2019-10-17 01:43', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (75, '万界仙踪第三季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157129140202083998.jpg', 'https://www.88ys.cc/vod-play-id-67772-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '钟巍 弓与蛇 妮子 袁明清', '玄青,茶白', '爱奇艺', '动漫', '国产动漫', '2019', '大陆', '未知', '2019-10-17 01:43', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (76, '萌芽熊治愈花店', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156715320230936531.jpg', 'https://www.88ys.cc/vod-play-id-65206-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (77, '纯情丫头休想逃', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156869160344296479.jpg', 'https://www.88ys.cc/vod-play-id-66022-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 5);
INSERT INTO `ca_filminfo` VALUES (79, '漫动画拯救世界吧大叔', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157007700402292312.jpg', 'https://www.88ys.cc/vod-play-id-66719-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (81, '七大罪 诸神的逆鳞', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157062180237215555.jpg', 'https://www.88ys.cc/vod-play-id-67172-src-2-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (82, '\r\n喜欢本大爷的竟然只有你一个?', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157004040179961713.jpg', 'https://www.88ys.cc/vod-play-id-66710-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (83, '\r\n内裤队长吓鬼记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157124940236096191.jpg', 'https://www.88ys.cc/vod-play-id-67741-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (84, '恶魔的独宠甜妻', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156709020442128811.jpg', 'https://www.88ys.cc/vod-play-id-65160-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (85, '将军妻不可欺', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156709020298415567.jpg', 'https://www.88ys.cc/vod-play-id-65161-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (86, '热带雨林的爆笑生活OVA2', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157123020284031690.jpg', 'https://www.88ys.cc/vod-play-id-67722-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (87, '战神金刚：传奇的保护神第五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-03/156460404127807952.jpg', 'https://www.88ys.cc/vod-play-id-50129-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (88, '\r\n初恋微甜', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157122481025168628.jpg', 'https://www.88ys.cc/vod-play-id-67694-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (89, '狼少女与黑王子', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/156463508606478105.jpg', 'https://www.88ys.cc/vod-play-id-67668-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (91, '\r\n魔术快斗中文版', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/156463508606478105.jpg', 'https://www.88ys.cc/vod-play-id-45257-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', '其他动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (93, '西行纪', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-07/156460309738657638.jpg', 'https://www.88ys.cc/vod-play-id-52319-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '动漫', ' 国产动漫', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (94, '哗众之人', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157142340191167427.jpg', 'https://www.88ys.cc/vod-play-id-67893-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电影', '剧情片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (95, '\r\n七天', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-02/156463190240055945.jpg', 'https://www.88ys.cc/vod-play-id-48674-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电影', '剧情片', '2019', '俄罗斯', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (96, '罗米的理发馆', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157142340170714614.jpg', 'https://www.88ys.cc/vod-play-id-67894-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电影', '剧情片', '2019', '韩国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (97, '真实', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/156463242087684000.jpg', 'https://www.88ys.cc/vod-play-id-46876-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '金秀贤 / 成东日 / 韩智恩 / 李圣旻 / 崔雪莉', '李沙朗', '爱奇艺', '电影', '剧情片', '2019', '韩国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (98, '犯罪现场', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564643048086467561.jpg', 'https://www.88ys.cc/vod-play-id-26324-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电影', '剧情片', '2019', '韩国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (99, '匹夫', 'https://www.88ys.cc/vod-play-id-26324-src-1-num-1.html', 'https://www.88ys.cc/vod-play-id-67881-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '黄晓明,张译,张歆艺', '杨树鹏', '爱奇艺', '电影', '剧情片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (100, '告诉我我是谁', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157140180156041173.jpg', 'https://www.88ys.cc/vod-play-id-67876-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电影', '恐怖片', '2019', '英国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (101, '凶机恶煞', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157140180167536865.jpg', 'https://www.88ys.cc/vod-play-id-67875-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '艾米·汉莫,达科塔·约翰逊,莎姬·贝兹', '巴巴克·安瓦里', '爱奇艺', '电影', '恐怖片', '2019', '美国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (102, '快腿', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157139580281361315.jpg', 'https://www.88ys.cc/vod-play-id-67865-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '周宏伟 马迎春 桑兴尧', '张洪源', '爱奇艺', '电影', '动作片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (103, '恐怖故事2', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564646908268065262.jpg', 'https://www.88ys.cc/vod-play-id-19303-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '李赫秀,金瑟琪,白珍熙,金智媛,成俊', '郑范识,金辉,金成浩', '爱奇艺', '电影', '惊悚片', '2019', '韩国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (104, '\r\n狄仁杰之天神下凡', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157139580261783757.jpg', 'https://www.88ys.cc/vod-play-id-67866-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '李晟荣,赵帅', '黄毅、黄思远', '爱奇艺', '电影', '动作片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (105, '第三度嫌疑人', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-03/156465912860357067.jpg', 'https://www.88ys.cc/vod-play-id-50433-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '福山雅治 役所广司 广濑铃 ', '是枝裕和', '爱奇艺', '电影', '惊悚片', '2019', '日本', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (106, '风吹吧麦浪', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156789600550531344.jpg', 'https://www.88ys.cc/vod-play-id-65591-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '张燕妮,杨宗儒', '惠栋', '爱奇艺', '电影', '剧情片', '2019', '未知', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (107, '两个人的芭蕾', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131780281751532.jpg', 'https://www.88ys.cc/vod-play-id-67803-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '倪萍,李璐,奚美娟,艾丽娅', '电影 剧情片', '爱奇艺', '电影', '剧情片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (108, '\r\n水下·你未见的中国', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157093920219828627.jpg', 'https://www.88ys.cc/vod-play-id-67374-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '朱世一,施聪,赖瑗,卢梅,朱厚真,李维潇', '爱奇艺', '电影', '剧情片', '2019', '大陆', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (109, '攻击链2019', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157140120148733498.jpg', 'https://www.88ys.cc/vod-play-id-67871-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '尼古拉斯·凯奇', '肯·桑泽尔', '爱奇艺', '电影', '剧情片', '2019', '美国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (110, '\r\n登堂入室', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157142340294746275.jpg', 'https://www.88ys.cc/vod-play-id-67889-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '法布莱斯·鲁奇尼,恩斯特·吴默埃', '弗朗索瓦·欧容', '爱奇艺', '电影', '剧情片', '2019', '法国', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (111, '\r\n奥菲斯恋歌', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157140120171565371.jpg', 'https://www.88ys.cc/vod-play-id-67870-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', 'Jasper Joseph,Kristina Kostiv', '托尔·伊本', '爱奇艺', '电影', '剧情片', '2019', '其它', '未知', '2019-10-17 11:37', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (112, '生化危城', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564649010414753992.jpg', 'https://www.88ys.cc/vod-play-id-67876-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '文颂娴,王凯韦,关宝慧,施熙愉,叶永健', '未知', '爱奇艺', '电影', '动作片', '2003', '中国香港', '未知', '2017-12-06 05:52', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (113, '独家记忆', 'http://wxt.sinaimg.cn/large/007drMcOly1g80qvulmstj30780a0ta0.jpg', 'https://www.88ys.cc/vod-play-id-53993-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '派珀·劳瑞,布瑞特·迪尔,布鲁克·亚当斯,艾米丽·芭尔多尼', '梅勒妮·梅隆', '爱奇艺', '电影', '剧情片', '2018', '美国', '未知', '2019-10-17 05:26', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (114, '\r\n纯熟意外 粤语版', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157126260283974225.jpg', 'https://www.88ys.cc/vod-play-id-67743-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '吴启华,曹永廉,李施嬅,滕丽名', '陈耀全', '爱奇艺', '电影', '剧情片', '2016', '香港', '未知', '2019-10-17 02:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (115, '鱼眼王', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157127880196324796.jpg', 'https://www.88ys.cc/vod-play-id-67751-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '滩仪武,本上真奈美,石松葛兹,山本浩司', '寺内康太郎', '爱奇艺', '电影', ' 剧情片', '2009', '日本', '未知', '2019-10-17 02:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (116, '雪山惊魂', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-05/156465905366387882.jpg', 'https://www.88ys.cc/vod-play-id-51251-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', 'Ingrid Bolsø Berdal', 'Roar Uthaug', '爱奇艺', '电影', ' 惊悚片', '2006', '其它', '未知', '2019-10-17 02:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (117, '鼠来宝2', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564643038536561119.jpg', 'https://www.88ys.cc/vod-play-id-26363-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '杰西·麦卡尼,马修·格雷·古柏勒', '贝蒂·托马斯', '爱奇艺', '电影', '喜剧片', '2010', '美国', '未知', '2019-10-17 02:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (118, '我的个神啊', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/156466425006055422.jpg', 'https://www.88ys.cc/vod-play-id-6564-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '阿米尔·汗,安努舒卡·莎玛,桑杰·达特', '拉吉库马尔·希拉尼', '爱奇艺', '电影', '爱情片', '2015', '印度', '未知', '2019-10-17 02:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (141, '\r\n一起募资', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156749880211031072.jpg', 'https://www.88ys.cc/vod-play-id-65387-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '刘仁娜,卢洪哲,刘俊相', NULL, '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (142, '\r\n青年的力量第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157128360195775455.jpg', 'https://www.88ys.cc/vod-play-id-67761-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '乔杉 何冲 灵超 韩宇 杨和苏 陈哲远 艾力 张皓宸 张桃儿', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (143, '\r\n恋梦空间第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-07/15641049398.jpg', 'https://www.88ys.cc/vod-play-id-64072-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '吴昕,韩雪,马思超', '刘蕾', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (144, '\r\n遇见天坛', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156597180125877255.webp', 'https://www.88ys.cc/vod-play-id-64640-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '冯绍峰,苗苗,黄明昊,蔡徐坤,迪丽热巴', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (145, '\r\n寻情记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156551882022473729.jpg', 'https://www.88ys.cc/vod-play-id-64507-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (146, '青春', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145222121026782.jpg', 'https://www.88ys.cc/vod-play-id-67897-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (147, '\r\n女儿们的恋爱第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156596580161731115.jpg', 'https://www.88ys.cc/vod-play-id-64639-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '郑爽,郭碧婷,陈乔恩,徐璐', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (148, '第三调解室', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-04/15557548397.jpg', 'https://www.88ys.cc/vod-play-id-59324-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (149, '西伯利亚先遣队', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084740307041077.jpg', 'https://www.88ys.cc/vod-play-id-67271-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '李善均,金南佶,李相烨,金敏植', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (150, '新西游记外传：去冰岛的三餐', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156968880340013107.jpg', 'https://www.88ys.cc/vod-play-id-66503-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '殷志源,李寿根', '罗英石', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (151, '\r\n三时三餐山村篇', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-08/156551882624963333.jpg', 'https://www.88ys.cc/vod-play-id-64479-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '廉晶雅,尹世雅,朴素丹', '罗英石', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (152, '\r\n拜托了冰箱', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157120200223855130.jpg', 'https://www.88ys.cc/vod-play-id-67638-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (153, '目光交流/眼神交流', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156759480187935038.jpg', 'https://www.88ys.cc/vod-play-id-65437-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2008', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (154, '\r\n超人回来了', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-06/156460349380266499.jpg', 'https://www.88ys.cc/vod-play-id-51598-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', 'tablo,秋成勋,李辉才,张铉诚', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (155, '再次出发3/JTBC Begin Again3', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-07/156359563814.jpg', 'https://www.88ys.cc/vod-play-id-63787-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '이동희', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (156, '蒙面歌王', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-02/156463183243468777.jpg', 'https://www.88ys.cc/vod-play-id-48890-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (157, '\r\n家师父一体', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-04/15559899061.jpg', 'https://www.88ys.cc/vod-play-id-59444-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '金叡园/申宝拉/金九拉/洪恩熙/金亨石/池尚烈/金成柱', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (158, '玩家/Player', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-07/15631618612.jpg', 'https://www.88ys.cc/vod-play-id-63504-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '李寿根,李伊庚,金东炫,李龙真', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (159, 'Battle Trip', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-04/15559901701.jpg', 'https://www.88ys.cc/vod-play-id-59450-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '성시경,郑山 ,김숙,San E', '朴熙妍', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (160, '街头美食斗士第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157110900196734123.jpg', 'https://www.88ys.cc/vod-play-id-67559-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '白钟元', '朴熙妍', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (161, '草地状元', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-07/156460324879009840.jpg', 'https://www.88ys.cc/vod-play-id-52138-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '萧大陆,周筱云,席曼宁,盖克', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (162, '\r\n来自星星的事', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2018-12/156460175956831742.jpg', 'https://www.88ys.cc/vod-play-id-55611-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '庹宗康,许效舜,赵正平', '未知', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (163, '\r\n非你莫属[2019]', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-02/156459992552056196.png', 'https://www.88ys.cc/vod-play-id-56882-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '黄健翔，涂磊', '爱奇艺', '综艺', '综艺', '2019', '大陆', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (164, '恶评之夜', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-06/15614676591.jpg', 'https://www.88ys.cc/vod-play-id-62443-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '申东烨,金淑,金钟民,崔雪莉', '未知', '爱奇艺', '综艺', '综艺', '2019', '韩国', '未知', '2019-10-15 08:00', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (166, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (167, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (168, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 2);
INSERT INTO `ca_filminfo` VALUES (169, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 3);
INSERT INTO `ca_filminfo` VALUES (170, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 4);
INSERT INTO `ca_filminfo` VALUES (181, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-6.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 5);
INSERT INTO `ca_filminfo` VALUES (182, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-7.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 6);
INSERT INTO `ca_filminfo` VALUES (183, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-8.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 7);
INSERT INTO `ca_filminfo` VALUES (184, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-9.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 8);
INSERT INTO `ca_filminfo` VALUES (185, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-10.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 9);
INSERT INTO `ca_filminfo` VALUES (186, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-11.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 10);
INSERT INTO `ca_filminfo` VALUES (187, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-12.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 11);
INSERT INTO `ca_filminfo` VALUES (188, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-13.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 12);
INSERT INTO `ca_filminfo` VALUES (189, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-13.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 13);
INSERT INTO `ca_filminfo` VALUES (190, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-14.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 14);
INSERT INTO `ca_filminfo` VALUES (191, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-15.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 15);
INSERT INTO `ca_filminfo` VALUES (192, '可疑的岳母', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-05/15583351691.jpg', 'https://www.88ys.cc/vod-play-id-60699-src-1-num-16.html', '/liuzhuwodelinhun', '5.0', '申多恩,朴镇宇,金惠善,金正铉,孙宇赫', '李正勋', '爱奇艺', '电视剧', '剧情片', '2019', '韩国', '未知', '2017-12-06 05:52', '1', '0', '1', 16);
INSERT INTO `ca_filminfo` VALUES (194, '联邦调查局第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157023000273758421.jpg', 'https://www.88ys.cc/vod-play-id-66871-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '埃博尼·诺埃尔,杰瑞米·西斯托,雪拉·渥德', '尼克·戈麦斯', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 08:26', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (195, '联邦调查局第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157023000273758421.jpg', 'https://www.88ys.cc/vod-play-id-66871-src-1-num-2.html66871-src-2-num-1.html', '/liuzhuwodelinhun', '5.0', '埃博尼·诺埃尔,杰瑞米·西斯托,雪拉·渥德', '尼克·戈麦斯', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 08:26', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (196, '联邦调查局第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157023000273758421.jpg', 'https://www.88ys.cc/vod-play-id-66871-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '埃博尼·诺埃尔,杰瑞米·西斯托,雪拉·渥德', '尼克·戈麦斯', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 08:26', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (197, '联邦调查局第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157023000273758421.jpg', 'https://www.88ys.cc/vod-play-id-66871-src-4-num-4.html', '/liuzhuwodelinhun', '5.0', '埃博尼·诺埃尔,杰瑞米·西斯托,雪拉·渥德', '尼克·戈麦斯', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 08:26', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (198, '邪恶第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084744569146690.jpg\"', 'https://www.88ys.cc/vod-play-id-67268-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '麦克·柯尔特,迈克尔·爱默生', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:58', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (199, '邪恶第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084744569146690.jpg\"', 'https://www.88ys.cc/vod-play-id-67268-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '麦克·柯尔特,迈克尔·爱默生', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:58', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (200, '邪恶第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084744569146690.jpg\"', 'https://www.88ys.cc/vod-play-id-67268-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '麦克·柯尔特,迈克尔·爱默生', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:58', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (201, '邪恶第一季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157084744569146690.jpg\"', 'https://www.88ys.cc/vod-play-id-67268-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '麦克·柯尔特,迈克尔·爱默生', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:58', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (202, '在水一方', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-02/156459970255869985.jpg', 'https://www.88ys.cc/vod-play-id-56945-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', 'Natalie Panalee,Ohm Kanin', '阿丽莎拉·翁差丽', '爱奇艺', '电视剧', '泰剧', '2019', '泰国', '未知', '2019-10-13 05:36', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (203, '在水一方', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-02/156459970255869985.jpg', 'https://www.88ys.cc/vod-play-id-56945-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', 'Natalie Panalee,Ohm Kanin', '阿丽莎拉·翁差丽', '爱奇艺', '电视剧', '泰剧', '2019', '泰国', '未知', '2019-10-13 05:36', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (204, '在水一方', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-02/156459970255869985.jpg', 'https://www.88ys.cc/vod-play-id-56945-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', 'Natalie Panalee,Ohm Kanin', '阿丽莎拉·翁差丽', '爱奇艺', '电视剧', '泰剧', '2019', '泰国', '未知', '2019-10-13 05:36', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (205, '在水一方', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-02/156459970255869985.jpg', 'https://www.88ys.cc/vod-play-id-56945-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', 'Natalie Panalee,Ohm Kanin', '阿丽莎拉·翁差丽', '爱奇艺', '电视剧', '泰剧', '2019', '泰国', '未知', '2019-10-13 05:36', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (206, '吸血鬼后裔第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157080360206354598.jpg', 'https://www.88ys.cc/vod-play-id-67242-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 10:23', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (207, '吸血鬼后裔第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157080360206354598.jpg', 'https://www.88ys.cc/vod-play-id-67242-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 10:23', '1', '0', '1', 2);
INSERT INTO `ca_filminfo` VALUES (208, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (209, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (210, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (211, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (212, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 5);
INSERT INTO `ca_filminfo` VALUES (213, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-6.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 6);
INSERT INTO `ca_filminfo` VALUES (214, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-7.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 7);
INSERT INTO `ca_filminfo` VALUES (215, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-8.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 8);
INSERT INTO `ca_filminfo` VALUES (216, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-9.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 9);
INSERT INTO `ca_filminfo` VALUES (217, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-10.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 10);
INSERT INTO `ca_filminfo` VALUES (218, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-11.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 11);
INSERT INTO `ca_filminfo` VALUES (219, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-12.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 12);
INSERT INTO `ca_filminfo` VALUES (220, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-13.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 13);
INSERT INTO `ca_filminfo` VALUES (221, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-14.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 14);
INSERT INTO `ca_filminfo` VALUES (222, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-15.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 15);
INSERT INTO `ca_filminfo` VALUES (224, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-16.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 16);
INSERT INTO `ca_filminfo` VALUES (225, '\r\n刑警战记', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145220812407982.jpg', 'https://www.88ys.cc/vod-play-id-67902-src-1-num-17.html', '/liuzhuwodelinhun', '5.0', '范雨林,郭柯彤', '徐杰', '爱奇艺', '电视剧', '国产剧', '2019', '中国', '未知', '2019-10-19 10:53', '1', '1', '1', 17);
INSERT INTO `ca_filminfo` VALUES (245, '相棒第十八季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145222564014085.jpg', 'https://www.88ys.cc/vod-play-id-67895-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '水谷丰 , 反町隆史', '桥本一', '爱奇艺', '电视剧', '日韩剧', '2019', '日本', '未知', '2019-10-19 10:49', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (246, '相棒第十八季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145222564014085.jpg', 'https://www.88ys.cc/vod-play-id-67895-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '水谷丰 , 反町隆史', '桥本一', '爱奇艺', '电视剧', '日韩剧', '2019', '日本', '未知', '2019-10-19 10:49', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (247, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (248, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (249, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (250, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (251, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 5);
INSERT INTO `ca_filminfo` VALUES (253, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-6.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 6);
INSERT INTO `ca_filminfo` VALUES (254, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-7.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 7);
INSERT INTO `ca_filminfo` VALUES (255, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-8.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 8);
INSERT INTO `ca_filminfo` VALUES (256, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-9.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 9);
INSERT INTO `ca_filminfo` VALUES (257, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-10.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 10);
INSERT INTO `ca_filminfo` VALUES (258, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-11.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 11);
INSERT INTO `ca_filminfo` VALUES (259, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-12.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 12);
INSERT INTO `ca_filminfo` VALUES (260, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-13.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 13);
INSERT INTO `ca_filminfo` VALUES (261, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-14.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 14);
INSERT INTO `ca_filminfo` VALUES (263, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-15.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 15);
INSERT INTO `ca_filminfo` VALUES (264, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-16.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 16);
INSERT INTO `ca_filminfo` VALUES (266, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-17.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 17);
INSERT INTO `ca_filminfo` VALUES (267, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-18.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 18);
INSERT INTO `ca_filminfo` VALUES (268, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-19.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 19);
INSERT INTO `ca_filminfo` VALUES (269, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-20.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 20);
INSERT INTO `ca_filminfo` VALUES (270, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-21.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 21);
INSERT INTO `ca_filminfo` VALUES (271, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-22.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 22);
INSERT INTO `ca_filminfo` VALUES (272, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-23.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 23);
INSERT INTO `ca_filminfo` VALUES (273, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-24.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 24);
INSERT INTO `ca_filminfo` VALUES (274, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-25.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 25);
INSERT INTO `ca_filminfo` VALUES (275, '我和僵尸有个约会', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2017-12/1564645860280236543.jpg', 'https://www.88ys.cc/vod-play-id-21814-src-1-num-26.html', '/liuzhuwodelinhun', '5.0', '杨恭如,万绮雯,尹天照', '陈新侠', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 03:38', '1', '1', '1', 26);
INSERT INTO `ca_filminfo` VALUES (276, 'CHEAT～各位欺诈师请注意～', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157087560201535908.jpg', 'https://www.88ys.cc/vod-play-id-67297-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '本田翼,金子大地,风间俊介', '汤浅典子,安見悟朗', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:59', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (277, 'CHEAT～各位欺诈师请注意～', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157087560201535908.jpg', 'https://www.88ys.cc/vod-play-id-67297-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '本田翼,金子大地,风间俊介', '汤浅典子,安見悟朗', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:59', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (278, 'CHEAT～各位欺诈师请注意～', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157087560201535908.jpg', 'https://www.88ys.cc/vod-play-id-67297-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '本田翼,金子大地,风间俊介', '汤浅典子,安見悟朗', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:59', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (279, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (280, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 2);
INSERT INTO `ca_filminfo` VALUES (281, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 3);
INSERT INTO `ca_filminfo` VALUES (282, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 4);
INSERT INTO `ca_filminfo` VALUES (283, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 5);
INSERT INTO `ca_filminfo` VALUES (284, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-6.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 6);
INSERT INTO `ca_filminfo` VALUES (285, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-7.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 7);
INSERT INTO `ca_filminfo` VALUES (286, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-8.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 8);
INSERT INTO `ca_filminfo` VALUES (287, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-9.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 9);
INSERT INTO `ca_filminfo` VALUES (288, '没有秘密的你', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157131840172771316.jpg', 'https://www.88ys.cc/vod-play-id-67802-src-1-num-10.html', '/liuzhuwodelinhun', '5.0', '戚薇,金瀚,王阳明,黄梦莹', '于中中', '爱奇艺', '电视剧', '国产剧', '2019', '大陆', '未知', '2019-10-18 10:38', '1', '0', '1', 10);
INSERT INTO `ca_filminfo` VALUES (289, '单身家长第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156958020129588662.jpg', 'https://www.88ys.cc/vod-play-id-66445-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 01:16', '1', '0', '1', 1);
INSERT INTO `ca_filminfo` VALUES (290, '单身家长第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156958020129588662.jpg', 'https://www.88ys.cc/vod-play-id-66445-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 01:16', '1', '0', '1', 2);
INSERT INTO `ca_filminfo` VALUES (291, '单身家长第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156958020129588662.jpg', 'https://www.88ys.cc/vod-play-id-66445-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 01:16', '1', '0', '1', 3);
INSERT INTO `ca_filminfo` VALUES (292, '单身家长第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156958020129588662.jpg', 'https://www.88ys.cc/vod-play-id-66445-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '未知', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 01:16', '1', '0', '1', 4);
INSERT INTO `ca_filminfo` VALUES (293, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (294, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (295, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (296, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (297, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (298, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-6.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (299, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-7.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (300, '\r\n完美之声', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156965460173069300.jpg', 'https://www.88ys.cc/vod-play-id-66478-src-1-num-8.html', '/liuzhuwodelinhun', '5.0', '布莱德利·惠特福德,安娜·坎普', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 02:28', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (301, '很便宜，千里马超市', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156903600143445258.jpg', 'https://www.88ys.cc/vod-play-id-66218-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '李东辉,金炳哲,朴浩山', '未知', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:11', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (302, '很便宜，千里马超市', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156903600143445258.jpg', 'https://www.88ys.cc/vod-play-id-66218-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '李东辉,金炳哲,朴浩山', '未知', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:11', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (303, '很便宜，千里马超市', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-09/156903600143445258.jpg', 'https://www.88ys.cc/vod-play-id-66218-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '李东辉,金炳哲,朴浩山', '未知', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-19 11:11', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (304, '\r\n解决师粤语', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157107780211893902.jpg', 'https://www.88ys.cc/vod-play-id-67528-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '王浩信,唐诗咏,陈敏之', '刘家豪,徐遇安,陈志江', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 11:09', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (305, '\r\n解决师粤语', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157107780211893902.jpg', 'https://www.88ys.cc/vod-play-id-67528-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '王浩信,唐诗咏,陈敏之', '刘家豪,徐遇安,陈志江', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 11:09', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (306, '\r\n解决师粤语', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157107780211893902.jpg', 'https://www.88ys.cc/vod-play-id-67528-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '王浩信,唐诗咏,陈敏之', '刘家豪,徐遇安,陈志江', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 11:09', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (307, '\r\n解决师粤语', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157107780211893902.jpg', 'https://www.88ys.cc/vod-play-id-67528-src-1-num-4.html', '/liuzhuwodelinhun', '5.0', '王浩信,唐诗咏,陈敏之', '刘家豪,徐遇安,陈志江', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 11:09', '1', '1', '1', 4);
INSERT INTO `ca_filminfo` VALUES (308, '\r\n解决师粤语', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157107780211893902.jpg', 'https://www.88ys.cc/vod-play-id-67528-src-1-num-5.html', '/liuzhuwodelinhun', '5.0', '王浩信,唐诗咏,陈敏之', '刘家豪,徐遇安,陈志江', '爱奇艺', '电视剧', '港台剧', '2019', '香港', '未知', '2019-10-19 11:09', '1', '1', '1', 5);
INSERT INTO `ca_filminfo` VALUES (309, '\r\n邪恶力量第十五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157080480181273333.jpg', 'https://www.88ys.cc/vod-play-id-67246-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '贾德·帕达里克,詹森·阿克斯', '约翰·F·修华特', '爱奇艺', '电视剧', ' 欧美剧', '2019', '美国', '未知', '2019-10-18 06:42', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (310, '\r\n邪恶力量第十五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157080480181273333.jpg', 'https://www.88ys.cc/vod-play-id-67246-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '贾德·帕达里克,詹森·阿克斯', '约翰·F·修华特', '爱奇艺', '电视剧', ' 欧美剧', '2019', '美国', '未知', '2019-10-18 06:42', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (311, '\r\n邪恶力量第十五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157080480181273333.jpg', 'https://www.88ys.cc/vod-play-id-67246-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '贾德·帕达里克,詹森·阿克斯', '约翰·F·修华特', '爱奇艺', '电视剧', ' 欧美剧', '2019', '美国', '未知', '2019-10-18 06:42', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (312, 'G弦上的你和我', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157121820202511900.jpg', 'https://www.88ys.cc/vod-play-id-67674-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '波瑠,中川大志,松下由树', '福田亮介', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-17 06:05', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (313, '同期的小樱', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157079220223725291.jpg', 'https://www.88ys.cc/vod-play-id-67227-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '高畑充希,桥本爱', '明石広人,南云圣一', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-17 06:05', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (314, '同期的小樱', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157079220223725291.jpg', 'https://www.88ys.cc/vod-play-id-67227-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '高畑充希,桥本爱', '明石広人,南云圣一', '爱奇艺', '电视剧', '日韩剧', '2019', '韩国', '未知', '2019-10-17 06:05', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (315, '脉冲第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145460329058750.jpg', 'https://www.88ys.cc/vod-play-id-67915-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '恩卡·奥库玛,克雷格·阿诺德', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 11:41', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (316, '\r\n伴娘的秘密', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157145460299202384.jpg', 'https://www.88ys.cc/vod-play-id-67917-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '凯蒂·麦克格雷斯', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-19 11:41', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (317, '绿箭侠第八季\r\n绿箭侠第八季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157123560197873515.jpg', 'https://www.88ys.cc/vod-play-id-67734-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '查理·班尼特,凯蒂·卡西迪', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 12:25', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (318, '\r\n康纳一家第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157111200265459720.jpg', 'https://www.88ys.cc/vod-play-id-67569-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '劳里·梅特卡夫,莎拉·吉尔伯特', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:59', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (319, '\r\n康纳一家第二季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157111200265459720.jpg', 'https://www.88ys.cc/vod-play-id-67569-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '劳里·梅特卡夫,莎拉·吉尔伯特', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-18 03:59', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (320, '罗马三贱客第五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157132680283788224.jpg', 'https://www.88ys.cc/vod-play-id-67807-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '汤姆·罗森塔尔,乔尔·弗莱,瑞恩·山普森', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-17 11:44', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (321, '罗马三贱客第五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157132680283788224.jpg', 'https://www.88ys.cc/vod-play-id-67807-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '汤姆·罗森塔尔,乔尔·弗莱,瑞恩·山普森', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-17 11:44', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (322, '罗马三贱客第五季', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157132680283788224.jpg', 'https://www.88ys.cc/vod-play-id-67807-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '汤姆·罗森塔尔,乔尔·弗莱,瑞恩·山普森', '未知', '爱奇艺', '电视剧', '欧美剧', '2019', '美国', '未知', '2019-10-17 11:44', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (323, '南茜的情史', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157122960208305436.jpg', 'https://www.88ys.cc/vod-play-id-67707-src-1-num-1.html', '/liuzhuwodelinhun', '5.0', '蕾切尔·斯特灵', '杰弗里·萨克斯', '爱奇艺', '电视剧', '欧美剧', '2002', '美国', '未知', '2019-10-16 11:54', '1', '1', '1', 1);
INSERT INTO `ca_filminfo` VALUES (324, '南茜的情史', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157122960208305436.jpg', 'https://www.88ys.cc/vod-play-id-67707-src-1-num-2.html', '/liuzhuwodelinhun', '5.0', '蕾切尔·斯特灵', '杰弗里·萨克斯', '爱奇艺', '电视剧', '欧美剧', '2002', '美国', '未知', '2019-10-16 11:54', '1', '1', '1', 2);
INSERT INTO `ca_filminfo` VALUES (325, '南茜的情史', 'https://cdn.aqdstatic.com:966/88ys/upload/vod/2019-10/157122960208305436.jpg', 'https://www.88ys.cc/vod-play-id-67707-src-1-num-3.html', '/liuzhuwodelinhun', '5.0', '蕾切尔·斯特灵', '杰弗里·萨克斯', '爱奇艺', '电视剧', '欧美剧', '2002', '美国', '未知', '2019-10-16 11:54', '1', '1', '1', 3);
INSERT INTO `ca_filminfo` VALUES (326, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (327, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (328, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (329, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (330, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (331, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (332, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (333, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (334, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (335, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (336, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (337, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (338, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (339, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (340, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (341, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (342, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ca_filminfo` VALUES (343, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for cz_history
-- ----------------------------
DROP TABLE IF EXISTS `cz_history`;
CREATE TABLE `cz_history`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `film_id` int(11) NULL DEFAULT NULL COMMENT '影片id',
  `add_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '观看时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cz_history
-- ----------------------------
INSERT INTO `cz_history` VALUES (2, 1, 7, '12345676543');
INSERT INTO `cz_history` VALUES (3, 1, 8, '12345676543');
INSERT INTO `cz_history` VALUES (4, 1, 9, '12345676543');
INSERT INTO `cz_history` VALUES (5, 1, 10, '12345676543');
INSERT INTO `cz_history` VALUES (6, 12, 1, '12345676543');
INSERT INTO `cz_history` VALUES (7, 12, 1, '12345676543');
INSERT INTO `cz_history` VALUES (8, 12, 1, '12345676543');
INSERT INTO `cz_history` VALUES (9, 7, 3, '12345676543');
INSERT INTO `cz_history` VALUES (10, 7, 8, '12345676543');
INSERT INTO `cz_history` VALUES (11, 7, 4, '12345676543');
INSERT INTO `cz_history` VALUES (12, 7, 1, '12345676543');

-- ----------------------------
-- Table structure for cz_ip_address
-- ----------------------------
DROP TABLE IF EXISTS `cz_ip_address`;
CREATE TABLE `cz_ip_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问id',
  `film_id` int(11) NULL DEFAULT NULL COMMENT '影片id',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '影片地址',
  `url_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url的hash值',
  `film_num` int(11) NULL DEFAULT NULL COMMENT '集数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cz_message
-- ----------------------------
DROP TABLE IF EXISTS `cz_message`;
CREATE TABLE `cz_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `add_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论时间',
  `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `film_id` int(11) NULL DEFAULT NULL COMMENT '影片id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cz_message
-- ----------------------------
INSERT INTO `cz_message` VALUES (3, '我是荒野大嫖客', '还行，有些资源确实不好找', '12345676543', '2', 3);
INSERT INTO `cz_message` VALUES (5, '徐晃是我', '来了来了，这个还可以', '12345676543', '4', 3);
INSERT INTO `cz_message` VALUES (6, '虎子', '哦吼吼', '12345676543', '5', 4);
INSERT INTO `cz_message` VALUES (7, '江湖小剪刀', '来了来了，他真的来了', '12345676543', '4', 5);
INSERT INTO `cz_message` VALUES (8, '小世界是我', '天哪，真的好看。支持', '12345676543', '3', 2);
INSERT INTO `cz_message` VALUES (13, '李晓雨', '31313123', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (17, '张宇', '如', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (19, '祥林嫂', '如', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (20, '祥林嫂', '如', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (21, '祥林嫂', '如', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (24, '祥林嫂', '认为我', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (25, '祥林嫂', '撒旦', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (26, '祥林嫂', '安定', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (27, NULL, 'sdcs', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (28, NULL, 'sdcssdc', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (29, NULL, 'sdcssdc', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (30, NULL, 'sdcssdc', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (31, NULL, 'sdcssdc', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (32, NULL, '43', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (33, NULL, 'ss', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (34, NULL, 'ss', '12345676543', NULL, NULL);
INSERT INTO `cz_message` VALUES (35, NULL, 'ss', '1571367973160', NULL, NULL);
INSERT INTO `cz_message` VALUES (36, NULL, 'ss', '1571367973419', NULL, NULL);
INSERT INTO `cz_message` VALUES (37, NULL, 'ss', '1571367973647', NULL, NULL);
INSERT INTO `cz_message` VALUES (38, NULL, 'ss', '1571367973860', NULL, NULL);
INSERT INTO `cz_message` VALUES (39, NULL, 'ss', '1571367974081', NULL, NULL);
INSERT INTO `cz_message` VALUES (40, NULL, 'ss', '1571367974276', NULL, NULL);
INSERT INTO `cz_message` VALUES (41, NULL, 'ss', '1571367974466', NULL, NULL);
INSERT INTO `cz_message` VALUES (42, NULL, 'ss', '1571367987274', NULL, NULL);
INSERT INTO `cz_message` VALUES (43, NULL, 'ss', '1571368115786', NULL, NULL);
INSERT INTO `cz_message` VALUES (44, NULL, '阿迪斯', '1571383390665', NULL, NULL);
INSERT INTO `cz_message` VALUES (45, NULL, '阿迪斯', '1571383405702', NULL, NULL);
INSERT INTO `cz_message` VALUES (46, NULL, '四川', '1571383411090', NULL, NULL);
INSERT INTO `cz_message` VALUES (47, NULL, '四川', '1571383423174', NULL, NULL);
INSERT INTO `cz_message` VALUES (48, NULL, '四川', '1571383478699', NULL, NULL);
INSERT INTO `cz_message` VALUES (49, NULL, '撒', '1571383490272', NULL, NULL);
INSERT INTO `cz_message` VALUES (50, NULL, '剩下的', '1571383503732', NULL, NULL);
INSERT INTO `cz_message` VALUES (51, NULL, '剩下的', '1571384319730', NULL, NULL);
INSERT INTO `cz_message` VALUES (52, NULL, '撒旦', '1571384323780', NULL, NULL);
INSERT INTO `cz_message` VALUES (53, NULL, '大王', '1571384346655', NULL, NULL);
INSERT INTO `cz_message` VALUES (54, NULL, '大王', '1571384395706', NULL, NULL);
INSERT INTO `cz_message` VALUES (55, NULL, '大王', '1571384622152', NULL, NULL);
INSERT INTO `cz_message` VALUES (56, NULL, '', '1571385033360', NULL, NULL);
INSERT INTO `cz_message` VALUES (57, NULL, '', '1571385033827', NULL, NULL);
INSERT INTO `cz_message` VALUES (58, NULL, '123', '1571385081495', NULL, NULL);
INSERT INTO `cz_message` VALUES (59, NULL, ' 321', '1571385084460', NULL, NULL);
INSERT INTO `cz_message` VALUES (60, NULL, '', '1571385413131', NULL, NULL);
INSERT INTO `cz_message` VALUES (61, NULL, ' ', '1571385418747', NULL, NULL);
INSERT INTO `cz_message` VALUES (62, NULL, '', '1571385650628', NULL, NULL);
INSERT INTO `cz_message` VALUES (63, NULL, 'fw', '1571385656390', NULL, NULL);
INSERT INTO `cz_message` VALUES (64, NULL, '', '1571385678195', NULL, NULL);
INSERT INTO `cz_message` VALUES (65, NULL, '321312', '1571483836145', NULL, NULL);
INSERT INTO `cz_message` VALUES (66, NULL, '321312', '1571483838546', NULL, NULL);
INSERT INTO `cz_message` VALUES (67, NULL, '321312', '1571483838802', NULL, NULL);
INSERT INTO `cz_message` VALUES (68, NULL, '321312', '1571483839074', NULL, NULL);
INSERT INTO `cz_message` VALUES (69, NULL, '321312', '1571483839370', NULL, NULL);
INSERT INTO `cz_message` VALUES (70, NULL, '', '1571483885075', NULL, NULL);
INSERT INTO `cz_message` VALUES (71, NULL, '321312', '1571483887714', NULL, NULL);
INSERT INTO `cz_message` VALUES (72, NULL, '321312', '1571483887953', NULL, NULL);
INSERT INTO `cz_message` VALUES (73, NULL, '321312', '1571483888234', NULL, NULL);
INSERT INTO `cz_message` VALUES (74, NULL, '321312', '1571483888513', NULL, NULL);
INSERT INTO `cz_message` VALUES (75, NULL, '321312', '1571483888761', NULL, NULL);
INSERT INTO `cz_message` VALUES (76, NULL, '丹爷真好看', '1571621374783', NULL, NULL);
INSERT INTO `cz_message` VALUES (77, NULL, '丹爷真好看', '1571621375857', NULL, NULL);
INSERT INTO `cz_message` VALUES (78, NULL, '丹爷真好看', '1571621376007', NULL, NULL);
INSERT INTO `cz_message` VALUES (79, NULL, '丹爷真好看', '1571621376172', NULL, NULL);
INSERT INTO `cz_message` VALUES (80, NULL, '丹爷真好看', '1571621376363', NULL, NULL);
INSERT INTO `cz_message` VALUES (81, NULL, '丹爷真好看', '1571621377194', NULL, NULL);
INSERT INTO `cz_message` VALUES (82, NULL, '丹爷真好看', '1571621377381', NULL, NULL);
INSERT INTO `cz_message` VALUES (83, NULL, '丹爷真好看', '1571621377602', NULL, NULL);
INSERT INTO `cz_message` VALUES (84, NULL, '丹爷真好看', '1571621459942', NULL, NULL);
INSERT INTO `cz_message` VALUES (85, NULL, '丹爷真好看', '1571621462821', NULL, NULL);
INSERT INTO `cz_message` VALUES (86, NULL, '丹爷真好看', '1571621463011', NULL, NULL);
INSERT INTO `cz_message` VALUES (87, NULL, '丹爷真好看', '1571621463177', NULL, NULL);

-- ----------------------------
-- Table structure for cz_order
-- ----------------------------
DROP TABLE IF EXISTS `cz_order`;
CREATE TABLE `cz_order`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '订单号',
  `add_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单时间',
  `money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付金额',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付状态',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `start_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cz_order
-- ----------------------------
INSERT INTO `cz_order` VALUES (2, 134566666, '12345672222', '15', '1', 1, '11111111111', '11111111122');
INSERT INTO `cz_order` VALUES (4, 134566666, '12345672222', '15', '1', 4, '11111111111', '11111111111');
INSERT INTO `cz_order` VALUES (5, 134566666, '12345672222', '15', '1', 5, '11111111111', '11111111111');
INSERT INTO `cz_order` VALUES (6, 134566666, '12345672222', '15', '1', 6, '11111111111', '11111111111');
INSERT INTO `cz_order` VALUES (7, 134566666, '12345672222', '15', '1', 7, '11111111111', '11111111111');
INSERT INTO `cz_order` VALUES (8, 134566666, '12345672222', '15', '1', 8, '11111111111', '11111111111');
INSERT INTO `cz_order` VALUES (11, NULL, '1571489426840', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (12, NULL, '1571489522739', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (13, NULL, '1571489642741', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (14, NULL, '1571489668726', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (15, NULL, '1571489687315', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (16, NULL, '1571489788728', '5', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (17, NULL, '1571489879843', '15', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (18, NULL, '1571490029419', '15', NULL, NULL, NULL, NULL);
INSERT INTO `cz_order` VALUES (20, 1571490290, '1571490290247', '5', '1', 2, '1571490290247', '1571492882247');
INSERT INTO `cz_order` VALUES (21, 1571491892, '1571491891859', '5', '1', 10, '1571491891859', '1571494483859');
INSERT INTO `cz_order` VALUES (32, 1571551350, '1571551349853', '5', '1', 41, '1571551349853', '1571553941853');

-- ----------------------------
-- Table structure for cz_recharge
-- ----------------------------
DROP TABLE IF EXISTS `cz_recharge`;
CREATE TABLE `cz_recharge`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `order_id` int(11) NOT NULL COMMENT '关联订单表  订单id',
  `start_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cz_recharge
-- ----------------------------
INSERT INTO `cz_recharge` VALUES (1, 1, 1, '11111111111', '11111154311');
INSERT INTO `cz_recharge` VALUES (2, 1, 1, '11111111111', '11111154311');
INSERT INTO `cz_recharge` VALUES (3, 1, 1, '11111111111', '11111154311');
INSERT INTO `cz_recharge` VALUES (4, 1, 1, '11111111111', '11111154311');
INSERT INTO `cz_recharge` VALUES (5, 2, 1, '11111111111', '11111154311');
INSERT INTO `cz_recharge` VALUES (6, 1, 1, '11111', '222222');
INSERT INTO `cz_recharge` VALUES (7, 2, 2, '111111', '11111111');
INSERT INTO `cz_recharge` VALUES (8, 6, 3, '2222', '23333');

-- ----------------------------
-- Table structure for cz_user
-- ----------------------------
DROP TABLE IF EXISTS `cz_user`;
CREATE TABLE `cz_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `head_img` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `sex` enum('男','女','保存') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `state` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '默认0,1禁用,0正常',
  `add_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cz_user
-- ----------------------------
INSERT INTO `cz_user` VALUES (1, '张宇', '123123', '123123', '', '女', '1', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (4, '陈泽军', '38215215', '13892432254', '', '男', '1', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (6, '祥林嫂', '8465132', '15369215314', '', '女', '0', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (7, '小杜杜', '61584652', '13698544522', 'http://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&step_word=&hs=0&pn=219&spn=0&di=9240&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=1650868194%2C3331868551&os=1545465569%2C4222537464&simid=3040939992%2C3460059789&adpicid=0&lpn=0&ln=3064&fr=&fmq=1390280702008_R&fm=&ic=0&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201901%2F04%2F20190104222555_Rvvyu.thumb.700_0.jpeg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3D8anlnn8dcl&gsm=&rpstart=0&rpnum=0&islist=&querylist=&force=undefined', '女', '1', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (8, '小萱萱', '8464162', '13952254554', 'http://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&step_word=&hs=0&pn=46&spn=0&di=62920&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=319134109%2C2293704912&os=1993946760%2C593231084&simid=0%2C0&adpicid=0&lpn=0&ln=3064&fr=&fmq=1390280702008_R&fm=&ic=0&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=http%3A%2F%2Fimg4.duitang.com%2Fuploads%2Fitem%2F201510%2F24%2F20151024100752_cX4K2.thumb.700_0.jpeg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3D90lb98bdl&gsm=&rpstart=0&rpnum=0&islist=&querylist=&force=undefined', '女', '0', '2019-10-03 21:24:35');
INSERT INTO `cz_user` VALUES (9, '小凯凯', '12686201', '13689415617', 'http://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&step_word=&hs=0&pn=169&spn=0&di=53900&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=2563687107%2C70361612&os=1607392338%2C3885450904&simid=0%2C0&adpicid=0&lpn=0&ln=3064&fr=&fmq=1390280702008_R&fm=&ic=0&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201609%2F30%2F20160930194238_PwxNj.thumb.700_0.jpeg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3Dm99lnm0bl&gsm=&rpstart=0&rpnum=0&islist=&querylist=&force=undefined', '男', '1', '2019-10-11 21:25:33');
INSERT INTO `cz_user` VALUES (10, '小卓卓', '65165952', '13865559162', 'http://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&step_word=&hs=0&pn=197&spn=0&di=18590&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=2&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=-1&cs=328820871%2C406442716&os=4275979515%2C385585085&simid=4100842428%2C648873958&adpicid=0&lpn=0&ln=3064&fr=&fmq=1390280702008_R&fm=&ic=0&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=&height=&face=undefined&ist=&jit=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201603%2F17%2F20160317214310_mP4ke.jpeg&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3Dc9b8c0m88&gsm=&rpstart=0&rpnum=0&islist=&querylist=&force=undefined', '男', '1', '2019-10-09 21:26:22');
INSERT INTO `cz_user` VALUES (12, '二哈', '685320520', '11369258517', '', '男', '1', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (13, '白雪公主', '96326512', '12065254585', '', '女', '0', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (14, '张大炮', '48965114', '13684265141', 'http://845123', '男', '0', '2019-10-09 21:23:09');
INSERT INTO `cz_user` VALUES (15, '张大答', '48965114', '13684265141', NULL, '男', '1', '1571315770623');
INSERT INTO `cz_user` VALUES (17, '12321', '123213', NULL, NULL, '女', NULL, '1571363246817');
INSERT INTO `cz_user` VALUES (18, '2112', '1221', NULL, NULL, '女', NULL, '1571364428735');
INSERT INTO `cz_user` VALUES (19, '2121', '212112', NULL, NULL, '女', NULL, '1571381777345');
INSERT INTO `cz_user` VALUES (20, '张大宇', '498651320', '984651230', NULL, '男', '1', '1571447583041');
INSERT INTO `cz_user` VALUES (21, '', '', NULL, NULL, NULL, NULL, '1571450580406');
INSERT INTO `cz_user` VALUES (22, '111111', '1111111', NULL, NULL, NULL, NULL, '1571450936543');
INSERT INTO `cz_user` VALUES (23, '12121212', '1111111', NULL, NULL, NULL, NULL, '1571451024955');
INSERT INTO `cz_user` VALUES (24, '2222222', '111111', NULL, NULL, NULL, NULL, '1571451135736');
INSERT INTO `cz_user` VALUES (25, '12121221', '111111', NULL, NULL, NULL, NULL, '1571451199729');
INSERT INTO `cz_user` VALUES (26, '1111111', '11111111', NULL, NULL, NULL, NULL, '1571451823719');
INSERT INTO `cz_user` VALUES (27, '11111111', '1111111', NULL, NULL, NULL, NULL, '1571452464374');
INSERT INTO `cz_user` VALUES (28, '111111111', '1111111', NULL, NULL, NULL, NULL, '1571453101412');
INSERT INTO `cz_user` VALUES (29, '222222', '111111', NULL, NULL, '女', NULL, '1571453252266');
INSERT INTO `cz_user` VALUES (30, '1221212121', '222222', NULL, 'blob:http://127.0.0.1:8000/5af32fed-44b5-4170-aa00-f5712f1e2757', '', NULL, '1571457609854');
INSERT INTO `cz_user` VALUES (31, '5555555', '111111', NULL, 'blob:http://127.0.0.1:8000/f0984d9a-90d5-4bf5-b9b7-2b9abc098c85', '', NULL, '1571467648403');
INSERT INTO `cz_user` VALUES (32, '666666', '111111', NULL, 'blob:http://127.0.0.1:8000/c5c65e4d-5165-4969-9a2b-814c3d17dd62', '', NULL, '1571468316895');
INSERT INTO `cz_user` VALUES (33, '111222', '111111', NULL, NULL, '', NULL, '1571533369349');
INSERT INTO `cz_user` VALUES (34, '5555555555', '555555', NULL, '/img/zyz11.jpg', '', NULL, '1571536210801');
INSERT INTO `cz_user` VALUES (35, '777777', '7777777', NULL, '/img/zyz4.jpg', '', NULL, '1571536373427');
INSERT INTO `cz_user` VALUES (36, '66666666', '111111', NULL, '/img/zyz11.jpg', '', NULL, '1571541904485');
INSERT INTO `cz_user` VALUES (37, '121221', '1212211', NULL, '/img/zyz11.jpg', '', NULL, '1571548035326');
INSERT INTO `cz_user` VALUES (38, '1212212', '12122121', NULL, '/img/zyz3.jpg', '', NULL, '1571548173100');
INSERT INTO `cz_user` VALUES (39, '12122112', '121221121', NULL, '/img/zyz11.jpg', '', NULL, '1571548290687');
INSERT INTO `cz_user` VALUES (40, '12122113', '121221131', NULL, '/img/zyz11.jpg', '', NULL, '1571548371939');
INSERT INTO `cz_user` VALUES (41, '7777777', '777777', NULL, '/img/zyz11.jpg', '', '1', '1571549235384');
INSERT INTO `cz_user` VALUES (42, '小杜杜4444', '61584652', NULL, NULL, '', NULL, '1571549461009');
INSERT INTO `cz_user` VALUES (43, '10101010', '111111', NULL, '/img/zyz4.jpg', '', NULL, '1571551517105');
INSERT INTO `cz_user` VALUES (44, '小杜杜123', '123456', NULL, NULL, '', NULL, '1571552758554');
INSERT INTO `cz_user` VALUES (45, '123456', '12345678', NULL, '/img/28.jpg', '', NULL, '1571556965812');
INSERT INTO `cz_user` VALUES (46, '小杜杜2312312', '123456', NULL, '/img/28.jpg', '', NULL, '1571557479535');
INSERT INTO `cz_user` VALUES (47, '小杜杜123456', '123456', NULL, '/img/28.jpg', '', NULL, '1571557512958');
INSERT INTO `cz_user` VALUES (48, 'admin1', '123456', NULL, '/img/9b9e611ba6d744bc8c602d4d49fe785d.jpeg', '', NULL, '1571566053621');
INSERT INTO `cz_user` VALUES (49, 'admin321', '123321', NULL, '/img/9b9e611ba6d744bc8c602d4d49fe785d.jpeg', '', NULL, '1571621414346');

-- ----------------------------
-- Table structure for leftmun
-- ----------------------------
DROP TABLE IF EXISTS `leftmun`;
CREATE TABLE `leftmun`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mun_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '一级菜单',
  `numes_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '二级菜单',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of leftmun
-- ----------------------------
INSERT INTO `leftmun` VALUES (1, '用户管理', '用户', '/users');
INSERT INTO `leftmun` VALUES (2, '会员查询', '会员', '/hui');
INSERT INTO `leftmun` VALUES (3, '历史记录', '历史', '/history');
INSERT INTO `leftmun` VALUES (4, '超级用户', '管理员', '/admin');
INSERT INTO `leftmun` VALUES (5, '影视管理', '影视', '/film');
INSERT INTO `leftmun` VALUES (6, '订单管理', '订单', '/ding');
INSERT INTO `leftmun` VALUES (7, '评论记录', '评论', '/ping');

SET FOREIGN_KEY_CHECKS = 1;
