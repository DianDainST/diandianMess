const mysql = require("mysql");

const connection = mysql.createConnection({
  useConnectionPooling: true,
  port: "3306",
  host: "39.106.158.169",
  user: "root",
  password: "654321",
  database: "diandian",
  // 让sql语句进行拼接
  multipleStatements: true
  // 对Cannot enqueue Query after fatal error 这个问题进行排除
});

module.exports = connection;
