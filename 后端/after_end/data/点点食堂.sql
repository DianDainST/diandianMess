create database diandian

use diandian

-- 0.1 用户基本信息表 
create table users (
    id int(11) primary key auto_increment not null comment '用户基本信息表的id',
    user_name varchar(20) not null comment ' 用户名',
    pwd varchar(50) not null comment '密码',
    nick_name varchar(20) comment '昵称',
    phone_num varchar(12) comment '手机号',
    class varchar(15) comment '班级',
    idcard varchar(20) comment '身份证号',
    sushe_num int(11) comment '宿舍号',
    come_from varchar(50) comment '籍贯',
    wechat_id varchar(30) comment '微信号',
    favorite varchar(50) comment '爱好',
    gender enum("男","女") not null comment '性别',
    birthday date not null comment '生日',
    last_point int(11) not null comment '剩余积分',
    total_point int(11) not null comment '总积分',
    header_img varchar(255) not null comment '头像',
    money decimal(11,2) not null comment '钱'
);

-- insert into users values(null,'王海龙','123456','海龙','13778424516','全栈12','511123200008267015',321,'四川','13778424516','游泳','男','2000-8-26',1000,1000,'https://wanghailongwang.oss-cn-beijing.aliyuncs.com/1559565909404.png','1000.1');

-- 0.2 管理员基本信息表 grantee
create table grantee ( 
    id int(11) primary key auto_increment not null comment '管理员基本信息表的id',
    user_name varchar(20) not null comment '用户名',
    pwd varchar(32) not null comment '密码',
    part varchar(4) not null comment '模块',
    win_id tinyint(4) not null comment '权限：0：全部权限，-1为没有权限'
)

-- 0.3 餐品基本信息表 list
create table list (
    id int(11) primary key auto_increment not null comment '餐品',
    name varchar(10) not null comment '菜名',
    price int(11) not null comment '价格',
    img varchar(255) comment '图片',
    intro varchar(300) comment '简介',
    unit varchar(10) not null comment '计量单位',
    win_id int(11) not null comment '所属窗口 窗口id 外键-餐品基本信息表'
)
-- insert into list values(null,'红烧狮子头','10','https://wanghailongwang.oss-cn-beijing.aliyuncs.com/%E4%B8%8B%E8%BD%BD.jpg','这是一道非常美味的菜肴',"2个","1");

-- 0.4 订单基本信息表 orders
create table orders (
    id int(11) primary key auto_increment not null comment '订单表的id',
    users_id int(11) not null comment '用户的id 外键-用户表',
    meal_time_id int(11) not null comment '用餐时间 外键-用餐时间表',
    people_num int(11) not null comment '用餐人数',
    orders_state tinyint(4) not null comment '0:未支付，1：支付未领取，2：领取未评价 3：已评价',
    coupons_id int(11) comment '优惠卷id 外键-优惠卷表',
    coupons_cut_money int(11) comment '优惠金额 外键-优惠卷表',
    express_fee int(11) comment '快递费',
    total_price int(11) not null comment '总价格',
    add_time datetime not null comment '下单时间',
    qr_code varchar(32) comment '二维码信息',
    get_time datetime not null comment '取餐时间',
    box_fee tinyint(4) not null comment '0:食堂  大于0:打包'
)

--  ---------------------------------------------------------------------------------

-- 0.5 用户评价信息表 evaluate
create table evaluate ( 
    id int(11) primary key auto_increment not null comment '用户评价信息表的id',
    list_id int(11) not null comment '菜品的id 外键-餐品基本信息表',
    content varchar(300) comment '评价内容',
    orders_id int(11) not null comment '订单id 外键-订单表',
    add_time datetime not null comment '评价时间'
)


-- 0.6 优惠卷信息表 coupons
create table coupons (
    id int(11) primary key auto_increment not null comment '优惠卷表的id',
    coupons_name varchar(20) not null comment '优惠卷名称',
    cut_money tinyint(4) not null comment '面额',
    time_limit tinyint(4) not null comment '期限',
    type tinyint(4) not null comment '优惠券类型 0:可重复领取 1:不可重复领取',
    take_state tinyint(4) not null comment '0：可领取 1：不可领取',
    point_use int(11) comment '兑换积分'
)

-- 0.7 菜单表 menu
create table menu (
    id int(11) primary key auto_increment not null comment '菜单表的id',
    meal_time_id int(11) not null comment '用餐时间id 外键-用餐时间表',
    list_id int(11) not null comment '菜品的id 外键-餐品基本信息表',
    count int(11) not null comment '预售数量：大于等于0：有预售，等于0为售罄；-1：不限预售数量',
    off_price int(11) not null comment '促销价 0为没有促销价，以原价上架'
)

-- 0.8 餐馆公告信息表 gonggao
create table gonggao (
    id int(11) primary key auto_increment not null comment '餐馆广告表的id',
    title varchar(20) not null comment '标题',
    add_user varchar(20) not null comment '发布人',
    add_time datetime not null comment '发布时间',
    last_update_user varchar(20) comment '修改人',
    last_update_time datetime comment '修改时间',
    content varchar(1000) not null comment '公告详情',
    state tinyint(4) not null comment '0：隐藏，1：显示'
) 
 
-- 0.9 订单详情表 order_detail
create table order_detail (
    id int(11) primary key auto_increment not null comment '订单详情表id',
    orders_id int(11) not null comment '订单id',
    list_id int(11) not null comment '菜品的id 外键-餐品基本信息表',
    list_name varchar(20) not null comment '菜品名称 外键-餐品基本信息表',
    list_price int(11) not null comment '金额 外键-餐品基本信息表',
    sum int(11) not null comment '菜品份数',
    win_id int(11) not null comment '窗口id 外键- ',
    state tinyint(4) not null comment '状态：0：未领取；1：已领取'
)

-- 1.0 用户优惠券·表 coupons_detail
create table coupons_detail (
    id int(11) primary key auto_increment not null comment '用户优惠卷标id',
    users_id int(11) not null comment '用户id 外键-用户基本信息表',
    last_use_time datetime not null comment '到期时间',
    coupons_state tinyint(4) not null comment '优惠券状态 0:可用 1：已使用 2：已过期  外键-优惠卷信息表',
    coupons_id int(11) not null comment '优惠卷id 外键-优惠卷信息表'
)


-- 1.1 用餐时间表 meal_time
create table meal_time (
    id int(11) primary key auto_increment not null comment '用餐时间表的id',
    meal_name varchar(20) not null comment '用餐时间名称',
    meal_start datetime not null comment '用餐开始时间',
    meal_end datetime not null comment '用餐结束时间',
    pay_end datetime comment '最晚付款时间'
)

-- 1.2 投票表 vote 
create table vote (
    id int(11) primary key auto_increment not null comment '投票表的id',
    users_id int(11) not null comment '发起用户id 外键-用户表',
    title varchar(20) not null comment '发起投票标题',
    name varchar(20) not null comment '菜品名称 外键-餐品基本信息表',
    img varchar (255) not null comment '菜品图片 外键-餐品基本信息表',
    sum int(11) not null comment '投票数',
    add_time datetime not null comment '发起时间',
    end_time datetime not null comment '结束时间',
    state tinyint(4) not null comment '0:未采纳;1:已采纳'
)

-- 1.3 投票详情表 vote_detail
create table vote_detail (
    id int(11) primary key auto_increment not null comment '投票详情表的id',
    vote_id int(11) not null comment '投票id 外键-用户表',
    users_id int(11) not null comment '踢票id 外键-用户表', 
)

-- 1.4 留言表 message
create table message (
    id int(11) primary key auto_increment not null comment '留言表的id',
    users_id int(11) not null comment '留言用户的id 外键-用户表',
    content varchar(255) not null comment '留言内容',
    add_time datetime not null comment '留言时间',
    reply varchar(255) comment '回复内容',
    reply_time datetime comment '回复时间',
    grantee_id int(11) comment '回复人'
)

-- 1.5 窗口权限管理表 win
create table win (
    id int(11) primary key auto_increment not null comment '窗口权限表的id',
    win_name varchar(20) not null comment '窗口名'
)

-- 1.6 窗口业绩表 win_count 
create table win_count (
    id int(11) primary key auto_increment not null comment '窗口业绩表的id',
    win_id int(11) not null comment '窗口id 外键-餐品基本信息表',
    win_date date not null comment '业绩时间',
    win_sum int(11) not null comment '窗口销售额'
)

-- 1.7 预付优惠表 
create table depay (
    id int(11) primary key auto_increment not null comment '预付优惠表',
    pay_money int(11) not null comment '充值金额',
    song_money int(11) not null comment '赠送金额'
)