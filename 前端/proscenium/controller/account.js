const conn = require('../data/index.js')

//渲染付款页面
module.exports.payment = (req, res) => {
    let id = req.params.id;
    let payment_amount = req.query.money;
    conn.query("select money from users where id = ? ", id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "获取成功",
            date: data,
            payment_amount: payment_amount

        })
    })
}

// 支付成功
module.exports.pay = (req, res) => {
    let id = req.params.id;
    let money = req.body.pay;
    let arr = req.body.arr1;
    // console.log(arr);
    let arr1 = JSON.parse(arr)
    // console.log(arr1[0])
    conn.query(`update users set money = ? where id = ?`, [money, id], (err1, data1) => {
        if (err1) {
            res.json({
                code: "400",
                msg: '修改金额错误',
                err: err1
            })
            return console.log(err1);
        }
        conn.query(`update orders set orders_state = 1 where id= ?`, [arr1[0]], (err2, data2) => {
            if (err2) {
                res.json({
                    code: "400",
                    msg: '修改订单状态错误',
                    err: err2
                })
                return console.log(err2);
            }
            res.json({
                code: "200",
                msg: "支付成功",
            })

        })
    })
}