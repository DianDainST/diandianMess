const conn = require('../data');

module.exports.personage = (req, res) => {
  res.render('personage');
}

// 用户名与头像
module.exports.personage1 = (req, res) => {
  let id = req.query.id || 1;
  // console.log(id);
  conn.query('select * from users where id =?', id, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.json(results);
  })
}

// 优惠券数量
module.exports.personage2 = (req, res) => {
  let id = req.query.id || 1;
  // console.log(id);
  conn.query('select count(*) as num from coupons_detail where users_id =?', id, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.json(results);
  })
}


// 账户余额

module.exports.personage3 = (req, res) => {
  let id = req.query.id || 1;
  // console.log(id);
  conn.query('select money from users where id =?', id, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.json(results);
  })
}

// 喜欢的菜品

module.exports.personage4 = (req, res) => {
  let id = req.query.id;
  // console.log(id);
  conn.query('select * from list limit 0,8', id, (error, results) => {
    if (error) {
      console.log(error);
    }
    res.json(results);
  })
}


// 火热菜品

module.exports.personage5 = (req, res) => {
  let id = req.query.id;
  // console.log(id);

  conn.query('select c.* ,d.content,count(*) from orders a , order_detail b,list c ,evaluate d where a.id = b.orders_id And c.id = b.list_id And d.list_id = c.id Group by b.list_name order by count(*) desc limit 2', (error, results) => {
    if (error) {
      console.log(error);
    }
    res.json(results);
  })
}

// 充值
module.exports.personage6 = (req, res) => {
  let id = req.body.id;
  conn.query('select * from users where id = ?', id, (error, data) => {
    if (error) {
      res.json({
        code: "400",
        msg: "失败"
      })
      return console.log(error);
    }
    let oldMoney = data[0].money-0;
    let newMoney = oldMoney + (req.body.addmoney-0);
    let params = {
      money: newMoney,
      id: req.body.id
    }

    conn.query('update users set money = ? where id =?',[newMoney,req.body.id], (error2, results2) => {
      if (error2) {
        res.json({
          code:"400",
          msg:"添加金额失败",
          error:error2
        })
        return console.log(error2);
      }
      res.json({
        code:200,
        msg:"添加金额成功",
        data:results2
      })
     
    })
  })

}

