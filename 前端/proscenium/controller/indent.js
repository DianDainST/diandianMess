const conn = require('../data/index.js')

// 渲染订单页面
module.exports.showIndent = (req, res) => {
    let id = req.params.id;
    conn.query('select a.id orderID,a.*,b.*,c.img from orders a, order_detail b, list c where a.users_id = ? and a.orders_state = 0 and a.id = b.orders_id and b.list_id = c.id ;', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "成功",
            data: data,
        })
        // console.log(data);
    })
}

// 跳转结算
module.exports.upIndent = (req, res) => {
    let id = req.query.id;
    // let money = req.query.money;
    // global.payment_amount = req.query.money
    res.render('account')
}


// 渲染支付未领取
module.exports.showUnpaid = (req, res) => {
    let id = req.params.id;
    conn.query('select a.*,b.*,c.img from orders a, order_detail b, list c where a.users_id = ? and a.orders_state = 1  and a.id = b.orders_id and b.list_id = c.id ;', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "获取未领取成功",
            data
        })
    })
}

// 点击确认领取
module.exports.putUnpaid = (req, res) => {
    // console.log(1)
    let id = req.params.id;
    let cateId = req.body.cateId;
    conn.query('update orders set orders_state = 2 where users_id = ?', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "领取成功"
        })
    })
}




// 渲染完成未评价
module.exports.getEvaluation = (req, res) => {
    let id = req.params.id;
    conn.query('select a.*,b.*,c.img from orders a, order_detail b, list c where a.users_id = ?  and a.orders_state = 2  and a.id = b.orders_id and b.list_id = c.id ;', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "获取未评价成功",
            data
        })
    })
}

// 点击评论
module.exports.putEvaluation = (req, res) => {
    let id = req.params.id;
    let paramse = {
        list_id: req.body.list_id,
        content: req.body.name,
        orders_id: req.body.ordersID,
        add_time: new Date(),
    }
    conn.query('update orders set orders_state = 3 where users_id = ?', id, (error1, data1) => {
        if (error1) {
            res.json({
                code: "400",
                msg: "点击评论失败",
                err: error1
            })
            return console.log(error1)
        }
        conn.query(`insert into evaluate set ? `, paramse, (error2, data) => {
            if (error2) {
                res.json({
                    code: "400",
                    msg: "点击评论失败",
                    err: error2
                })
                return console.log(error2)
            }
            res.json({
                code: "200",
                msg: "谢谢您,评论成功"
            })
        })

    })
}

// 全部完成
module.exports.accomplish = (req, res) => {
    let id = req.params.id;
    conn.query('select a.*,b.*,c.img from orders a, order_detail b, list c where a.users_id = ?  and a.orders_state = 3  and a.id = b.orders_id and b.list_id = c.id ;', id, (error, data) => {
        if (error) return console.log(error)
        res.json({
            code: "200",
            msg: "获取完成成功",
            data
        })
    })
}
