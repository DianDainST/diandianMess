const conn = require('../data');

module.exports.discount = (req, res) => {
    res.render('discount');
}


module.exports.discount1 = (req, res) => {
    let id = req.query.id || 1;
    // console.log(id);
    conn.query('select * from coupons where id in (select coupons_id from coupons_detail where users_id = ?)', id, (error, results) => {
        if (error) {
            console.log(error);
        }
        res.json(results);
    })
}


module.exports.addDiscount = (req, res) => {
    // let id = req.query.id || 1;
    // console.log(id);
    data = {
        users_id: req.body.users_id,
        last_use_time: req.body.last_use_time,
        coupons_state: req.body.coupons_state,
        coupons_id: req.body.coupons_id,
    }
    sql = 'insert into coupons_detail set ?'
    console.log(sql,data)
    conn.query(sql, data, (error, results) => {
        if (error) return console.log(error);

        res.json({
            code: "200",
            msg: "添加成功"
        });
    })
}

