const express = require("express");

const app = express();

// 配置body-parser

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));

const config = require("./config");

// 静态托管
app.use(express.static('./public'))

//#region 路由
// 菜单
const menu = require("./router/menu.js");
app.use(menu);
// 登录
const login = require("./router/login.js");
app.use(login);
// orders 订单的产生
const orders = require('./router/orders.js');
app.use(orders);
// 首页
const index = require("./router/index.js");
app.use(index);
// healthDetails
const healthDetails =require("./router/healthDetails.js");
app.use(healthDetails);
// activity 
const activity = require("./router/activity.js");
app.use(activity);
//#endregion


// personage 路由
const personage = require("./router/personage.js");
app.use(personage);


// discount 路由
const discount = require("./router/discount.js");
app.use(discount)


// 订单页面
const indent = require("./router/indent");
app.use(indent)

// 付款页面
const account = require("./router/account")
app.use(account)



//#region 3.配置ejs模板引擎
// 3.1 下载ejs包
// 3.2 设置模板引擎的后缀是什么
app.set('view engine', 'ejs');
// 3.3 设置模板引擎的模板在哪个目录
app.set('views', __dirname + '/views');
//#endregion


// 监听窗口
app.listen(config.port, () => {
    console.log('http://127.0.0.1:8090');
})