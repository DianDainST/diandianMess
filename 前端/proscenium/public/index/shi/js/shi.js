// 需引入jquery

// 2.0
// 给元素添加具体样式
function $style($class, css, zhi) {
  $("[class*=" + $class + "]").each(function () {
    if ($(this).attr("style") != undefined) {
      arr = $(this).attr("style").split(";")
      if (arr.indexOf(css + ":" + zhi + "px") == -1){
        $(this).attr("style", $(this).attr("style") + css + ":" + zhi + "px;");
      }

    }
    if ($(this).attr("style") == undefined) {
      $(this).attr("style", css + ":" + zhi + "px;");
    }
  });
}
// 设置快捷样式
// 例：$css("wi", "width") "wi"是可以自定义的快捷样式，"width"是快捷样式完整的意思。如想自行修改，按格式接着写即可，切记:快捷样式名为两位字符串。
$Element = $("[class]").each(function () {
  $class = $.trim($(this).attr("class")).split(" ");
  function $css($qz, css) {
    if (qz == $qz) {
      $style($class[i], css, zhi);
    }
  }
  for (i = 0; i < $class.length; i++) {
    if ($class[i] != "") {
      qz = $class[i].substr(0, 2);
      zhi = $class[i].substr(2, $class[i].length - 2);
      $css("wi", "width");
      $css("hi", "height");
      $css("lh", "line-height");
      $css("fs", "font-size");
      $css("pt", "padding-top");
      $css("pr", "padding-right");
      $css("pb", "padding-bottom");
      $css("pl", "padding-left");
      $css("mt", "margin-top");
      $css("mr", "margin-right");
      $css("mb", "margin-bottom");
      $css("ml", "margin-left");
    }
  }
});

// 随机洗牌函数
function random(arr) {
  let i = arr.length;
  while (i) {
    let j = Math.floor(Math.random() * i--); //5555
    [arr[j], arr[i]] = [arr[i], arr[j]];
  }
}

// 元素移动到一定位置触发动画
function scrollnumber(element, cssname, offset, time) {
  var a, b, c, d;
  // console.log($(element).offset(), cssname, offset, time)
  d = $(element).offset().top;
  a = eval(d + offset);
  b = $(window).scrollTop();
  c = $(window).height();
  if (b + c > a) {
    setTimeout(function() {
      $(element).addClass(cssname);
    }, time);
  }
  // console.log(d,a,b,c);
}

// 鼠标滚动触发事件
function roll(element,dist) {
  var scrollTop = $(window).scrollTop(); //页面滚动的距离
  console.log($(window))
  if (dist <= scrollTop) {
      $(element).addClass("aos-animate");
  } 
  if (dist >= scrollTop) {
      $(element).removeClass("aos-animate")
  }
}

// $(function () {
//   $(window).scroll(roll(".fade-down"));
// });