const express = require('express');

const router = express.Router();

const CtrlCategories = require('../controllers/personageCon');

router.get('/personage', CtrlCategories.personage);

module.exports = router;
