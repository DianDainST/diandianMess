// 需引入jquery

// 给元素添加具体样式
function $style($class, css, zhi) {
  $("[class*=" + $class + "]").each(function() {
    if ($(this).attr("style") != undefined) {
      $(this).attr("style", $(this).attr("style") + css + ":" + zhi + "px;");
    } else {
      $(this).attr("style", css + ":" + zhi + "px;");
    }
  });
}
// 设置快捷样式
// 例：$css("wi", "width") "wi"是可以自定义的快捷样式，"width"是快捷样式完整的意思。如想自行修改，按格式接着写即可，切记:快捷样式名为两位字符串。
$Element = $("[class]").each(function() {
  $class = $.trim($(this).attr("class")).split(" ");
  function $css($qz, css) {
    if (qz == $qz) {
      $style($class[i], css, zhi);
    }
  }
  for (i = 0; i < $class.length; i++) {
    if ($class[i] != "") {
      qz = $class[i].substr(0, 2);
      zhi = $class[i].substr(2, $class[i].length - 2);
      $css("wi", "width");
      $css("hi", "height");
      $css("lh", "line-height");
      $css("fs", "font-size");
      $css("pt", "padding-top");
      $css("pr", "padding-right");
      $css("pb", "padding-bottom");
      $css("pl", "padding-left");
      $css("mt", "margin-top");
      $css("mr", "margin-right");
      $css("mb", "margin-bottom");
      $css("ml", "margin-left");
      $css("tp", "top");
      $css("rt", "right");
      $css("bm", "bottom");
      $css("lt", "left");
    }
  }
});

// 随机洗牌函数
function random(arr) {
  let i = arr.length;
  while (i) {
    let j = Math.floor(Math.random() * i--); //5555
    [arr[j], arr[i]] = [arr[i], arr[j]];
  }
}
