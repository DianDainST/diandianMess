setTimeout(() => {

    $(".sum").each(function () {
        const sumText = $(this).prev().prev().text()
        $(this).text(sumText)

    })


    $('#tab-menu>li').click(function () {
        $(this).addClass('active').siblings().removeClass('active')
        //对应索引的下标
        $('.indent_main').eq($(this).index()).addClass('selected').siblings().removeClass('selected')
    })

    $("button.jia").on("click", function () {
        num = $(this).siblings("input").val(); //字符串数量
        num = Number(num) + 1; //数字数量
        $(this).siblings("input").val(num); //给input赋值
        price = $(this).parent().parent("li").prev().text() //带￥符号的单价
        price = Number(price.substring(1, price.length)) //不带￥符号的单价
        prices = num * price; //数量*单价=总价
        $(this).parent().parent("li").next().text("￥" + prices + ".00") //给总价赋值
        // console.log(prices);
        count()
    });
    $("button.jian").on("click", function () {
        num = $(this).siblings("input").val();
        if (num > 1) {
            num = Number(num) - 1;
        } else {
            num = 1;
        }
        $(this).siblings("input").val(num);
        price = $(this).parent().parent("li").prev().text() //带￥符号的单价
        price = Number(price.substring(1, price.length)) //不带￥符号的单价
        prices = num * price; //数量*单价=总价
        $(this).parent().parent("li").next().text("￥" + prices + ".00") //给总价赋值
        // console.log(prices);
        count()
    });

    // 订单
    function count() {
        let sum = 0,
            moneys = 0;
        $(".Item1").each(function () {
            if ($(this).prop('checked') == true) {
                num = Number($(this).parent().next().next().next().children("div").children("input").val())
                sum += num;
                money = $(this).parent().next().next().next().next().text()
                money = Number(money.substring(1, money.length))
                moneys += money;
            }
        });
        $(".selected").children("em").text(sum)
        $(".indent_floor_right_moneys").children("em").text("￥" + moneys + ".00")
        if (moneys > 0) {
            $(".indent_floor_right_close a").css("backgroundColor", "orangered")
        } else {
            $(".indent_floor_right_close a").css("backgroundColor", "rgb(176, 176, 176)")
        }
    }
    $(".qxan1").on("click", function () {
        flag = $(this).prop('checked')
        $(".qxan1").prop('checked', flag)
        $(".Item1").prop('checked', flag)
        $(".Item1").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
        });
        count()
    })

    $(".Item1").on("click", function () {
        flag = true;
        $(".Item1").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
            if ($(this).prop('checked') == false) {
                flag = false;
            }
        });
        if (flag == false) {
            $(".qxan1").prop('checked', false)
        } else {
            $(".qxan1").prop('checked', true)
        }
        count()
    })


    // 支付未领取
    $(".qxan2").on("click", function () {
        flag = $(this).prop('checked')
        $(".qxan2").prop('checked', flag)
        $(".Item2").prop('checked', flag)
        $(".Item2").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
        });
        count()
    })

    $(".Item2").on("click", function () {
        flag = true;
        $(".Item2").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
            if ($(this).prop('checked') == false) {
                flag = false;
            }
        });
        if (flag == false) {
            $(".qxan2").prop('checked', false)
        } else {
            $(".qxan2").prop('checked', true)
        }
        count()
    })
    // 完成未评价
    $(".qxan3").on("click", function () {
        flag = $(this).prop('checked')
        $(".qxan3").prop('checked', flag)
        $(".Item3").prop('checked', flag)
        $(".Item3").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
        });
        count()
    })

    $(".Item3").on("click", function () {
        flag = true;
        $(".Item3").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).parent().parent().css('backgroundColor', "#fff8e1");
            } else {
                $(this).parent().parent().css('backgroundColor', "#fcfcfc");
            }
            if ($(this).prop('checked') == false) {
                flag = false;
            }
        });
        if (flag == false) {
            $(".qxan3").prop('checked', false)
        } else {
            $(".qxan3").prop('checked', true)
        }
        count()
    })

    // 
    // 需引入jquery

    // 给元素添加具体样式
    function $style($class, css, zhi) {
        $("[class*=" + $class + "]").each(function () {
            if ($(this).attr("style") != undefined) {
                $(this).attr("style", $(this).attr("style") + css + ":" + zhi + "px;");
            } else {
                $(this).attr("style", css + ":" + zhi + "px;");
            }
        });
    }
    // 设置快捷样式
    // 例：$css("wi", "width") "wi"是可以自定义的快捷样式，"width"是快捷样式完整的意思。如想自行修改，按格式接着写即可，切记:快捷样式名为两位字符串。
    $Element = $("[class]").each(function () {
        $class = $.trim($(this).attr("class")).split(" ");

        function $css($qz, css) {
            if (qz == $qz) {
                $style($class[i], css, zhi);
            }
        }
        for (i = 0; i < $class.length; i++) {
            if ($class[i] != "") {
                qz = $class[i].substr(0, 2);
                zhi = $class[i].substr(2, $class[i].length - 2);
                $css("wi", "width");
                $css("hi", "height");
                $css("lh", "line-height");
                $css("fs", "font-size");
                $css("pt", "padding-top");
                $css("pr", "padding-right");
                $css("pb", "padding-bottom");
                $css("pl", "padding-left");
                $css("mt", "margin-top");
                $css("mr", "margin-right");
                $css("mb", "margin-bottom");
                $css("ml", "margin-left");
            }
        }
    });

    // 随机洗牌函数
    function random(arr) {
        let i = arr.length;
        while (i) {
            let j = Math.floor(Math.random() * i--); //5555
            [arr[j], arr[i]] = [arr[i], arr[j]];
        }
    }
    // 金额
    let unit = $(".unit")
    let amount = $(".amount")
    $(".unit").each((index) => {
        // console.log(unit[index].innerText,amount[index].innerText);
        let aggregate = unit[index].innerText * amount[index].innerText;
        // console.log(aggregate);
        $(".amount_money")[index].innerText = aggregate;
    })
}, 500);

function time() {
    // let unit = $(".unit").text()
    // let amount = $(".amount").text()
    // let aggregate = unit * amount;
    // console.log(unit, amount)
    // $(".amount_money").text(aggregate)
}
setInterval('time()', 5000)
// 请求订单列表
let showIndent = function () {
    let id = window.sessionStorage.getItem("stid");
    // console.log(id);
    $.ajax({
        type: 'get',
        url: '/indent/' + id,
        data: '',
        success: function (data) {
            // console.log(data);
            if (data.code !== "200") {
                alert("网络错误")
            } else {
                let html = template("indent", {
                    list: data.data
                })
                $('.rabbet').html(html)
            }
        }
    })
}
showIndent()


// 结算并跳转结算页面
$('#close').click(function () {
    let id = window.sessionStorage.getItem("stid");
    var num = $('.total').text();
    var num1 = num.match(/\d+(.\d+)?/g);
    var money = num1[0]
    var arr = [];
    $(".Item1[name='checkbox']:checked").each(function (i) {
        arr.push($(this).val());
    });
    let obj = { arr, money }
    if (arr.length == 0) {
        return alert("你没有选择要付款的菜品")
    } else {
        $.ajax({
            type: 'get',
            url: '/indents/account',
            data: {
                id,
                money
            },
            success: function () {
                window.sessionStorage.setItem("obj", JSON.stringify(obj))
                location.href = "/indents/account"
            }
        })
    }
})

// 渲染支付未领取
let showUnpaid = function () {
    let id = window.sessionStorage.getItem("stid");
    $.ajax({
        type: 'get',
        url: '/indents/unpaid/' + id,
        data: '',
        success: function (data) {
            // console.log(data);
            if (data.code !== '200') {
                return alert("网络错误")
            } else {
                let html = template('unpaid', {
                    list: data.data
                })
                $('.unpaid').html(html)

            }
        },
        complete() {
            // 金额

        }
    })
}
showUnpaid()

// 确认领取
$(document).on('click', '.Determined', function () {
    let id = window.sessionStorage.getItem("stid");
    let cateId = $(this).data('id');
    $.ajax({
        type: 'put',
        url: '/indents/unpaid/' + id,
        data: {
            cateId
        },
        success: function (data) {
            console.log(data);
            if (data.code !== '200') {
                return alert("网络错误")
            } else {
                showUnpaid()
                evaluation()
            }
        }
    })
});

// 渲染完成未评价
let evaluation = function () {
    let id = window.sessionStorage.getItem("stid");
    $.ajax({
        type: 'get',
        url: '/indents/evaluation/' + id,
        data: '',
        success: function (data) {
            if (data.code !== '200') {
                return alert("网络错误")
            } else {
                let html = template('evaluation', {
                    list: data.data
                })
                $('.evaluation').html(html)
            }
        }
    })
}
evaluation()

// 点击评论
$(document).on('click', '.comment_on', function () {
    let list_id = $(this).data('id');
    // console.log(list_id);
    let ordersID = JSON.parse(window.sessionStorage.getItem("obj")).arr[0];
    // console.log(ordersID);
    var name = prompt("请输入您的评论,此评论包括所有菜品的评论", "");
    let id = window.sessionStorage.getItem("stid");
    if (!name) {
        return alert("请输入评论")
    } else {
        $.ajax({
            type: 'put',
            url: '/indents/evaluation/' + id,
            data: { list_id, name, ordersID },
            success: function (data) {
                if (data.code !== '200') {
                    return alert("网络错误")
                } else {
                    evaluation()
                    accomplish()
                }
            }
        })
    }
});

// 渲染完成
let accomplish = function () {
    let id = window.sessionStorage.getItem("stid");
    $.ajax({
        type: 'get',
        url: '/indents/accomplish/' + id,
        data: '',
        success: function (data) {
            if (data.code !== '200') {
                return alert("网络错误")
            } else {
                // console.log(data)
                let html = template('accomplish', {
                    list: data.data
                })
                $('.accomplish').html(html)
            }
        }
    })
}
accomplish()