let show = function () {
    let id = window.sessionStorage.getItem("stid");
    let obj = JSON.parse(window.sessionStorage.getItem("obj"));
    let money = obj.money;
    $.ajax({
        type: 'get',
        url: '/indents/account/' + id,
        data: {
            money
        },
        success: function (data) {
            if (data.code !== '200') {
                return alert("请返回订单页重新操作")
            } else {
                let html = template('account', data)
                $('.system-message').html(html)
            }
        }
    })
}
show()

// 支付
$(document).on('click', '#test', function () {
    let id = window.sessionStorage.getItem("stid");
    let obj = JSON.parse(window.sessionStorage.getItem("obj"));
    let arr = obj.arr;
    // console.log(arr)
    let arr1 = JSON.stringify(arr)

    let money = $('#money').text()
    let payment_amount = $("#payment_amount").text()
    let pay = money - payment_amount;
    if (pay < 0) {
        return alert("账户余额不足")
    } else {
        $.ajax({
            type: 'put',
            url: '/indents/account/' + id,
            data: {
                pay,
                arr1
            },
            success: function (data) {
                if (data.code !== '200') {
                    return alert("支付失败")
                } else {
                    alert('支付成功');
                    location.href = "/indent"
                }
            }
        })
    }
});

// let obj = JSON.parse(window.sessionStorage.getItem("obj"));
// console.log(obj)