const express = require('express');

const router = express.Router();

const conn = require("../controller/account.js")

// 渲染付款页面
router.get('/indents/account/:id', conn.payment)

// 确认付款
router.put("/indents/account/:id",conn.pay)

module.exports = router