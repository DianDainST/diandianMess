const express = require('express');

const router = express.Router();

// 渲染activity1页面
router.get("/activity1",function(req,res){
    return res.render('activity1');
})

// 渲染activity2页面
router.get("/activity2",function(req,res){
    return res.render('activity2');
})


// 渲染activity3页面
router.get("/activity3",function(req,res){
    return res.render('activity3');
})

// 渲染activity4页面
router.get("/activity4",function(req,res){
    return res.render('activity4');
})

module.exports = router;