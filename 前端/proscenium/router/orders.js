const express = require('express');

const router = express.Router();

// 渲染template menu页面
router.get("/order", function (req, res) {
    return res.render('order');
})

const mysql = require("mysql");

let conn = mysql.createConnection({
    useConnectionPooling: true,
    port: "3306",
    host: "39.106.158.169",
    user: "root",
    password: "123456",
    database: "diandian",
    // 让sql语句进行拼接
    multipleStatements: true
    // 对Cannot enqueue Query after fatal error 这个问题进行排除
})
// 渲染订单页面
router.get("/")


// 添加 orders 数据
router.post("/api/v1/orders", function (req, res) {
    // 对id的判断
    if (req.body.users_id === undefined) {
        res.json({
            code: "400",
            msg: "请登录"
        })
        return
    }
    else if (req.body.total_price === undefined) {
        res.json({
            code: "400",
            msg: "请输入总价格"
        })
        return
    }
    // 
    //  获取到 用餐时间的id
    conn.query("select id from meal_time where now() > meal_start AND now() < meal_end", (error1, data1) => {
        if (error1) {
            res.json({
                code: "400",
                msg: "亲抱歉,获取到 用餐时间的id失败",
                error: error1
            })
            return console.log(error1)
        }
        else if (data1.length == 0) {
            return res.json({
                code: "400",
                msg: "亲抱歉,当前时间还没有用餐服务",
            })
        }
        let meal_time_id = data1[0].id;
        // 
        let paramse = {
            users_id: req.body.users_id,
            meal_time_id: meal_time_id,
            people_num: req.body.people_num || 1,
            orders_state: req.body.orders_state || 0,
            coupons_id: req.body.coupons_id || 0,
            coupons_cut_money: req.body.coupons_cut_money || "0",
            express_fee: req.body.express_fee || 0,
            total_price: req.body.total_price,
            add_time: req.body.add_time || new Date(),
            qr_code: req.body.qr_code || "无",
            get_time: req.body.get_time ||  new Date(),
            box_fee: req.body.box_fee || 0
        }
        // sql语句
        let sql = 'insert into orders set ?'

        conn.query(sql, paramse, (error, data) => {
            if (error) {
                res.json({
                    code: "400",
                    msg: "添加订单失败",

                    error: error
                })
                return console.log(error);
            } else {
                res.json({
                    code: "200",
                    msg: "添加订单成功",
                    data: data
                })
            }
        })
    })

})

// 添加 order_detail 数据
router.post("/api/v1/order_detail",function(req,res) {
    let paramse = {
        orders_id : req.body.orders_id,
        list_id : req.body.list_id,
        list_name : req.body.list_name,
        list_price : req.body.list_price,
        sum : req.body.sum || 1,
        win_id : req.body.win_id ,
        state : req.body.state ||0
    }
    conn.query("insert into order_detail set ?",[paramse],(error,data)=>{
        if(error){
            res.json({
                code:"400",
                msg:"添加菜单详情数据出现错误"
            })
            return console.log(error);
        }
        res.json({
            code:"200",
            msg:"添加菜单详情数据成功"
        })
    })
})

module.exports = router;