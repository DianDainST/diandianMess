const express = require('express');

const router = express.Router();

// 渲染template menu页面
router.get("/menu", function (req, res) {
    return res.render('menu');
})

const mysql = require("mysql");

let conn = mysql.createConnection({
    useConnectionPooling: true,
    port: "3306",
    host: "39.106.158.169",
    user: "root",
    password: "123456",
    database: "diandian",
    // 让sql语句进行拼接
    multipleStatements: true
    // 对Cannot enqueue Query after fatal error 这个问题进行排除
})

// 获取到 menu 数据
router.get("/api/v1/list", function (req, res) {
    let pagenum = req.query.pagenum || 1;
    let pagesize = req.query.pagesize || 50;
    let indexStart = (pagenum - 1) * pagesize;
    let win_id = req.query.win_id
    if (win_id = req.query.win_id) {
        sql = `select * from list where win_id = ${win_id}  limit ${indexStart},${pagesize}`
    }
    else {
        sql = `select * from list limit ${indexStart},${pagesize}`
    }
    conn.query(sql, (error, data) => {
        if (error) {
            res.json({
                code: "200",
                msg: "获取菜单数据失败",
                err: error
            })
            return console.log(error)
        }
        res.json({
            code: "400",
            msg: "获取成功",
            data: data
        })
    });
})

module.exports = router;