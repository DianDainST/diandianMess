const express = require('express');

const router = express.Router();

const CtrlCategories = require('../controller/discount');

// 显示个人个人中心优惠券
router.get('/discount', CtrlCategories.discount);


// 优惠券具体显示
router.get('/discount1', CtrlCategories.discount1);

router.post('/discount1', CtrlCategories.addDiscount);

module.exports = router;
