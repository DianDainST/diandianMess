const express = require('express');

const router = express.Router();

const conn = require("../controller/indent")

// 渲染template menu页面
router.get("/indent", function (req, res) {
    return res.render('indent');
})

// 渲染订单列表
router.get("/indent/:id", conn.showIndent)

// 跳转付款页面
router.get("/indents/account", conn.upIndent)

// 渲染支付未领取
router.get('/indents/unpaid/:id', conn.showUnpaid)
// 点击确认领取
router.put('/indents/unpaid/:id', conn.putUnpaid)

// 渲染完成未评价
router.get('/indents/evaluation/:id', conn.getEvaluation)
// 点击评论
router.put('/indents/evaluation/:id', conn.putEvaluation)

// 渲染完成未评价
router.get('/indents/accomplish/:id', conn.accomplish)
// 付款页面确认付款
module.exports = router;