const express = require('express');

const router = express.Router();

const CtrlCategories = require('../controller/personageCon');

router.get('/personage', CtrlCategories.personage);

// 头像及用户名
router.get('/personage1', CtrlCategories.personage1);

// 优惠券数量
router.get('/personage2', CtrlCategories.personage2);

// 账户余额
router.get('/personage3', CtrlCategories.personage3);

// 喜欢的菜品
router.get('/personage4', CtrlCategories.personage4);

// 火热菜品
router.get('/personage5', CtrlCategories.personage5);

// 充值
router.put('/personage6', CtrlCategories.personage6);
module.exports = router;
