image_version=`date +%Y%m%d%H%M`;
echo $image_version;
git pull
docker stop diandian;
docker rm diandian;
docker build -t diandian:$image_version .;
docker images;
docker run -p 60000:80 -d --name diandian diandian:$image_version;
# -v ~/docker-data/house-web/appsettings.json:/app/appsettings.json -v ~/docker-data/house-web/NLogFile/:/app/NLogFile   --restart=always
docker logs diandian;
#删除build过程中产生的镜像    #docker image prune -a -f
docker rmi $(docker images -f "dangling=true" -q)