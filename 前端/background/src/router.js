import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    // 如果现在我去首页 但是我没有登录 应该去登录页
    // 到根目录 那么就重定向到首页
    {
      path: "/",
      name: "login",
      redirect: "/login",
      component: () => import("./views/Login.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./views/Login.vue")
    },
    // home页面
    {
      path: "/home",
      name: "home",
      component: () => import("./views/Home.vue"),
      children: [
        // 数据信息管理
        {
          path: "/view",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/view.vue")
        },
        // 菜品库管理
        {
          path: "/list",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/list.vue")
        },
        // 菜单管理
        {
          path: "/menu",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/menu.vue")
        },

        // 用户评论
        {
          path: "/evaluate",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/evaluate.vue")
        },

        // 投票管理
        {
          path: "/vote",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/vote.vue")
        },

        // 优惠券管理
        {
          path: "/coupons",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/coupons.vue")
        },

        // 公告管理
        {
          path: "/announcemente",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/announcemente.vue")
        },

        // 订单管理
        {
          path: "/order",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/order.vue")
        },

        // 留言管理
        {
          path: "/message",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/message.vue")
        },

        // 管理员管理
        {
          path: "/grantee",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/grantee.vue")
        },

        // 用户管理
        {
          path: "/users",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/users.vue")
        },

        // 模板管理
        {
          path: "/template",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/template.vue")
        },

        // 权限管理
        {
          path: "/jurisdiction",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/jurisdiction.vue")
        },

        // 模块管理
        {
          path: "/module1",
          //  @ 的含义相当于 ./src
          component: () => import("@/components/module1.vue")
        }
      ]
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, from, next) => {
  // 判断是否是login这个路由是这个路由放行
  if (to.path == "/login") return next();

  // 获取有没有令牌 如果没有令牌 去login
  const token = window.sessionStorage.getItem("token");
  if (!token) {
    window.sessionStorage.removeItem("token");
    return next("/login");
  }

  // 如果登录了 那么就放行
  next();
});

export default router;
