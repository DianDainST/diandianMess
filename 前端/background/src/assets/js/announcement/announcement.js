const queryString = require("querystring");
export default {
  data() {
    return {
      // 表格数据
      tableData: [],
      // 查询数据
      queryInfo: {
        // 给后端传的查询数据的关键词
        query: "",
        // 第几页数据
        pagenum: 1,
        // 一页显示多条条数据
        pagesize: 100
      },
      // 添加公告弹框
      addDialogVisible: false,
      // 添加公告表单数据
      addRuleForm: {
        state: "1"
      },
      // 修改公告弹框
      upDialogVisible: false,
      // 修改表单
      upRuleForm: {}
    };
  },
  methods: {
    // 渲染公告
    async showAnnouncement() {
      let { data: res } = await this.$http.get("announcement", {
        params: this.queryInfo
      });
      // console.log(res)
      if (res.code !== "200") {
        return this.$message.error("获取公告列表失败");
      } else {
        this.tableData = res.data;
        this.$message.success("获取公告列表失败");
      }
    },
    // 打开添加弹框
    addition() {
      this.addDialogVisible = true;
    },
    // 点击取消关闭添加公告
    addResetForm() {
      this.$confirm("此操作将取消添加该公告, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(() => {
          this.addDialogVisible = false;
          (this.addRuleForm.title = ""),
            (this.addRuleForm.content = ""),
            this.$message({
              type: "warning",
              message: "取消成功!"
            });
        })
        .catch(() => {
          this.$message({
            type: "success",
            message: "继续操作"
          });
        });
    },
    // 点击遮罩层取消添加
    handleClose() {
      this.$confirm("此操作将取消添加该公告, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(() => {
          this.addDialogVisible = false;
          (this.addRuleForm.title = ""),
            (this.addRuleForm.content = ""),
            this.$message({
              type: "warning",
              message: "取消成功!"
            });
        })
        .catch(() => {
          this.$message({
            type: "success",
            message: "继续操作"
          });
        });
    },
    // 添加公告
    async addAnnouncement() {
      var date = new Date();
      var year = date.getFullYear(); //获取当前年份
      var mon = date.getMonth() + 1; //获取当前月份
      var da = date.getDate(); //获取当前日
      var h = date.getHours(); //获取小时
      var m = date.getMinutes(); //获取分钟
      var s = date.getSeconds(); //获取秒
      var d = document.getElementById("Date");
      d = year + "-" + mon + "-" + da + " " + h + ":" + m + ":" + s;
      let time = d;
      this.addRuleForm.add_time = time;
      const dt1 = new Date(this.addRuleForm.add_time);
      const y1 = dt1.getFullYear();
      const m1 = (dt1.getMonth() + 1 + "").padStart(2, "0");
      const d1 = (dt1.getDate() + "").padStart(2, "0");
      const hh1 = (dt1.getHours() + "").padStart(2, "0");
      const mm1 = (dt1.getMinutes() + "").padStart(2, "0");
      const ss1 = (dt1.getSeconds() + "").padStart(2, "0");
      const date1 = `${y1}-${m1}-${d1} ${hh1}:${mm1}:${ss1}`;
      this.addRuleForm.add_time = date1;
      let { data: res } = await this.$http.post(
        "announcement",
        queryString.stringify(this.addRuleForm)
      );
      if (res.code !== "200") {
        return this.$message.error("添加失败");
      } else {
        this.$message.success("添加成功");
        this.addDialogVisible = false;
        this.showAnnouncement();
      }
    },
    // 删除公告
    del(id) {
      this.$confirm("此操作将删除该公告, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(async () => {
          let { data: res } = await this.$http.delete("announcement/" + id);
          if (res.code !== "200") {
            return this.$message.error("删除失败");
          }
          this.$message({
            type: "success",
            message: "删除成功!"
          });
          this.showAnnouncement();
        })
        .catch(() => {
          this.$message({
            type: "error",
            message: "删除失败"
          });
        });
    },
    // 修改公告回显
    async modification(id) {
      this.upDialogVisible = true;
      let { data: res } = await this.$http.get("announcement/" + id);
      if (res.code !== "200") {
        return this.$message.error("查找该公告失败");
      } else {
        this.upRuleForm = res.data[0];
        let num = "";
        let nub = res.data[0].state;
        let state = num + nub;
        this.upRuleForm.state = state;
      }
    },
    // 点击遮罩层取消修改
    uphandleClose() {
      this.$confirm("此操作将取消修改该公告, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(() => {
          this.upDialogVisible = false;
          (this.upRuleForm.title = ""),
            (this.upRuleForm.content = ""),
            this.$message({
              type: "warning",
              message: "取消修改!"
            });
        })
        .catch(() => {
          this.$message({
            type: "success",
            message: "继续修改"
          });
        });
    },
    // 点击取消关闭修改公告
    upResetForm() {
      this.$confirm("此操作将取消修改该公告, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(() => {
          this.upDialogVisible = false;
          (this.upRuleForm.title = ""),
            (this.upRuleForm.content = ""),
            this.$message({
              type: "warning",
              message: "取消修改!"
            });
        })
        .catch(() => {
          this.$message({
            type: "success",
            message: "继续修改"
          });
        });
    },
    // 确定修改公告
    async upAnnouncement() {
      console.log(this.upRuleForm);
      var date = new Date();
      var year = date.getFullYear(); //获取当前年份
      var mon = date.getMonth() + 1; //获取当前月份
      var da = date.getDate(); //获取当前日
      var h = date.getHours(); //获取小时
      var m = date.getMinutes(); //获取分钟
      var s = date.getSeconds(); //获取秒
      var d = document.getElementById("Date");
      d = year + "-" + mon + "-" + da + " " + h + ":" + m + ":" + s;
      let time = d;
      this.upRuleForm.last_update_time = time;
      const dt1 = new Date(this.upRuleForm.last_update_time);
      const y1 = dt1.getFullYear();
      const m1 = (dt1.getMonth() + 1 + "").padStart(2, "0");
      const d1 = (dt1.getDate() + "").padStart(2, "0");
      const hh1 = (dt1.getHours() + "").padStart(2, "0");
      const mm1 = (dt1.getMinutes() + "").padStart(2, "0");
      const ss1 = (dt1.getSeconds() + "").padStart(2, "0");
      const date1 = `${y1}-${m1}-${d1} ${hh1}:${mm1}:${ss1}`;
      this.upRuleForm.last_update_time = date1;
      let { data: res } = await this.$http.put(
        "announcement/" + this.upRuleForm.id,
        queryString.stringify(this.upRuleForm)
      );
      if (res.code !== "200") {
        return this.$message.error("修改失败");
      } else {
        this.$message.success("修改成功");
        this.showAnnouncement();
        this.upDialogVisible = false;
      }
    }
  },
  created() {
    this.showAnnouncement();
  },
  filters: {
    dateFormat(originVal) {
      const dt = new Date(originVal);
      const y = dt.getFullYear();
      const m = (dt.getMonth() + 1 + "").padStart(2, "0");
      const d = (dt.getDate() + "").padStart(2, "0");
      const hh = (dt.getHours() + "").padStart(2, "0");
      const mm = (dt.getMinutes() + "").padStart(2, "0");
      const ss = (dt.getSeconds() + "").padStart(2, "0");
      return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
    }
  }
};
