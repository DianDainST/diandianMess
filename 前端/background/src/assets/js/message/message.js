const queryString = require("querystring");
export default {
  data() {
    return {
      tableData: [],
      dialogVisible: false,
      form: {},
      queryInfo: {
        // 第几页数据
        pagenum: 1,
        // 一页显示多条条数据
        pagesize: 10
      },
      total: 0
    };
  },
  methods: {
    // 渲染页面
    async showmessage() {
      let { data: res } = await this.$http.get("message", {
        params: this.queryInfo
      });
      console.log(res);
      if (res.code !== "200") {
        return this.$message.error("获取留言列表失败");
      } else {
        this.$message.success("获取留言列表成功");
        this.tableData = res.data;
        this.total = res.total[0].total;
      }
    },
    // 回复
    async reply(id) {
      this.dialogVisible = true;
      let { data: res } = await this.$http.get("message/" + id);
      if (res.code !== "200") {
        return this.$message.error("查找该留言失败");
      } else {
        console.log(res);
        this.form = res.data[0];
      }
    },
    // 确认回复
    async notarize() {
      var date = new Date();
      var year = date.getFullYear(); //获取当前年份
      var mon = date.getMonth() + 1; //获取当前月份
      var da = date.getDate(); //获取当前日
      var h = date.getHours(); //获取小时
      var m = date.getMinutes(); //获取分钟
      var s = date.getSeconds(); //获取秒
      var d = document.getElementById("Date");
      d = year + "-" + mon + "-" + da + " " + h + ":" + m + ":" + s;
      let time = d;
      this.form.reply_time = time;
      const dt1 = new Date(this.form.reply_time);
      const y1 = dt1.getFullYear();
      const m1 = (dt1.getMonth() + 1 + "").padStart(2, "0");
      const d1 = (dt1.getDate() + "").padStart(2, "0");
      const hh1 = (dt1.getHours() + "").padStart(2, "0");
      const mm1 = (dt1.getMinutes() + "").padStart(2, "0");
      const ss1 = (dt1.getSeconds() + "").padStart(2, "0");
      const date1 = `${y1}-${m1}-${d1} ${hh1}:${mm1}:${ss1}`;
      this.form.reply_time = date1;
      let { data: res } = await this.$http.put(
        "message",
        queryString.stringify(this.form)
      );
      // console.log(res)
      if (res.code !== "200") {
        return console.log(res.error);
      } else {
        this.dialogVisible = false;
        this.showmessage();
      }
    },
    // 弹框
    handleClose() {
      this.dialogVisible = false;
    },
    // 分页
    // 当前第几页
    handleCurrentChange(num) {
      this.queryInfo.pagenum = num;
      this.showmessage();
    },
    //   一页显示多少数据
    handleSizeChange(val) {
      this.queryInfo.pagesize = val;
      this.showmessage();
    }
  },
  created() {
    this.showmessage();
  },
  filters: {
    dateFormat(originVal) {
      const dt = new Date(originVal);
      const y = dt.getFullYear();
      const m = (dt.getMonth() + 1 + "").padStart(2, "0");
      const d = (dt.getDate() + "").padStart(2, "0");
      const hh = (dt.getHours() + "").padStart(2, "0");
      const mm = (dt.getMinutes() + "").padStart(2, "0");
      const ss = (dt.getSeconds() + "").padStart(2, "0");
      return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
    }
  }
};
