/* eslint-disable */
import $ from "../jquery/jquery";
/* eslint-enable */
const queryString = require("querystring");
export default {
  data() {
    // 时间的校验
    let checkTime = (rule, value, callback) => {
      if (!value) {
        return callback(new Error("请输入时间，格式为：YYYY-MM-dd HH:mm:ss"));
      } else if (
        /^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(
          value
        )
      ) {
        return callback();
      } else {
        return callback(new Error("时间格式不正确,格式：YYYY-MM-dd HH:mm:ss"));
      }
    };
    return {
      // 表格数据
      tableData: [],
      // 菜品弹出框数据
      gridData: [],
      // 查询数据
      queryInfo: {
        // 给后端传的查询数据的关键词
        query: "",
        // 第几页数据
        pagenum: 1,
        // 一页显示多条条数据
        pagesize: 10
      },
      //数据总条数
      total: 0,
      // 修改弹框开关
      dialogVisible: false,
      // 修改数据
      updataform: {},
      // 修改数据显示选中的菜品表
      cities: [],
      // 添加数据弹出框
      adddialogVisible: false,
      // 添加数据
      addform: {
        // 菜单的名字
        meal_name: "",
        // 菜品的id
        list_id: [],
        // 选中的菜
        num: [],
        // 用餐开始时间
        meal_start: "",
        // 用餐结束时间
        meal_end: "",
        // 付款结束时间
        pay_end: "",
        // 菜品的预售
        count: 0,
        // 菜是否有优惠
        price: 1
      },
      // 所有菜品跟id
      addcities: [],
      // 选中时的菜品
      dishes: {},
      // 添加表单校验
      addrules: {
        meal_name: [
          {
            required: true,
            message: "请输入菜单名称",
            trigger: "blur"
          },
          {
            min: 12,
            max: 12,
            message: "名称长度在12个字符",
            trigger: "blur"
          }
        ],
        meal_start: [
          {
            required: true,
            validator: checkTime,
            trigger: "blur"
          }
        ],
        meal_end: [
          {
            required: true,
            validator: checkTime,
            trigger: "blur"
          }
        ],
        pay_end: [
          {
            required: true,
            validator: checkTime,
            trigger: "blur"
          }
        ],
        list_id: [
          {
            required: true
          }
        ]
      }
    };
  },
  methods: {
    // 渲染页面
    async getMenulist() {
      const { data: res } = await this.$http.get("menus", {
        params: this.queryInfo
      });
      if (res.code !== "200") {
        return this.$message.error("获取菜单列表失败");
      } else {
        this.$message.success("获取菜单列表成功");
        // 分页数据的总条数
        this.total = res.total;
        // 读出所有的菜品放进添加表单的数据中
        let dishes = res.dishes;
        let dishesname = [];
        for (let i = 0; i < dishes.length; i++) {
          dishesname.push(dishes[i].name);
        }
        this.addcities.name = dishesname;
        // 读出所有的菜品id放进添加表单的数据中
        let dishesid = [];
        for (let i = 0; i < dishes.length; i++) {
          dishesid.push(dishes[i].id);
        }
        this.addcities.id = dishesid;
        //
        let list = res.data;
        const meal_time_id = [];
        for (let i = 0; i < list.length; i++) {
          meal_time_id.push(list[i].id);
        }
        const meal_timeArr = [];
        const lists = [];
        const listss = [];
        for (let i = 0; i < list.length; i++) {
          if (lists.indexOf(list[i].id) == -1) {
            lists.push(list[i].id);
            listss.push(list[i]);
          }
        }
        for (let i = 0; i < meal_time_id.length; i++) {
          if (meal_timeArr.indexOf(meal_time_id[i]) == -1) {
            meal_timeArr.push(meal_time_id[i]);
          }
        }
        const name = [];
        for (let i = 0; i < meal_timeArr.length; i++) {
          name[i] = [];
          for (let j = 0; j < list.length; j++) {
            if (list[j].id == meal_timeArr[i]) {
              name[i].push(list[j].name);
              listss[i].name = name[i];
            }
          }
        }
        this.tableData = listss;
      }
    },
    // 删除数据
    remove(meal_name) {
      this.$confirm("此操作将永久删除该菜单, 是否继续?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      })
        .then(async () => {
          let { data: res } = await this.$http.delete("menus" + meal_name);
          if (res.code !== "200") {
            return this.$error("删除菜单失败");
          }
          this.$message({
            type: "success",
            message: "删除菜单成功!"
          });
          this.getMenulist();
        })
        .catch(() => {
          this.$message({
            type: "info",
            message: "已取消删除"
          });
        });
    },
    // 修改数据
    // 点击遮罩层把修改菜单关闭
    handleClose(done) {
      this.$confirm("确认关闭？")
        .then(() => {
          done();
          // 菜单的名字
          this.addform.meal_name = "";
          // 菜品的id
          this.addform.list_id = [];
          // 选中的菜
          this.addform.num = [];
          // 用餐开始时间
          this.addform.meal_start = "";
          // 用餐结束时间
          this.addform.meal_end = "";
          // 付款结束时间
          this.addform.pay_end = "";
          // 菜品的预售
          this.addform.count = 0;
          // 菜是否有优惠
          this.addform.price = 1;
        })
        .catch();
    },
    // 点击修改回显数据
    async modification(id) {
      this.dialogVisible = true;
      let { data: res } = await this.$http.get("menus/" + id);
      if (res.code !== "200") {
        return this.$message.error("查找该菜单表失败");
      } else {
        this.$message.success("查找该菜单表成功");
        // 菜品
        const dishes = res.data[0];
        const dishesId = [];
        const dishesName = [];
        for (var i = 0; i < dishes.length; i++) {
          dishesId.push(dishes[i].id);
          dishesName.push(dishes[i].name);
        }
        this.cities.name = dishesName;
        this.cities.id = dishesId;
        // 菜单
        const list = res.data[1];
        const nameIdArr = [];
        const nameArr = [];
        for (let i = 0; i < list.length; i++) {
          nameIdArr.push(list[i].list_id);
          nameArr.push(list[i].name);
        }
        /* eslint-disable */
        function dateFormat(originVal) {
          const dt = new Date(originVal);
          const y = dt.getFullYear();
          const m = (dt.getMonth() + 1 + "").padStart(2, "0");
          const d = (dt.getDate() + "").padStart(2, "0");
          const hh = (dt.getHours() + "").padStart(2, "0");
          const mm = (dt.getMinutes() + "").padStart(2, "0");
          const ss = (dt.getSeconds() + "").padStart(2, "0");
          return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
        }
        /* eslint-enable */
        const listObj = {};
        const meal_start = dateFormat(list[0].meal_start);
        const meal_end = dateFormat(list[0].meal_end);
        const pay_end = dateFormat(list[0].pay_end);
        listObj.meal_name = list[0].meal_name;
        listObj.list_id = nameIdArr;
        listObj.name = nameArr;
        listObj.meal_start = meal_start;
        listObj.meal_end = meal_end;
        listObj.pay_end = pay_end;
        this.updataform = listObj;
        // 预售数量
        this.updataform.count = res.data[1][0].count;
        this.updataform.price = res.data[1][0].off_price;
      }
    },
    // 修改点击菜品时把菜的名字放进数组
    handleCheckedCitiesChange(id) {
      var arr = id;
      var arr1 = arr.filter(function(o) {
        return !isNaN(o);
      });
      const arr2 = [];
      for (let i = 0; i < arr1.length; i++) {
        arr2.push(this.cities.id[arr1[i]]);
      }
      this.updataform.list_id = arr2;
    },
    // 确定提交修改数据
    async updatadetermine() {
      console.log(this.updataform);
      let { data: res } = await this.$http.put(
        "menus/" + this.updataform.meal_name,
        queryString.stringify(this.updataform)
      );
      if (res.code !== "200") {
        this.$message.error("修改该菜单表失败");
      } else {
        this.dialogVisible = false;
        this.$message.success("修改该菜单表成功");
        this.getMenulist();
      }
    },
    // 添加菜品
    addhandleCheckedCitiesChange(index) {
      var arr = index;
      const arr1 = [];
      for (let i = 0; i < arr.length; i++) {
        arr1.push(this.addcities.id[arr[i]]);
      }
      this.addform.num = arr1;
    },
    // 确认添加
    addconfirm() {
      this.$refs.addverify.validate(async valid => {
        if (!valid) return this.$message.error("表单内容不正确");
        if (this.addform.list_id.length == 0) {
          return this.$message.error("请选择菜品");
        } else {
          // 用餐开始时间
          const dt = new Date(this.addform.meal_start);
          const y = dt.getFullYear();
          const m = (dt.getMonth() + 1 + "").padStart(2, "0");
          const d = (dt.getDate() + "").padStart(2, "0");
          const hh = (dt.getHours() + "").padStart(2, "0");
          const mm = (dt.getMinutes() + "").padStart(2, "0");
          const ss = (dt.getSeconds() + "").padStart(2, "0");
          const date = `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
          // 用餐结束时间
          const dt1 = new Date(this.addform.meal_end);
          const y1 = dt1.getFullYear();
          const m1 = (dt1.getMonth() + 1 + "").padStart(2, "0");
          const d1 = (dt1.getDate() + "").padStart(2, "0");
          const hh1 = (dt1.getHours() + "").padStart(2, "0");
          const mm1 = (dt1.getMinutes() + "").padStart(2, "0");
          const ss1 = (dt1.getSeconds() + "").padStart(2, "0");
          const date1 = `${y1}-${m1}-${d1} ${hh1}:${mm1}:${ss1}`;
          // 用餐结束时间
          const dt2 = new Date(this.addform.pay_end);
          const y2 = dt2.getFullYear();
          const m2 = (dt2.getMonth() + 1 + "").padStart(2, "0");
          const d2 = (dt2.getDate() + "").padStart(2, "0");
          const hh2 = (dt2.getHours() + "").padStart(2, "0");
          const mm2 = (dt2.getMinutes() + "").padStart(2, "0");
          const ss2 = (dt2.getSeconds() + "").padStart(2, "0");
          const date2 = `${y2}-${m2}-${d2} ${hh2}:${mm2}:${ss2}`;
          // 赋值
          this.addform.meal_start = date;
          this.addform.meal_end = date1;
          this.addform.pay_end = date2;
          let { data: res } = await this.$http.post(
            "menus",
            queryString.stringify(this.addform)
          );
          if (res.code == "300") {
            return this.$message.error(res.msg);
          } else if (res.code !== "200") {
            return this.$message.error("添加失败");
          } else {
            this.adddialogVisible = false;
            this.$message.success("添加菜单成功");
            this.getMenulist();
            this.adddialogVisible = false;
            // 菜单的名字
            this.addform.meal_name = "";
            // 菜品的id
            this.addform.list_id = [];
            // 选中的菜
            this.addform.num = [];
            // 用餐开始时间
            this.addform.meal_start = "";
            // 用餐结束时间
            this.addform.meal_end = "";
            // 付款结束时间
            this.addform.pay_end = "";
            // 菜品的预售
            this.addform.count = 0;
            // 菜是否有优惠
            this.addform.price = 1;
          }
        }
      });
    },
    // 取消添加
    addcancel() {
      this.adddialogVisible = false;
      // 菜单的名字
      this.addform.meal_name = "";
      // 菜品的id
      this.addform.list_id = [];
      // 选中的菜
      this.addform.num = [];
      // 用餐开始时间
      this.addform.meal_start = "";
      // 用餐结束时间
      this.addform.meal_end = "";
      // 付款结束时间
      this.addform.pay_end = "";
      // 菜品的预售
      this.addform.count = 0;
      // 菜是否有优惠
      this.addform.price = 1;
    },
    // 分页
    // 当前第几页
    handleCurrentChange(num) {
      this.queryInfo.pagenum = num;
      this.getMenulist();
    },
    //   一页显示多少数据
    handleSizeChange(val) {
      this.queryInfo.pagesize = val;
      this.getMenulist();
    }
  },

  created() {
    this.getMenulist();
  },
  filters: {
    dateFormat(originVal) {
      const dt = new Date(originVal);
      const y = dt.getFullYear();
      const m = (dt.getMonth() + 1 + "").padStart(2, "0");
      const d = (dt.getDate() + "").padStart(2, "0");
      const hh = (dt.getHours() + "").padStart(2, "0");
      const mm = (dt.getMinutes() + "").padStart(2, "0");
      const ss = (dt.getSeconds() + "").padStart(2, "0");
      return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
    }
  }
};
