import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

// 引入base.css
import "./assets/css/base.css";

// 引入elementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

// 配置Axios
import Axios from "axios";
Vue.prototype.$http = Axios;
Axios.defaults.baseURL = "http://39.106.158.169:8080/api/v1/";

// 添加了一个请求拦截器
// 如果是请求拦截器 所有经过axios发起的请求都要经过拦截器的拦截
Axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    // console.log(config.headers);
    // 因为除了登录 其他都需要授权 因此在发起请求的时候 自动让拦截器拦截该请求 并且在该请求身上添加一个请求头字段
    // 字段名: Authorization
    // 字段值: token值
    config.headers.Authorization =
      "Bearer " + window.sessionStorage.getItem("token");
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// 时间过滤器
Vue.filter("dateFormat", function(originVal) {
  const dt = new Date(originVal);
  const y = dt.getFullYear();
  const m = (dt.getMonth() + 1 + "").padStart(2, "0");
  const d = (dt.getDate() + "").padStart(2, "0");
  const hh = (dt.getHours() + "").padStart(2, "0");
  const mm = (dt.getMinutes() + "").padStart(2, "0");
  const ss = (dt.getSeconds() + "").padStart(2, "0");
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
